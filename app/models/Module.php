<?php
/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 19/08/14
 * Time: 3:29 PM
 */
class Module extends Eloquent {
    protected $table = 'modules';
    
    //protected $hidden = array('updated_at');
    protected $guarded = array('id');

	public function tasks()
	{
		return $this->hasMany('Task');
	}

    public function projects()
    {
        return $this->belongsToMany('Project');
    }
}