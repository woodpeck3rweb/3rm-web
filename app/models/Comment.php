<?php

/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 19/08/14
 * Time: 3:25 PM
 */

class Comment extends Eloquent
{
	protected $table = 'comments';

	//protected $hidden = array('password');
	protected $fillable = array('task_id', 'message', 'user_id');
	protected $guarded = array('id');

	public static function validate($params)
	{
		$rules = array(
			'message' => 'Required',
			'task_id' => 'Required',
			'user_id' => 'Required'
		);
		$v = Validator::make($params, $rules);
		return $v;
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function task()
	{
		return $this->belongsTo('Task');
	}

	public function files()
	{
		return $this->hasMany('File');
	}
}