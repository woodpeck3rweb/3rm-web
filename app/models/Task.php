<?php
/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 19/08/14
 * Time: 3:20 PM
 */

/**
 * due_date
 * time_spent
 * assignee_id
 * priority : ["Blocker", "Critical", "Major", "Minor", "Trivial"]
 * status ["Open", "In Progress", "Resolved", "Reopened", "Closed"]
 */
class Task extends Eloquent {
    protected $table = 'tasks';
    
    //protected $hidden = array('password');
//    protected $fillable = array('');
    protected $guarded = array('id');

	public static function validate($params)
	{
		$rules = array(
			'title'=>'Required'
		);
		$v = Validator::make($params, $rules);
		return $v;
	}

	public static function boot()
	{
		parent::boot();

		static::created(function($task)
		{

		});

		static::updated(function($task)
		{

		});
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function assignee()
	{
		return $this->belongsTo('User', 'assignee_id');
	}

	public function project()
	{
		return $this->belongsTo('Project');
	}

	public function module()
	{
		return $this->belongsTo('Module');
	}

	public function milestone()
	{
		return $this->belongsTo('Milestone');
	}

	public function component()
	{
		return $this->belongsTo('Component');
	}

	public function files()
	{
		return $this->hasMany('File');
	}

	public function comments()
	{
		return $this->hasMany('Comment');
	}

	public function timelogs()
	{
		return $this->hasMany('TimeLog');
	}

	public function getTotalTimeAttribute()
	{
		return $this->hasMany('TimeLog')->sum('time_spent');
	}
}