<?php
/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 19/08/14
 * Time: 3:24 PM
 */
class File extends Eloquent {
    protected $table = 'files';
    
    //protected $hidden = array('password');
    //protected $fillable = array('username', 'email');
    protected $guarded = array('id');

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function task()
	{
		return $this->belongsTo('Task');
	}

	public function comment()
	{
		return $this->belongsTo('Comment');
	}
}