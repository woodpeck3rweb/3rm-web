<?php
/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 17/08/14
 * Time: 12:08 AM
 */
class Client extends Eloquent {
    protected $table = 'clients';
    
    //protected $hidden = array('password');
//    protected $fillable = array('*');
    protected $guarded = array('id');

	public static function validate($params) {
		$rules = array(
			'full_name' => 'required|min:1'
		);
		return Validator::make($params, $rules);
	}
}