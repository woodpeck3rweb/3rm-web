<?php
/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 17/08/14
 * Time: 12:05 AM
 */
class Project extends Eloquent {
    protected $table = 'projects';
    
    //protected $hidden = array('password');
    protected $fillable = array('title', 'description', 'logo','status', 'eta','start_date', 'end_date', 'client_id', 'manager_id', 'user_id');
    //protected $guarded = array('id', 'password');

	public static function validate($params) {
		$rules = array(
			'title' => 'required|min:1'
		);
		return Validator::make($params, $rules);
	}

    public function client()
	{
		return $this->belongsTo('Client');
	}

	public function manager()
	{
		return $this->belongsTo('User', 'manager_id');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function milestones()
	{
		return $this->hasMany('Milestone');
	}

	public function components()
	{
		return $this->hasMany('Component');
	}

	public function tasks()
	{
		return $this->hasMany('Task');
	}
    public function modules()
    {
        return $this->belongsToMany('Module');
    }
    public function users()
    {
        return $this->belongsToMany('User', 'favorites', 'project_id', 'user_id');
    }
}