<?php
/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 19/08/14
 * Time: 3:14 PM
 */
class Milestone extends Eloquent {
    protected $table = 'milestones';
    
    //protected $hidden = array('password');
    //protected $fillable = array('username', 'email');
    protected $guarded = array('id');

    public static function validate($params) {
        $rules = array(
            'title' => 'required|min:1'
        );
        return Validator::make($params, $rules);
    }

    public function project()
	{
		return $this->belongsTo('Project');
	}
    public function tasks(){

        return $this->hasMany('task');
    }
}