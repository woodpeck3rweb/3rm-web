<?php
/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 24/08/14
 * Time: 1:24 PM
 */
class TimeLog extends Eloquent {
    protected $table = 'time_logs';
    
    //protected $hidden = array('updated_at');
    protected $fillable = array('id', 'description', 'time_spent', 'start_time', 'end_time', 'ip');
    protected $guarded = array('id');
    
    public function user()
	{
		return $this->belongsTo('User');
	}

	public function task()
	{
		return $this->belongsTo('Task');
	}

	public function module()
	{
		return $this->belongsTo('Module');
	}
}