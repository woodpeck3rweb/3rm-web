<?php
/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 19/08/14
 * Time: 3:15 PM
 */
class Component extends Eloquent {
    protected $table = 'components';
    
    //protected $hidden = array('password');
    //protected $fillable = array('username', 'email');
    protected $guarded = array('id');

	public function project()
	{
		return $this->belongsTo('Project');
	}
}