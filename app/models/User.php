<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface
{

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('email', 'password', 'picture', 'first_name', 'last_name',
		'full_name', 'designation', 'employee_id');

	protected $hidden = array('password', 'remember_token');

	public function getRememberToken()
	{
		return $this->remember_token;
	}

	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	public static function validate($params)
	{
		$rules = array(
			'password' => 'required|min:3',
			'email' => 'required|email|unique:users'
		);
		return Validator::make($params, $rules);
	}

	public function userdetails()
	{
		return $this->hasOne('UserDetails');
	}

	public function projects()
	{
		return $this->hasMany('Project');
	}

	public function modules()
	{
		return $this->hasMany('Module');
	}

	public function createdtasks()
	{
		return $this->hasMany('Task');
	}

	public function tasks()
	{
		return $this->hasMany('Task', 'assignee_id');
	}

	public function activities()
	{
		return $this->hasMany('Activity');
	}

	public function comments()
	{
		return $this->hasMany('Comment');
	}

	public function files()
	{
		return $this->hasMany('File');
	}

	public function timelogs()
	{
		return $this->hasMany('TimeLog');
	}

	public function currentTimeLogs()
	{
		return $this->hasMany('TimeLog')
			->where('end_time', '=', '0000-00-00 00:00:00')
			->where('start_time', '!=', '0000-00-00 00:00:00');
	}

	public function getTotalTimeAttribute()
	{
		return $this->hasMany('TimeLog')->sum('time_spent');
	}

	public function favorites()
	{
		return $this->belongsToMany('Project', 'favorites', 'user_id', 'project_id');
	}
}
