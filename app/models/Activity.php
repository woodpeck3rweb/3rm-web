<?php
/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 19/08/14
 * Time: 3:30 PM
 */
class Activity extends Eloquent {
    protected $table = 'activity';
    
    //protected $hidden = array('updated_at');
    //protected $fillable = array('name');
    protected $guarded = array('id');

	public function user()
	{
		return $this->belongsTo('User');
	}
}