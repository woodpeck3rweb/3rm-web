<?php
/**
 * Created by PhpStorm.
 * User: pradyumnad
 * Date: 17/08/14
 * Time: 12:10 AM
 */
class UserDetails extends Eloquent {
    protected $table = 'user_details';
    
    protected $fillable = array('dob', 'height', 'weight', 'blood_group', 'about_me', 'interest',
		'facebook', 'twitter', 'skype', 'website');
	protected $guarded = array('id');

    public function user()
	{
		return $this->belongsTo('User');
	}
}