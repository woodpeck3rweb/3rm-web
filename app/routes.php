<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
	if (Auth::check()) {
		return Redirect::to('/home');
	} else {
		return Redirect::to('/login');
	}
});
Route::get('/home', 'HomeController@Home');
Route::get('/login', 'HomeController@Login');

Route::get('/logout', 'HomeController@Logout');
Route::get('/profile', 'HomeController@Profile');
Route::get('/settings', 'HomeController@Settings');
Route::get('/allclients', 'HomeController@Allclients');
Route::get('/newclient', 'HomeController@Newclient');
Route::get('/register', 'HomeController@Register');
Route::get('/users', 'HomeController@Users');
Route::get('/reports', 'HomeController@Reports');
Route::get('/finance', 'HomeController@Finance');
Route::get('/leavemanagement', 'HomeController@Leavemanagement');
Route::get('/calendar', 'HomeController@Calendar');
Route::get('/form-elements', 'HomeController@Formelements');
Route::get('/forgotpassword', 'HomeController@Forgotpassword');
Route::post('/sendnewpassword', 'HomeController@sendnewpassword');
Route::get('/sendstatus', 'HomeController@sendstatus');

Route::controller('/user', 'UserController');
Route::post('user/favorite', 'UserController@Favorite');
Route::get('user/favorites/{id}', 'UserController@Favorites');

Route::controller('/client', 'ClientController');

Route::controller('/project', 'ProjectController');

Route::controller('/task', 'TaskController');

Route::controller('/milestone', 'MilestoneController');
Route::get('/milestone/tasks/{id}', 'MilestoneController@tasks');
Route::post('/milestone/delete/{id}', 'MilestoneController@delete');
Route::post('/project/assignmodules', 'ProjectController@assignmodules');

Route::controller('/timelog', 'TimeLogController');

Route::controller('/comment', 'CommentController');

//package configuration file
$config = Config::get("socialogin::config");

//OAuth response. Set your redirect url in vendor/enuke/socialogin/src/config/config.php
Route::get($config['redirect'], function () {
	//get OAuth provider name store in session
	$oauth_provider = Session::get('oauth_provider');

	if (!Session::has('oauth_provider')) {
		die('Invalid Access');
	}

	if ($oauth_provider == 'facebook') {
		$code = Input::get('code');
		if (strlen($code) == 0) die('There was an error communicating with Facebook');
		$uid = Socialogin::getUser();
		if ($uid == 0) die('There was an error');
		$data = Socialogin::api('/me');

	} else if ($oauth_provider == 'google') {
		$google = new Enuke\Socialogin\Google;
		$data = $google->get_data();

		if (strpos($data["email"],'@kronch.co') == false) {
			return Redirect::to('/');
		}

		$params = array();
		$params['email'] = $data["email"];
		$params['full_name'] = $data["name"];
		$password = $data['email'] . '#2014';
		$params['password'] = Hash::make($data['email'] . '#2014');
		$params['first_name'] = $data["given_name"];
		$params['last_name'] = $data['family_name'];
		$params['picture'] = $data['picture'];
		$user = User::firstOrNew(array('email' => $data['email']));
		$status = $user->save();

		$user->update($params);
		//Creating an empty User Details row
		if ($user->userdetails == null) {
			$userInfo = new UserDetails;
			$userInfo->user()->associate($user);
			$userInfo->save();
			$userInfo->touch();
		}

		Auth::attempt(array('email' => $data['email'], 'password' => $password), true);
		if ($status)
			return Redirect::to('/home');

		if (empty($data)) die('There was an error');
	} else if ($oauth_provider == 'twitter') {
		$twitter = new Enuke\Socialogin\Twitter;
		$data = $twitter->get_return();
		if (empty($data)) die('There was an error communicating with Twitter');
	}
	//Removing An Item From The Session
	Session::forget('key');
});

// login route
Route::get('/glogin/', function () {
	$type = Input::get('type');
	if (empty($type)) {
		die('Invalid Access');
	}
	Session::put('oauth_provider', $type);

	if ($type == 'facebook') {
		return Redirect::to(Socialogin::loginUrl());
	} else if ($type == 'twitter') {
		$twitter = new Enuke\Socialogin\Twitter;
		$check_connection = $twitter->login();
		if ($check_connection) {
			return Redirect::to($check_connection);
		} else {
			die('Something Went Wrong');
		}
	} elseif ($type == 'google') {
		$google = new Enuke\Socialogin\Google;
		$url = $google->login();
		return Redirect::to($url);
	}
});