<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTimeLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('time_logs', function(Blueprint $table)
		{
			//
			$table->dateTime("start_time");
			$table->dateTime("end_time");
			$table->string("ip");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('time_logs', function(Blueprint $table)
		{
			//
		});
	}

}
