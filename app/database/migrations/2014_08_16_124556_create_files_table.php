<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('files', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('title');
			$table->string('type');
			$table->string('file');

			$table->unsignedInteger('task_id')->nullable();
			$table->foreign('task_id')
				->references('id')->on('tasks')
				->onDelete('cascade');

			$table->unsignedInteger('comment_id')->nullable();
			$table->foreign('comment_id')
				->references('id')->on('comments')
				->onDelete('cascade');

			$table->unsignedInteger('user_id')->nullable();
			$table->foreign('user_id')
				->references('id')->on('users');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('files');
	}

}
