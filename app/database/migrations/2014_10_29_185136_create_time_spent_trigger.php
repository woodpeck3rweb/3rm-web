<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeSpentTrigger extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		DB::unprepared("
			CREATE TRIGGER time_spent_update BEFORE UPDATE ON time_logs
			FOR EACH ROW
			SET New.time_spent = ROUND(TIMESTAMPDIFF(MINUTE, New.start_time, New.end_time)/60.0, 2)");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		DB::unprepared("DROP TRIGGER time_spent_update");
	}
}
