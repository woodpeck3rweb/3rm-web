<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('title');
			$table->text('description');
			$table->string('logo');
			$table->string('status');
			$table->integer('eta');

			$table->date('start_date');
			$table->date('end_date');

			$table->unsignedInteger('manager_id')->nullable();
			$table->foreign('manager_id')
				->references('id')->on('users');

			$table->unsignedInteger('client_id')->nullable();
			$table->foreign('client_id')
				->references('id')->on('clients');

			$table->unsignedInteger('user_id');
			$table->foreign('user_id')
				->references('id')->on('users');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
