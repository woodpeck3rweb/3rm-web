<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksTableAddAssigneeId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tasks', function(Blueprint $table)
		{
//
			//
			$table->unsignedInteger("assignee_id")->nullable();
			$table->foreign('assignee_id')
				->references('id')->on('users');

			$table->date("due_date");
			$table->float("time_spent");

			DB::statement("ALTER TABLE tasks MODIFY eta FLOAT(8,2)");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tasks', function(Blueprint $table)
		{
			//
		});
	}

}
