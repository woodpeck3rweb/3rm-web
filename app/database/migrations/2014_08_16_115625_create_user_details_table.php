<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('dob');
			$table->double('height');
			$table->double('weight');
			$table->string('blood_group');
			$table->string('shirt_size');
			$table->string('gender');
			$table->string('about_me');
			$table->string('interest');
			$table->string('facebook');
			$table->string('skype');
			$table->string('twitter');
			$table->string('website');

			$table->unsignedInteger('user_id');
			$table->foreign('user_id')
				->references('id')->on('users')
				->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_details');
	}

}
