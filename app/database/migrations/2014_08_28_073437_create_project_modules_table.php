<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectModulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('module_project', function(Blueprint $table)
		{
			$table->increments('id');
            $table->unsignedInteger('module_id');
            $table->foreign('module_id')
                ->references('id')->on('modules');
            $table->unsignedInteger('project_id');
            $table->foreign('project_id')
                ->references('id')->on('projects');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects_modules');
	}

}
