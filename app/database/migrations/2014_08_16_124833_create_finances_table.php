<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('finances', function(Blueprint $table)
		{
			$table->increments('id');

			$table->double('cost');
			$table->double('hourly_cost');
			$table->double('original_eta');
			$table->double('amount_paid');
			$table->string('payment_mode');
			$table->enum('type', array('hourly', 'total'));

			$table->unsignedInteger('project_id');
			$table->foreign('project_id')
				->references('id')->on('projects')
				->onDelete('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('finances');
	}

}
