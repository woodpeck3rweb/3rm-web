<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tasks', function(Blueprint $table)
		{
			//
			$table->unsignedInteger('component_id')->nullable();
			$table->foreign('component_id')
				->references('id')->on('components');

			$table->unsignedInteger('milestone_id')->nullable();
			$table->foreign('milestone_id')
				->references('id')->on('milestones');

			$table->unsignedInteger('project_id')->nullable();
			$table->foreign('project_id')
				->references('id')->on('projects');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tasks', function(Blueprint $table)
		{
			//
		});
	}

}
