<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('title');
			$table->text('description');
			$table->integer('eta');

			$table->unsignedInteger('module_id')->nullable();
			$table->foreign('module_id')
				->references('id')->on('modules')
				->onDelete('cascade');

			$table->unsignedInteger('user_id')->nullable();
			$table->foreign('user_id')
				->references('id')->on('users');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}

}
