<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('time_logs', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string("description");
			$table->float("time_spent");

			$table->unsignedInteger('module_id')->nullable();
			$table->foreign('module_id')
				->references('id')->on('modules');


			$table->unsignedInteger('task_id')->nullable();
			$table->foreign('task_id')
				->references('id')->on('tasks');

			$table->unsignedInteger('user_id');
			$table->foreign('user_id')
				->references('id')->on('users');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('time_logs');
	}

}
