<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('components', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('description');

			$table->unsignedInteger('project_id');
			$table->foreign('project_id')
				->references('id')->on('projects')
				->onDelete('cascade');

			$table->unsignedInteger('user_id')->nullable();
			$table->foreign('user_id')
				->references('id')->on('users');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('components');
	}

}
