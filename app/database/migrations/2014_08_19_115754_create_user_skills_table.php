<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_skills', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('range');

			$table->unsignedInteger('user_id');
			$table->foreign('user_id')
				->references('id')->on('users');

			$table->unsignedInteger('skill_id');
			$table->foreign('skill_id')
				->references('id')->on('skills');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_skills');
	}

}
