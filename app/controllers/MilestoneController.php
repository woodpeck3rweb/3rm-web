<?php
/**
 * Created by PhpStorm.
 * User: woodpeck
 * Date: 24/9/14
 * Time: 3:06 PM
 */

class MilestoneController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        //
        $milestones = Milestone::all();
        return Response::json($milestones);
    }

    public function getTasks($id)
    {
        $milestone = Milestone::find($id);

        $tasks = $milestone->tasks;

        return Response::json(array('tasks' => $tasks,'milestone'=>$milestone));

    }


    public function postDelete($id){

        if(Milestone::destroy($id)){
            $status = "Milestone deleted successfully.";
        }else{
            $status = "Could Not Delete Milestone.";
        }
        return Response::json(array('status' => $status));
    }

}