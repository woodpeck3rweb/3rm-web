<?php

class TimeLogController extends \BaseController {

	public function getShow($id)
	{
		$wt = TimeLog::find($id);
		return Response::json($wt);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		//
		$params = Input::all();
		$wt = TimeLog::find($id);
		if($wt->time_spent <= 0.0) {
			$wt->end_time = date_create()->format('Y-m-d H:i:s');

			$status = $wt->save();
			if ($status) {
				return Response::json(array("params"=>$params, "created_id"=>$wt->id), 201);
			}
			return Response::json(array("params"=>$params, "message"=>"Sorry there is some problem"), 400);
		}
		return Response::json(array("params"=>$params, "message"=>"Already punched out from other source"));
	}

	public function getRecent($id)
	{
		$params = Input::all();
		$user = Auth::user();

		$wt = $user->timelogs()->where('end_time', '=', null)->whereRaw('start_time > 0')->where('task_id', '=', $id)->first();
		if(sizeof($wt) > 0) {
			//Just to get rid of Client side and Server side time differences
			$wt['current_time'] = date_create()->format('Y-m-d H:i:s');
			return Response::json($wt, 201);
		}
		return Response::json(array('message'=>'No Recent Timers'), 401);
	}

	public function postCreate($task_id)
	{
		$params = Input::all();
		$wt = new TimeLog($params);
		$wt->start_time = date_create()->format('Y-m-d H:i:s');
//		$wt->browser = $_SERVER['HTTP_USER_AGENT'];
		$wt->ip = $_SERVER['SERVER_ADDR'];
//		$wt->server_name = $_SERVER['SERVER_NAME'];
//		$wt->remote_address = $_SERVER['REMOTE_ADDR'];

		$task = Task::find($task_id);
		$wt->task()->associate($task);

		$user = Auth::user();
		$wt->user()->associate($user);

		$status = $wt->save();
		if ($status) {
			return Response::json(array("params"=>$params, "created_id"=>$wt->id), 201);
		}
		return Response::json(array("params"=>$params, "message"=>"Sorry there is some problem"), 404);
	}

}
