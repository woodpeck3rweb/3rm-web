<?php

use Illuminate\Mail\Message;

class HomeController extends BaseController
{

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function Home()
	{
		$user = Auth::user();
		$tasks = $user->tasks()->where('due_date', '<=', date('Y-m-d'))->whereRaw("status NOT IN ('Closed', 'Resolved')")->get()->load(['project' => function ($q) {
				$q->select(['id', 'title']);
			}]);
		$mainTasks = $tasks;

		//Graph Data
		$graph_data = UserController::getAllweeklygraph();

		//Current Tasks
		$currentTasks = TaskController::getCurrent();

		return View::make('home', array('tasks' => $mainTasks, 'graph' => $graph_data, 'current' => $currentTasks, 'user' => $user));
	}

	public function Login()
	{
		return View::make('login');
	}

	public function Logout()
	{
		return View::make('logout');
	}

	public function Profile()
	{
		$user = Auth::user();
		$user->load('userdetails');

		return View::make('profile', array('user' => $user));
	}

	public function Settings()
	{
		return View::make('settings');
	}

	public function Allclients()
	{
		$user = Auth::user();
		$clients = Client::all();
		return View::make('allclients', array('clients' => $clients, 'user' => $user));
	}

	public function Newclient()
	{
		return View::make('newclient');
	}

	public function Register()
	{
		return View::make('register');
	}

	public function Users()
	{
		$users = User::all();
		return View::make('users', array('users' => $users));
	}

	public function Reports()
	{
		$user = Auth::user();

		$graph_data = UserController::getWeeklygraph($user->id);

		return View::make('all-reports', array('user' => $user, "graph" => $graph_data));
	}

	public function Leavemanagement()
	{
		$user = Auth::user();
		return View::make('leavemanagement', array('user' => $user));
	}

	public function Finance()
	{
		$user = Auth::user();
		return View::make('finance', array('user' => $user));
	}

	public function Calendar()
	{
		$user = Auth::user();
		return View::make('calendar', array('user' => $user));
	}

	public function Formelements()
	{
		return View::make('form-elements');
	}

	public function Forgotpassword()
	{

		return View::make('forgotpassword');
	}

	public function Sendnewpassword()
	{

		$params = Input::all();
		if (isset($params['email'])) {
			$password = rand(111111, 999999);
			$user = DB::table('users')->where('email', $params['email'])->first();
			if (isset($user->email)) {
				$user = User::find($user->id);
				$user->password = Hash::make($password);
				$status = $user->save();
				$user->touch();
				$data = array("name" => $user->first_name . $user->last_name, "password" => $password);
				if ($status) {
					$tempUser = array(
						'email' => $user->email,
						'name' => 'Laravelovich'
					);
					Mail::send('emails.forgotemail', $data, function ($message) use ($tempUser) {
						$message->from('team@SPARK.com', 'Site Admin');
						$message->to($tempUser['email'], $tempUser['name'])->subject('Your new password.!');
					});
				}
				if ($status) {
					return Response::json(array('message' => "New password is sent to your email"));
				} else {
					return Response::json(array('message' => "Something went wrong"));
				}
			} else {
				return Response::json(array('message' => "No user is registered with this email."));
			}
		}
	}

	public function sendstatus()
	{
		$to = "prady@kronch.co";
		$cc = "shankaer@kronch.co";
		$cc2 = "charita.gorijala@kronch.co";

		$params = Input::all();
		$status = $params["message"];
		$user = Auth::user();
		$response = Mail::send('emails.daily_status',
			array("name" => "Pradyumna", "status" => $status),
			function (Message $message) use ($to, $cc, $cc2, $user) {
				$today = date("F j, Y");
				$message->to($to)->subject($user->first_name . ' Daily Status ' . $today);
				$message->cc($cc);
				$message->cc($cc2);
				$message->cc($user->email);
			}
		);

		return Response::json(array("message" => $response));
	}
}