<?php

class UserController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		//
		if (Input::has('api_key')) {
			$users = User::all(['id AS value', 'first_name as text']);
			return Response::json($users);
		}
		$users = User::all();
		return Response::json($users);
	}


	public function getProjects()
	{
		$user = Auth::user();
		return Response::json($user->projects);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		//
		$params = Input::all();

		$v = User::validate($params);
		if ($v->passes()) {
			$params['password'] = Hash::make($params['password']);
			$user = User::create($params);
			$status = $user->save();

			//Creating an empty User Details row
			$userInfo = new UserDetails;
			$userInfo->user()->associate($user);
			$userInfo->save();
			$userInfo->touch();

			if ($status) {
				return Response::make("User creation success !");
			}
			return Response::json(array('message' => "Sorry, unable to create User", 'params' => $params));
		} else {
			return Response::json(array('message' => $v->messages()->toArray()));
		}
	}


	public function postLogin()
	{
		$params = Input::all();

		if (Auth::attempt($params, true)) {
			return Response::json(['message' => 'Success']);
		}
		return Response::json(['message' => 'Failed'], 400);
	}

	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('/login');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function getShow($id)
	{
		//
		$user = User::find($id);
		$user->load('userdetails');

		return Response::json($user);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		//

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		//
		$params = Input::all();

		User::saving(function ($user) {
			$user->full_name = $user->first_name . " " . $user->last_name;
			return true;
		});

		if (Input::has('password')) {
			$params['password'] = Hash::make($params['password']);
		}

		$user = User::find($id);
		if (array_key_exists('picture', $params)) {

			$imageBase64 = preg_replace('#^data:image/[^;]+;base64,#', '', $params['picture']);
			$imageData = base64_decode($imageBase64);
			$source = imagecreatefromstring($imageData);
			$imageName = $user->first_name . $user->id . ".png";
			$imageSave = imagejpeg($source, 'profile_pics/' . $imageName, 100);
			imagedestroy($source);
			// create a folder under public
			$url = base_path('public/profile_pics/' . $imageName);
			$url = asset('profile_pics/' . $imageName);

			$params['picture'] = $url;
		}

		//$user->phone= $params['phone'];
		$status = $user->update($params);
		$status = $user->userdetails->update($params);

		return Response::json(array('status' => $status));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function postDestroy($id)
	{
		//
	}

//switch($duration) {
//case 'D':
//$rates = $cp->agencyrates()->where('agency_id', '=', $id)->where(DB::raw('DAY(created_at)'), '=', date('d'))->select(['value', 'created_at'])->get();
//break;
//case 'W':
//$rates = $cp->agencyrates()->where('agency_id', '=', $id)->where(DB::raw('WEEKOFYEAR(created_at)'), '=', date('W'))->select(['value', 'created_at'])->get();
//break;
//case 'M':
//$rates = $cp->agencyrates()->where('agency_id', '=', $id)->where(DB::raw('MONTH(created_at)'), '=', date('m'))->select(['value', 'created_at'])->get();
//break;
//case '3M':
//$rates = $cp->agencyrates()->where('agency_id', '=', $id)->whereRaw('created_at >= now()-interval 3 month')->select(['value', 'created_at'])->get();
//break;
//case 'Y':
//$rates = $cp->agencyrates()->where('agency_id', '=', $id)->where(DB::raw('YEAR(created_at)'), '=', date('Y'))->select(['value', 'created_at'])->get();
//break;
//default:
//$rates = $cp->agencyrates()->where('agency_id', '=', $id)->where(DB::raw('DAY(created_at)'), '=', date('d'))->select(['value', 'created_at'])->get();
//break;
//}
	public function getGraph()
	{
		$week = date('W');
		$query =
			"SELECT u.first_name AS Name,
					(SELECT SUM( time_spent )
						FROM time_logs
						WHERE user_id = u.id AND WEEKOFYEAR(created_at) = '$week') as Time
			FROM users u";

		$hours = DB::select($query);

		$i = 0;

		foreach ($hours as $u) {
			$time = $u->Time ? $u->Time : 0.0;
			$results[$i] = array($u->Name, floatval($time));
			$names[$i] = $u->Name;
			$i++;
		}
		return array('values' => json_encode($results), 'names' => json_encode($names));
	}

	public static function getWeeklygraph($id)
	{
		$q = "
			SELECT id, SUM(time_spent) AS TIME , SUBSTRING(DAYNAME(created_at) , 1, 3) AS DAY
			FROM time_logs
			WHERE user_id = '$id'
			AND WEEKOFYEAR(created_at) = WEEKOFYEAR(NOW())
			GROUP BY WEEKDAY(created_at)
		";
		$hours = DB::select($q);


		$results = array();
		$days = array();
		for ($i = 0; $i < count($hours); $i++) {
			$day_time = $hours[$i];
			$time = $day_time->TIME ? $day_time->TIME: 0.0;
			$results[$i] = array($day_time->DAY, round(floatval($time), 2));
			$days[$i] = $day_time->DAY;
		}

		return array('values' => $results, 'days' => $days);
	}

	public static function getAllweeklygraph()
	{
		$q = "SELECT u.first_name AS Name, SUM( t.time_spent ) AS Time,
				SUBSTRING( DAYNAME( t.created_at ) , 1, 3 ) AS Day
				FROM
				users u, time_logs t
				WHERE t.user_id = u.id AND WEEKOFYEAR(t.created_at) = WEEKOFYEAR(NOW())
				GROUP BY u.id, WEEKDAY( t.created_at )
		";
		$hours = DB::select($q);

		$users = array();
		$results = array();
		$flag= 0;

		$days = array('Mon'=>0, "Tue"=>1, "Wed"=>2, "Thu"=>3, "Fri"=>4, "Sat"=>5, "Sun"=>6);
		for ($i = 0; $i < count($hours); $i++) {
			//Gettings single row
			$item = $hours[$i];

			//Getting the user name
			if(!array_key_exists($item->Name, $users)) {
				$users[$item->Name]['data'] = array_fill(0, 7, 0.00);
				$results[$flag]['name'] = $item->Name;
				$flag ++;
			}

			$days_data = $users[$item->Name]['data'];
			$days_data[$days[$item->Day]] = round(floatval($item->Time), 2);
			$users[$item->Name]['data'] = $days_data;
		}

		for($j =0; $j < count($results); $j++) {

			$item = $results[$j];
			$results[$j]['data'] = $users[$item['name']]['data'];
		}

		return array('values' => $results);
	}

	public function postFavorite()
	{

		$params = Input::all();
		$user_id = $params['user_id'];
		$projectId = $params['project_id'];
		$insertingArray = array();
		array_push($insertingArray, array('user_id' => $user_id, 'project_id' => $projectId));
		$status = DB::table('favorites')->insert($insertingArray);
		if ($status) {
			return Response::json(array('message' => "Added to favorites Successfully!", 'status' => 'success'));
		} else {
			return Response::json(array('message' => "Could Not add to favorites.", 'status' => 'failure'));
		}
	}

	public function getFavorites($id)
	{
		$user = User::find($id);
		$favorites = $user->favorites;

		return $favorites;
	}

	public function getIsworking(){
		$user = Auth::user();
		$results = $user->currentTimeLogs()->get();

		$status = false;
		if (count($results)) {
			$status = true;
		}
		return Response::json(array("status"=>$status));
	}
}
