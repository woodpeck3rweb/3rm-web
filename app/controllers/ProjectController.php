<?php

class ProjectController extends BaseController
{

	public function __construct()
	{
		$this->beforeFilter('auth', array('except' => 'getLogin'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		//
		$projects = Project::all();
		return Response::json($projects);
	}

	public function getAll()
	{
		$user = Auth::user();
		$favorites = $user->favorites;
		$users = User::all();
		$projects = DB::table('projects')
			->select('projects.id as pid', 'users.id as id', 'projects.*', 'users.*')
			->leftJoin('users', 'users.id', '=', 'projects.manager_id')
			->get();
		$clients = Client::all();

		// echo "<pre>";
		//var_dump($projects); exit;
		return View::make('project.all', array('projects' => $projects, 'users' => $users, 'clients' => $clients, 'favorites' => $favorites));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		$params = Input::all();

		$v = Project::validate($params);
		if ($v->passes()) {
			$project = new Project($params);
			$project->status = 'New';
			if (Input::has('user_id')) {
				$user = User::find($params['user_id']);
				$project->user()->associate($user);
			} else {
				$user = Auth::user();
				$project->user()->associate($user);
			}
			$status = $project->save();

			if ($status) {
				return Response::make("Project creation success !");
			}
			return Response::json(array('message' => "Sorry, unable to create Project", 'params' => $params));
		} else {
			return Response::json(array('message' => $v->messages()->toArray()));
		}
	}

	public function postNewmilestone()
	{
		$params = Input::all();

		$v = Milestone::validate($params);
		if ($v->passes()) {
			$milestone = new Milestone($params);
			$project = Project::find($params['project_id']);
			$milestone->project()->associate($project);
			$status = $milestone->save();

			if ($status) {
				return Response::make("Milestone creation success !");
			}
			return Response::json(array('message' => "Sorry, unable to create Milestone", 'params' => $params));
		} else {
			return Response::json(array('message' => $v->messages()->toArray()));
		}
	}


	public function getNewmilestone($id)
	{
		$project = Project::find($id);

		//$projects = Project::all(array('id', 'title'));

		return View::make('project/newmilestone', array('project' => $project));
	}

	public function getNew()
	{
		$clients = Client::all(array('id', 'full_name'));
		$users = User::all(array('id', 'first_name', 'last_name', 'email'));

		return View::make('project/new', array('clients' => $clients, 'users' => $users));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function getShow($id)
	{
		//
		$project = Project::find($id);
		return View::make('project.detailed', array('project' => $project));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		//
		$project = Project::find($id);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		$project = Project::find($id);

		$params = Input::all();
		$status = $project->update($params);

		if ($status) {
			return Response::json(array("message" => "Update succeeded"));
		}
		return Response::json(array("message" => "Update failed"), 400);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function postDestroy($id)
	{
		//
	}

	public function getTasks($id)
	{
		$project = Project::find($id);
		$milestones = Milestone::all();
		$tasks = $project->tasks()->get()->load([
			'assignee' => function ($q) {
					$q->select("id", 'first_name', 'last_name');
				},
			'user' => function ($q) {
					$q->select("id", 'first_name', 'last_name');
				},
		]);
		$users = User::all();
		if (Input::has('api_key')) {

			foreach ($tasks as $task) {
				$task->time_spent = $task->total_time;
				$task->save();
			}
			return Response::json($tasks);
		}
		return View::make('project.tasks', array('project' => $project, 'tasks' => $tasks, 'milestones' => $milestones, 'users' => $users));
	}

	public function getReports($id)
	{
		$project = Project::find($id);
		$values = $this->getGraph($id);
		return View::make('project.reports', array('project' => $project, 'values' => $values));
	}

	public function getMilestones($id)
	{
		$project = Project::find($id);
		$milestones = $project->milestones;
		if (Input::has('api_key')) {
			return Response::json($project->milestones);
		}
		return View::make('project.milestones', array('project' => $project, 'milestones' => $milestones));
	}

	public function getFinance($id)
	{
		$project = Project::find($id);
		return View::make('project.finance', array('project' => $project));
	}

	public function getComponents($id)
	{
		$project = Project::find($id);
		$components = Component::all();
		return View::make('project.components', array('project' => $project, 'components' => $components));
	}


	public function getProjectmanagement($id)
	{
		$project = Project::find($id);
		$myProjectId = $id;
		$restModules = DB::table('modules')
			->whereNotExists(function ($query) use ($id) {
				$query->select('*')
					->from('module_project')
					->leftJoin('projects', 'projects.id', '=', 'module_project.project_id')
					->whereRaw('modules.id= module_project.module_id')
					// ->where('projects.id','=',$id);
					->whereRaw("projects.id = $id");
			})
			->get();
		$modules = $project->modules;

		$users = DB::table('users')
			->join('team', 'team.userid', '=', 'users.id')
			->join('projects', 'projects.id', '=', 'team.projectid')
			->where('projects.id', '=', $id)
			->get();
		$restUsers = DB::table('users')
			->whereNotExists(function ($query) use ($id) {
				$query->select('*')
					->from('team')
					->leftJoin('projects', 'projects.id', '=', 'team.projectid')
					->whereRaw('users.id= team.userid')
					->whereRaw("projects.id = $id");
			})
			->get();
		$countRestUsers = count($restUsers);
		$countRestModules = count($restModules);
		return View::make('project.projectmanagement', array('project' => $project, 'modules' => $modules, 'restModules' => $restModules, 'users' => $users, 'restUsers' => $restUsers, 'countRestModules' => $countRestModules, 'countRestUsers' => $countRestUsers));
	}

	public function postAssignmodules()
	{
		$params = Input::all();

		$modules = $params['modules'];
		$projectId = $params['projectId'];
		$insertingArray = array();
		foreach ($modules as $each) {

			array_push($insertingArray, array('module_id' => $each, 'project_id' => $projectId));
		}
		$status = DB::table('module_project')->insert($insertingArray);
		if ($status) {
			return Response::json(array('message' => "Modules Assigned Successfully!", 'status' => 'success'));
		} else {
			return Response::json(array('message' => "Could Not Assign Modules.", 'status' => 'failure'));
		}
	}

	public function postAddusers()
	{
		$params = Input::all();

		$users = $params['users'];
		$projectId = $params['projectId'];
		$insertingArray = array();
		foreach ($users as $each) {

			array_push($insertingArray, array('userid' => $each, 'projectid' => $projectId));
		}

		$status = DB::table('team')->insert($insertingArray);
		if ($status) {
			return Response::json(array('message' => "Users Assigned Successfully!", 'status' => 'success'));
		} else {
			return Response::json(array('message' => "Could Not Assign Users.", 'status' => 'failure'));
		}
	}

	public function postProjectname()
	{
		$params = Input::all();
		if ($params['newName'] != '') {
			$project = Project::find($params['projectId']);
			$project->title = $params['newName'];
			$project->save();
			$status = true;
		} else {
			$status = false;
		}
		if ($status) {
			return Response::json(array('message' => "Project Name Changed Successfully!", 'status' => 'success'));
		} else {
			return Response::json(array('message' => "Could Not Change Name.", 'status' => 'failure'));
		}

	}

	public function getModules($id)
	{
		$project = Project::find($id);
		$modules = $project->modules;
		if (Input::has('api_key')) {

			foreach ($modules as $module) {
				//$task->time_spent = $task->total_time;
				$module->save();
			}
			return Response::json($modules);
		}

	}

	public function postAddcomponent($id)
	{
		$params = Input::all();
		//var_dump($params); exit;
		$component = new Component($params);
		//$component->title = $params['title'];
		$project = Project::find($id);
		$component->project()->associate($project);
		$status = $component->save();
		if ($status) {
			return Response::json(array('message' => "Component added successfully"));
		} else {
			return Response::json(array('message' => "Could Not add component."));
		}
	}

	public function getGraph($id)
	{
		$query =
			"SELECT u.first_name AS Name,
						(SELECT SUM( time_spent ) FROM time_logs
							WHERE
							user_id = u.id
							AND
							task_id IN (SELECT id FROM tasks WHERE project_id ={$id})) as Time
			FROM users u";

		$hours = DB::select($query);

		$i = 0;

		foreach ($hours as $u) {
			$time = $u->Time ? $u->Time : 0.0;
			$results[$i] = array($u->Name, floatval($time));
			$names[$i] = $u->Name;
			$i++;
		}
		return array('values' => json_encode($results), 'names' => json_encode($names));
	}

	public function deleteModule($id)
	{
		$params = Input::all();
		$project = Project::find($params['project_id']);
		$status = $project->modules()->detach($id);

		return array('status' => $status);
	}

}
