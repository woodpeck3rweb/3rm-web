<?php


use Illuminate\Mail\Message;

class TaskController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		//
	}

	public static function getCurrent()
	{
		$tasks = Task::with('assignee')->whereHas("timelogs", function($q) {
			$q->where('end_time', '=', '0000-00-00 00:00:00')
				->where('start_time', '!=', '0000-00-00 00:00:00');
		})->orWhere("status", "=", "progress")->get();

		return $tasks;
	}

	public function getEmail()
	{
		$params = Input::all();
		$to_id = $params['to'];
		$task_id = $params['task'];

		//Assigned To User
		$toUser = User::find($to_id);
		$to = $toUser->email;

		//Task assigned
		$task = Task::find($task_id)->load('project');

		$params = Input::all();
		$user = Auth::user();
		Mail::queue('emails.task_assigned',
			array("to_user"=>$toUser, "task"=>$task),
			function (Message $message) use ($to, $user) {
				$today = date("F j, Y");
				$message->to($to)->subject($user->first_name.' assigned a task to you');
			}
		);

		return Response::json(array("message"=>1));

//		return View::make("emails.task_assigned");
	}

	public function sendTaskEmail($to_id, $task_id)
	{
		//Assigned To User
		$toUser = User::find($to_id);
		if($toUser == null) {
			Log::alert("Unable to assign for task with id ".$task_id);
			return;
		}
		Log::debug($toUser->toArray());
		$to = $toUser->email;

		//Task assigned
		$task = Task::find($task_id)->load('project');

		$user = Auth::user();
		Mail::queue('emails.task_assigned',
			array("to_user"=>$toUser, "task"=>$task),
			function (Message $message) use ($to, $user) {
				$message->to($to)->subject($user->first_name.' assigned a task to you');
			}
		);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		//
		$params = Input::all();

		$v = Task::validate($params);
		if ($v->passes()) {
			$task = new Task($params);
			if (Input::has('user_id')) {
				$user = User::find($params['user_id']);
				if ($user == null) {
					return Response::json(array("message" => "No User associated with the id provided"));
				}
				$task->user()->associate($user);
			} else {
				$user = Auth::user();
				$task->user()->associate($user);
			}

			//Module Selection
			if (Input::has('module_id')) {
				$module = Module::find($params['module_id']);
				$task->module()->associate($module);
			} else {
				$module = Module::where('title', '=', 'Development')->get()->first();
				$module = Module::find($module->id);
				$task->module()->associate($module);
			}

			//Status
			if (!Input::has('status')) {
				$task->status = "Open";
			}

			//Priority
			if (!Input::has('priority')) {
				$task->priority = "Major";
			}

			//Type
			if (!Input::has('type')) {
				$task->type = "Task";
			}

			$status = $task->save();

			if ($status) {

				if(array_key_exists("assignee_id", $params)) {
					$this->sendTaskEmail($params['assignee_id'], $task->id);
				}

				return Response::json(array('message'=>"Task creation success !", 'task'=>$task));
			}
			return Response::json(array('message' => "Sorry, unable to create Task", 'params' => $params));
		} else {
			return Response::json(array('message' => $v->messages()->toArray()));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function getShow($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		//
	}

    public function postAssignmilestone(){
        $params = Input::all();
        $task = Task::find($params['taskId']);
        $milestone = $params['milestoneId'];
        $milestone = Milestone::find($milestone);
        $task->milestone()->associate($milestone);
        if($task->save()){
            $status = 'milestone attached successfully.';
        }else{
            $status = 'could not attach milestone.';
        }
        $task->touch();
        return Response::json(array('status' => $status));

    }
    public function postDeletetask(){
        $params = Input::all();

        if(Task::destroy($params['taskId'])){
            $status = "Task deleted successfully.";
        }else{
            $status = "Could Not Delete Task.";
        }
        return Response::json(array('status' => $status));

    }
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		//
		$params = Input::all();
		$task = Task::find($id);

		if (Input::has('name') && Input::has('value')) {
			$param = $params['name'];
			$fields[$param] = $params['value'];
		} else {
			$fields = $params;
		}

		if(array_key_exists("assignee_id", $fields)) {
			$this->sendTaskEmail($fields['assignee_id'], $id);
		}

		$status = $task->update($fields);
		return Response::json(array('status' => $status));
	}

	public function postTime($id)
	{
		$params = Input::all();

		//Calculating time
		if (Input::has("time_spent")) {
			$time_spent = $params['time_spent'];

			if (is_numeric($time_spent)) {
				$time = $time_spent;
			} else {
				$time = substr($time_spent, 0, strlen($time_spent) - 1);
				$unit = substr($time_spent, strlen($time_spent) - 1, 1);

				switch ($unit) {
					case "M":
					case "m":
						$time = $time / 60.0;
						break;
					case "H":
					case "h":
						//Make no changes, because default time unit is Hours
						break;
					default:
						break;
				}
			}

			$params['time_spent'] = $time;
		}

		$task = Task::find($id);
		if ($task == null) {
			return Response::json(array("message"=>"No Task associated with the given id"));
		}

		if (Input::has('user_id')) {
			$user = User::find($params['user_id']);
			if ($user == null) {
				return Response::json(array("message" => "No User associated with the given id"));
			}
		} else {
			$user = Auth::user();
		}

		$timeLog = new TimeLog($params);
		$timeLog->user()->associate($user);
		$status = $task->timelogs()->save($timeLog);


		$task->time_spent = $task->total_time;
		$task->save();

		if ($status) {
			return Response::json(array("message"=>"Time logged"));
		}
		return Response::json(array("message"=>"Time log failed, try again"), 400);
	}

	public function getComments($id)
	{
		$comments = Comment::with('user')->whereTask_id($id)->orderBy("created_at", 'desc')->get();
		return Response::json(array("results"=> $comments));
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function postDestroy($id)
	{
		//
	}
}
