<?php

use Illuminate\Mail\Message;

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		//
		$comments = Comment::all();
		return Response::json(array("results"=>$comments));
	}

	public function getEmail($task_id, $comment_id)
	{

		$comment = Comment::find($comment_id);

		//Task assigned
		$task = Task::find($task_id)->load('project');

		if(is_null($task->assignee_id)) {
			return;
		} else {
			$to_id = $task->assignee_id;
		}
		//Assigned To User
		$toUser = User::find($to_id);
		$to = $toUser->email;


		$user = Auth::user();
		Mail::queue('emails.commented',
			array("user"=>$user, "to_user" => $toUser,
				"task"=> $task, "comment"=>$comment),
			function (Message $message) use ($to, $user) {
				$message->to($to)->subject($user->first_name.' commented on a task');
			}
		);

//		return Response::view("emails.commented", array("user"=>$user, "to_user" => $toUser,
//			"task"=> $task, "comment"=>$comment));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		//
		$params = Input::all();

		if (!Input::has('user_id')) {
			if(Auth::check()) {
				$params['user_id'] = Auth::user()->id;
			}
		}

		$v = Comment::validate($params);
		if($v->fails()) {
			return Response::json($v->messages()->toArray());
		}

		$comment = new Comment($params);
		$status = $comment->save();
		if($status) {
			$comment = $comment->load('user');
			$this->getEmail($params["task_id"], $comment->id);
		}
		return Response::json(array("comment"=>$comment, "status"=>$status));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getShow($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		//

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postDestroy($id)
	{
		//
		$result = Comment::destroy($id);
		return Response::json(array("status"=> $result));
	}
}
