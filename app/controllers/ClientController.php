<?php

class ClientController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		//
		return Response::json(Client::all());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function postCreate()
	{
		//
		//
		$params = Input::all();

		$v = Client::validate($params);
		if($v->passes()) {
           // echo "bye1"; exit;
				$client = Client::create($params);
			$status = $client->save();

			if ($status) {
                return Redirect::to('allclients');
				//return Response::json(array('message'=>"Client creation success !", 'client'=>$client));
			}
			return Response::json(array('message'=>"Sorry, unable to create", 'params'=>$params));
		} else
        {
            //echo "bye2"; exit;
			return Response::json(array('message'=>$v->messages()->toArray()));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getShow($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		//
		$client = Client::find($id);

		$params = Input::all();
		$status = $client->update($params);

		if ($status) {
			return Response::json(array("message"=>"Update succeeded"));
		}
		return Response::json(array("message"=>"Update failed"), 400);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postDestroy($id)
	{
		//
	}


}
