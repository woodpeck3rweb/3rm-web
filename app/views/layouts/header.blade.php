
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>3RM - Dashboard</title>


    <!-- Maniac stylesheets -->
    {{HTML::style('css/bootstrap.min.css')}}
    {{HTML::style('css/font-awesome.min.css')}}
    {{HTML::style('css/gritter/jquery.gritter.css')}}
    {{HTML::style('css/bootstrap-tagsinput/bootstrap-tagsinput.css')}}
    {{HTML::style('css/jquery-jvectormap/jquery-jvectormap-1.2.2.css')}}
    {{HTML::style('css/animate/animate.min.css')}}
    {{HTML::style('css/iCheck/all.css')}}
    {{HTML::style('css/style.css')}}

    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="fixed">
<!-- Header -->
<header>
    <a href="index-2.html" class="logo"> <i class="fa fa-bolt"></i><span>3RM</span></a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="navbar-btn sidebar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-header">
            <form role="search" class="navbar-form" method="post" action="#">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                </div>
            </form>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown dropdown-notifications">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell"></i><span class="label label-warning">5</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header"><i class="fa fa-bell"></i>  You have 5 new notifications</li>
                        <li>
                            <ul>
                                <li><a href="#"><i class="fa fa-users success"></i> New user registered</a></li>
                                <li><a href="#"><i class="fa fa-heart info"></i> Jane liked your post</a></li>
                                <li><a href="#"><i class="fa fa-envelope warning"></i> You got a message from Jean</a></li>
                                <li><a href="#"><i class="fa fa-info success"></i> Privacy settings have been changed</a></li>
                                <li><a href="#"><i class="fa fa-comments danger"></i> New comments waiting approval</a></li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all notification</a></li>
                    </ul>
                </li>

                <li class="dropdown dropdown-messages">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope"></i><span class="label label-success">6</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header"><i class="fa fa-envelope"></i> You have 6 messages</li>
                        <li>
                            <ul>
                                <li>
                                    <a href="#">
                                        <div class="pull-left"><img src="img/avatar2.jpg" class="img-rounded" alt="image"/></div>
                                        <h4>Jill Doe<small><i class="fa fa-clock-o"></i> 1 mins</small></h4>
                                        <p>Can we meet somewhere?</p>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <div class="pull-left"><img src="img/avatar.jpg" class="img-rounded" alt="image"/></div>
                                        <h4>John Doe<small><i class="fa fa-clock-o"></i> 2 mins</small></h4>
                                        <p>Please send me a new email.</p>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <div class="pull-left"><img src="img/avatar3.jpg" class="img-rounded" alt="image"/></div>
                                        <h4>Jeremy Doe<small><i class="fa fa-clock-o"></i> 30 mins</small></h4>
                                        <p>Don't forget to subscribe to...</p>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <div class="pull-left"><img src="img/avatar4.jpg" class="img-rounded" alt="image"/></div>
                                        <h4>Jean Doe<small><i class="fa fa-clock-o"></i> 2 hours</small></h4>
                                        <p>I sent you a mail about me.</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all messages</a></li>
                    </ul>
                </li>

                <li class="dropdown dropdown-tasks">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-tasks"></i><span class="label label-danger">1</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header"><i class="fa fa-tasks"></i>  You have 1 new task</li>
                        <li>
                            <ul>
                                <li>
                                    <a href="#">
                                        <h3>PHP Developing<small class="pull-right">32%</small></h3>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success" style="width: 32%" role="progressbar" aria-valuenow="32" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <h3>Database Repair<small class="pull-right">14%</small></h3>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-warning" style="width: 14%" role="progressbar" aria-valuenow="14" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <h3>Backup Create<small class="pull-right">65%</small></h3>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" style="width: 65%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <h3>Create New Modules<small class="pull-right">80%</small></h3>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">View all tasks</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown widget-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="img/avatar.jpg" class="pull-left" alt="image" />
                        <span>Madhav <i class="fa fa-caret-down"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ URL::to('settings') }}"><i class="fa fa-cog"></i>Settings</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('profile') }}"><i class="fa fa-user"></i>Profile</a>
                        </li>
                        <li class="footer">
                            <a href="{{ URL::to('logout') }}"><i class="fa fa-power-off"></i>Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- /.header -->

<!-- wrapper -->
<div class="wrapper">
    <div class="leftside">
        <div class="sidebar">
            <div class="user-box">
                <div class="avatar">
                    <img src="img/avatar.jpg" alt="" />
                </div>
                <div class="details">
                    <p>Madhav</p>
                    <span class="position">Admin</span>
                </div>
                <div class="button">
                    <a href="{{ URL::to('login') }}"><i class="fa fa-power-off"></i></a>
                </div>
            </div>
            <ul class="sidebar-menu">
                <li class="active">
                    <a href="{{ URL::to('home') }}">
                        <i class="fa fa-home"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="sub-nav">
                    <a href="#">
                        <i class="fa fa-briefcase"></i>
                        <span>Projects</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{ URL::to('project/all') }}">All</a></li>
                        <li><a href="{{ URL::to('project/new') }}">New</a></li>
                        <li><a href="{{ URL::to('') }}">Manage</a></li>
                    </ul>
                </li>
                <li class=" sub-nav">
                    <a href="#">
                        <i class="fa fa-briefcase"></i>
                        <span>Clients</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{ URL::to('allclients') }}">All</a></li>
                        <li><a href="{{ URL::to('newclient') }}">New</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ URL::to('users') }}">
                        <i class="fa fa-users"></i> <span>Users</span>
                    </a>
                </li>
                <li>
                    <a href="{{ URL::to('reports') }}">
                        <span>Reports</span>
                    </a>
                </li>
                <li>
                    <a href="{{ URL::to('leavemanagement') }}">
                        <span>Leave Management</span>
                    </a>
                </li>

                <li>
                    <a href="{{ URL::to('calendar') }}">
                        <i class="fa fa-calendar"></i> <span>Calendar</span>
                    </a>
                </li>
                <li class="sub-nav">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Other Pages</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="404.blade.php">404 Error</a></li>
                        <li><a href="layouts/blank.blade.php">Blank Page</a></li>
                        <li><a href="invoice.blade.php">Invoice</a></li>
                        <li><a href="login.blade.php">Login</a></li>
                        <li><a href="register.blade.php">Register</a></li>
                        <li><a href="lockscreen.blade.php">Lockscreen</a></li>
                        <li><a href="timeline.blade.php">Timeline</a></li>
                    </ul>
                </li>
            </ul>

                </div>
            </div>
        </div>

    <div class="rightside">

        <div class="content">
            <div class="row">
                @yield('content')
        </div>
    </div>
</div><!-- /.wrapper -->

<!-- Javascript -->
{{ HTML::script('js/plugins/jquery/jquery-1.10.2.min.js'); }}
{{ HTML::script('js/plugins/jquery-ui/jquery-ui-1.10.4.min.js'); }}
{{ HTML::script('js/plugins/bootstrap/bootstrap.min.js'); }}
{{ HTML::script('js/plugins/gritter/jquery.gritter.min.js'); }}
{{ HTML::script('js/plugins/flot/jquery.flot.min.js'); }}
{{ HTML::script('js/plugins/flot/jquery.flot.resize.min.js'); }}
{{ HTML::script('js/plugins/flot/jquery.flot.pie.min.js'); }}
{{ HTML::script('s/plugins/flot/jquery.flot.stack.min.js'); }}
{{ HTML::script('js/plugins/flot/jquery.flot.crosshair.min.js'); }}
{{ HTML::script('js/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js'); }}
{{ HTML::script('js/plugins/jquery-jvectormap/jquery-jvectormap-europe-merc-en.js'); }}
{{ HTML::script('js/plugins/jquery-countTo/jquery.countTo.js'); }}
{{ HTML::script('js/plugins/slimScroll/jquery.slimscroll.min.js'); }}
{{ HTML::script('js/plugins/pace/pace.min.js'); }}
{{ HTML::script('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js'); }}
{{ HTML::script('js/plugins/iCheck/icheck.min.js'); }}
{{ HTML::script('js/custom.js'); }}


@yield('script')
</body>
</html>
