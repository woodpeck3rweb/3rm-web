<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>SPARK</title>

	<!-- Maniac stylesheets -->
	{{HTML::style('css/bootstrap.min.css')}}
	{{HTML::style('css/font-awesome.min.css')}}
	{{HTML::style('css/animate/animate.min.css')}}
	{{HTML::style('css/iCheck/all.css')}}
	{{HTML::style('css/style.css')}}

	@yield('styles')

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- start Mixpanel --><script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
			for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
		mixpanel.init("{{Config::get("mixpanel.app_id")}}");</script><!-- end Mixpanel -->

</head>
<body class="fixed  wysihtml5-supported pace-done">
<!-- Header -->
<header>
	<a href="{{ URL::to('home') }}" class="logo"><i class="fa fa-bolt"></i> <span>SPARK</span></a>
	<nav class="navbar navbar-static-top">
		<a href="#" class="navbar-btn sidebar-toggle">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>

		<div class="navbar-header">
			<form role="search" class="navbar-form" method="post" action="#">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
								</button>
                            </span>
				</div>
			</form>
		</div>
		<div class="navbar-right">
			<ul class="nav navbar-nav">

				<li>
					<a onclick="" href="#" data-toggle="modal" data-target="#daily-status">
						Send Status
					</a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Projects<span class="caret"></span></a>

					<ul class="dropdown-menu">
						<li class="header"><i class="glyphicon glyphicon-list"></i> Projects List</li>
						<li>
							<div class="slimScrollDiv">
								<ul class="list-group">
									<?php
									$projects = Project::all();
									?>
									@forelse ($projects as $proj)
									<li class="list-group-item"><a href="{{ URL::to('project/tasks/') }}/{{$proj->id}}">{{$proj->title}}</a>
									</li>
									@empty
									<li class="list-group-item">No Projects !! Start creating projects</li>
									<li class="divider"></li>
									@endforelse
								</ul>
								<div class="slimScrollBar"
									 style="width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; height: 180px; background: rgb(0, 0, 0);">

								</div>
								<div class="slimScrollRail"
									 style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div>
							</div>
						</li>
						<li class="footer"><a href="{{ URL::to('project/all/') }}">View all Projects</a></li>
					</ul>
				</li>

				<li class="dropdown widget-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img
							src="{{strlen(Auth::user()->picture) == 0 ? URL::to('img/avatar.jpg') : Auth::user()->picture}}"
							class="pull-left" alt="image"/>
						<span>{{Auth::user()->first_name}} <i class="fa fa-caret-down"></i></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="{{ URL::to('settings') }}"><i class="fa fa-cog"></i>Settings</a>
						</li>
						<li>
							<a href="{{ URL::to('profile') }}"><i class="fa fa-user"></i>Profile</a>
						</li>
						<li class="footer">
							<a href="{{ URL::to('user/logout') }}"><i class="fa fa-power-off"></i>Logout</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
<!-- /.header -->

<!-- wrapper -->
<div class="wrapper">
	<div class="leftside">
		<div class="sidebar">
			<div class="user-box">
				<div class="avatar">
					<img
						src="{{strlen(Auth::user()->picture) == 0 ? URL::to('img/avatar.jpg') : Auth::user()->picture}}"
						alt=""/>
				</div>
				<div class="details">
					<p>{{{Auth::user()->first_name}}}</p>
				</div>
			</div>
			<ul class="sidebar-menu">
				<li id="sidebar-dashboard">
					<a href="{{ URL::to('home') }}">
						<i class="fa fa-home"></i> <span>Dashboard</span>
					</a>
				</li>
				<li id="sidebar-projects">
					<a href="{{ URL::to('project/all') }}">
						<i class="fa fa-briefcase"></i> <span>Projects</span>
					</a>
				</li>
				<li id="sidebar-clients">
					<a href="{{ URL::to('allclients') }}">
						<i class="fa fa-briefcase"></i>
						<span>Clients</span>
					</a>
				</li>
				<li id="sidebar-users">
					<a href="{{ URL::to('users') }}">
						<i class="fa fa-users"></i> <span>Users</span>
					</a>
				</li>
				<li id="sidebar-reports">
					<a href="{{ URL::to('reports') }}">
						<i class="fa fa-files-o"></i> <span>Reports</span>
					</a>
				</li>
				<li id="sidebar-leave-management">
					<a href="{{ URL::to('leavemanagement') }}">
						<i class="glyphicon glyphicon-user"></i> <span>Leave Management</span>
					</a>
				</li>
				<li id="sidebar-finance">
					<a href="{{ URL::to('finance') }}">
						<i class="fa fa-dollar"></i><span>Finance</span>
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="rightside">
		<!--		<div class="page-head">-->
		<!--			<h1>Blank-->
		<!--				<small>small text goes here</small>-->
		<!--			</h1>-->
		<!--		</div>-->

		<div class="content">
			<!-- Main row -->
			@yield('main-content')
			<div class="row">
				@yield('content')
			</div>
			<!-- /.row -->
		</div>
	</div>
	<!-- /.wrapper -->
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Create Event</h4>
			</div>
			<div class="modal-body">
				<div class="input-group">
					<input type="text" id="new-event" class="form-control" placeholder="Event">

					<div class="input-group-btn">
						<button type="button" id="color-chooser-btn" class="btn btn-info dropdown-toggle"
								data-toggle="dropdown">Type <span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" id="color-chooser">
							<li><a class="palette-turquoise" href="#">Turquoise</a></li>
							<li><a class="palette-green-sea" href="#">Green sea</a></li>
							<li><a class="palette-emerald" href="#">Emerald</a></li>
							<li><a class="palette-nephritis" href="#">Nephritis</a></li>
							<li><a class="palette-peter-river" href="#">Peter River</a></li>
							<li><a class="palette-belize-hole" href="#">Belize hole</a></li>
							<li><a class="palette-amethyst" href="#">Amethyst</a></li>
							<li><a class="palette-wisteria" href="#">Wisteria</a></li>
							<li><a class="palette-wet-asphalt" href="#">Wet asphalt</a></li>
							<li><a class="palette-midnight-blue" href="#">Midnight blue</a></li>
							<li><a class="palette-sun-flower" href="#">Sun Flower</a></li>
							<li><a class="palette-orange" href="#">Orange</a></li>
							<li><a class="palette-carrot" href="#">Carrot</a></li>
							<li><a class="palette-pumpkin" href="#">Pumpkin</a></li>
							<li><a class="palette-alizarin" href="#">Alizarin</a></li>
							<li><a class="palette-pomegranate" href="#">Pomegranate</a></li>
							<li><a class="palette-concrete" href="#">Concrete</a></li>
							<li><a class="palette-asbestos" href="#">Asbestos</a></li>
						</ul>
					</div>
					<!-- /btn-group -->
				</div>
				<!-- /input-group -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close <i
						class="fa fa-times"></i></button>
				<button type="button" class="btn btn-success pull-left" id="add-new-event" data-dismiss="modal"><i
						class="fa fa-plus"></i> Add
				</button>
			</div>
		</div>
	</div>
</div>

<!-- Daily Status -->
<div class="modal fade" id="daily-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
						class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel"><i class="fa fa-envelope"></i>Daily Status</h4>
			</div>
			<div id="daily-status-info" class="alert alert-danger alert-dismissible fade in"
				 role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
						aria-hidden="true">×</span></button>
				<h4>Oh snap! You forgot to stop timer !</h4>

				<p>Please go to the respective tasks and stop the timer.</p>
				<!--				<p>-->
				<!--					<button type="button" class="btn btn-danger">Ok</button>-->
				<!--					<button type="button" class="btn btn-default">I've done that</button>-->
				<!--				</p>-->
			</div>
			<div class="modal-body">
				<div class="box">
					<div class="box-body">
						<form method="get">
							<div>
								<textarea id="status_message" placeholder="Write today's status.." class="form-control"
										  rows="11"></textarea>
							</div>
						</form>
					</div>
					<div class="box-footer clearfix">
						<button id="statusEmailButton" class="pull-left btn btn-success" type="button">Send <i
								class="fa fa-arrow-circle-right"></i></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Javascript -->
{{ HTML::script('js/plugins/jquery/jquery-2.1.0.min.js'); }}
{{HTML::script('js/plugins/jquery-ui/jquery-ui-1.10.4.min.js')}}

<!-- Bootstrap -->
{{HTML::script('js/plugins/bootstrap/bootstrap.min.js')}}

<!-- Interface -->
{{HTML::script('js/plugins/slimScroll/jquery.slimscroll.min.js')}}
{{HTML::script('js/plugins/pace/pace.min.js')}}

<!-- Forms -->
{{HTML::script('js/custom.js')}}

<!-- Script to show active Row-->

<script type="text/javascript">
	$(document).ready(function () {

		//Handling Active Page
		var url = window.location.href;
		var origin = window.location.origin;
		url.replace(origin, '');

		var highlight_item = 'dashboard';

		if (url.indexOf('project') > -1) {
			highlight_item = 'projects';
		} else if (url.indexOf('clients') > -1) {
			highlight_item = 'clients';
		} else if (url.indexOf('users') > -1) {
			highlight_item = 'users';
		} else if (url.indexOf('reports') > -1) {
			highlight_item = 'reports';
		} else if (url.indexOf('leavemanagement') > -1) {
			highlight_item = 'leave-management';
		}

		$('#sidebar-' + highlight_item).addClass('active');	//Check for ids in blank template

		console.log(highlight_item);

		$('#daily-status').on('show.bs.modal', function (event) {
			//Status check
			$.ajax({
				url: "{{ URL::to('/user/isworking') }}",
				type: "GET",
				dataType: "json",
				success: function (data) {
					console.log(data.status);

					if (!data.status) {
						console.log("Hide error");
						$("#daily-status-info").hide();
					} else {
						console.log("Dont hide the error");
						$("#daily-status-info").show();
					}
				}
			});
		});

		//	Sending Daily Status Email
		$("#statusEmailButton").click(function (event) {
			mixpanel.track("Daily Status");
			var message = $("#status_message").val();

			if (message.length > 0) {
				var url = "{{ URL::to('/sendstatus')}}";
				$.ajax({
					url: url,
					type: "GET",
					dataType: "json",
					data: {
						message: message
					},
					success: function (data) {
						$("#status_message").val("");
						$('#daily-status').modal('hide');
						alert("Email sent");
					}
				}).fail(function () {
					alert("Sorry there is some problem sending your email.");
				});
				event.preventDefault();
			} else {
				alert("Sending no status ?");
			}
		});
	});
</script>
<script type="text/javascript">
	mixpanel.track_links("a", "click nav link", {
		"referrer": document.referrer
	});
	mixpanel.track_forms("form", "form");

	mixpanel.identify("{{Auth::id()}}");
	<?php
		$user = Auth::user()->toArray();
	?>
	mixpanel.people.set({{ json_encode($user) }});
</script>
<!-- User Scripts -->
@yield('script')
</body>
</html>