<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Cache-Control" content="public">

	<title>Projects Detailed View</title>

	<!-- Maniac stylesheets -->
	{{ HTML::style('css/bootstrap.min.css'); }}
	{{ HTML::style('css/font-awesome.min.css'); }}
	{{ HTML::style('css/animate/animate.min.css'); }}
	{{ HTML::style('css/style.css'); }}

	@yield('styles')

	<!-- start Mixpanel --><script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
			for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
		mixpanel.init("{{Config::get("mixpanel.app_id")}}");</script><!-- end Mixpanel -->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="fixed">
<!-- Header -->
<header>
	<a href="{{ URL::to('home') }}" class="logo logo1" title="{{ $project->title}}"><i class="fa fa-home"></i> <span>{{{ substr($project->title, 0, 10).".." }}}</span></a>
	<nav class="navbar navbar-static-top">
		<a href="#" class="navbar-btn sidebar-toggle">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>

		<div class="navbar-header">
			<form role="search" class="navbar-form" method="post" action="#">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search..."/>
                    	<span class="input-group-btn">
                        	<button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
							</button>
                        </span>
				</div>
			</form>
		</div>


		<div class="navbar-right">
			<ul class="nav navbar-nav">
				<li style="margin-top: -8px;"><a href="#cant-do-all-the-work-for-you" data-toggle="tooltip"
												 data-placement="top">
						<button type="button" class="btn btn-info">+</button>
					</a></li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Projects<span class="caret"></span></a>

					<ul class="dropdown-menu">
						<li class="header"><i class="glyphicon glyphicon-list"></i> Projects List</li>
						<li style="overflow: scroll;height: 200px;overflow-x:hidden;">
							<div class="slimScrollDiv">
								<ul class="list-group">
									<?php
									$projects = Project::all();
									?>
									@forelse ($projects as $proj)
									<li class="list-group-item"><a href="{{ URL::to('project/tasks/') }}/{{$proj->id}}">{{$proj->title}}</a></li>
									@empty
									<li class="list-group-item">No Projects !! Start creating projects</li>
									<li class="divider"></li>
									@endforelse
								</ul>
								<div class="slimScrollBar" style="width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; height: 180px; background: rgb(0, 0, 0);">

								</div>
								<div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div></div>
						</li>
						<li class="footer"><a href="{{ URL::to('project/all/') }}">View all Projects</a></li>
					</ul>
				</li>

				<li class="dropdown widget-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img
							src="{{strlen(Auth::user()->picture) == 0 ? URL::to('img/avatar.jpg') : Auth::user()->picture}}"
							class="pull-left" alt="image"/>
						<span>{{Auth::user()->first_name}} <i class="fa fa-caret-down"></i></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="{{ URL::to('settings') }}"><i class="fa fa-cog"></i>Settings</a>
						</li>
						<li>
							<a href="{{ URL::to('profile') }}"><i class="fa fa-user"></i>Profile</a>
						</li>
						<li class="footer">
							<a href="{{ URL::to('user/logout') }}"><i class="fa fa-power-off"></i>Logout</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
<!-- /.header -->

<!-- wrapper -->
<div class="wrapper">
	<div class="leftside project">
		<div class="sidebar">

			<ul class="sidebar-menu side">
<!--				<li id="project-summary">-->
<!--					<a href="{{ URL::to('project/show/') }}/{{$project->id}}">-->
<!--						<i class="glyphicon glyphicon-edit"></i> <span>Summary</span>-->
<!--					</a>-->
<!--				</li>-->
				<li id="project-tasks">
					<a href="{{ URL::to('project/tasks/') }}/{{$project->id}}">
						<i class="glyphicon glyphicon-tasks"></i> <span>Tasks</span>
					</a>
				</li>
				<li id="project-reports">
					<a href="{{ URL::to('project/reports/') }}/{{$project->id}}">
						<i class="fa fa-files-o"></i><span>Reports</span>
					</a>
				</li>
				<li id="project-milestones">
					<a href="{{ URL::to('project/milestones/') }}/{{$project->id}}">
						<i class="glyphicon glyphicon-map-marker"></i> <span>Milestones</span>
					</a>
				</li>
				<li id="project-components">
					<a href="{{ URL::to('project/components/') }}/{{$project->id}}">
						<i class="glyphicon glyphicon-random"></i> <span>Components</span>
					</a>
				</li>
				<li id="project-projectmanagement">
					<a href="{{ URL::to('project/projectmanagement') }}/{{ $project->id}}">
						<i class="fa fa-cog"></i> <span> Manage</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="rightside">

	<div class="content">
		<!-- Main row -->
		<div class="row">

			@yield('content')
		</div>
	</div>
	<!-- /.box -->
</div>
<!-- /.wrapper -->


</form>
<div id="cant-do-all-the-work-for-you" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
	 aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md ">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title text-center" id="mySmallModalLabel">Create Task</h4>
			</div>
			<div class="modal-body">
				<form id="create-task-form" method="post">
					<div class=" col-sm-12">
						<div class="form-group col-md-12">
							<?php
							//Gettings all Users
							$projects = Project::all(array('id', 'title'));
							?>
							<select id="Project" class="form-control">
								<option value="000">-- Select Project --</option>
								@foreach ($projects as $p)
								@if($project->id == $p->id)
								<option selected="selected" value="{{$p->id}}">{{$p->title}}</option>
								@else
								<option value="{{$p->id}}">{{$p->title}}</option>
								@endif
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-12">
							<input id="Title" type="text" name='title' placeholder='Title' class="form-control"/>
						</div>
						<div class="form-group col-md-12">
							<textarea id="Description" class="form-control" name="description" placeholder='Description'
									  style="height: 80px;"></textarea>
							<hr/>
						</div>
						<div class="form-group col-md-12">
							<select id="Type" class="form-control">
								<option value="Task">Task</option>
								<option value="Bug">Bug</option>
								<option value="Improvement">Improvement</option>
								<option value="New Feature">New Feature</option>
							</select>
						</div>
						<div class="form-group col-md-12">
							<select id="Priority" class="form-control">
								<option value="Blocker">Blocker</option>
								<option value="Critical">Critical</option>
								<option selected="selected" value="Major">Major</option>
								<option value="Minor">Minor</option>
								<option value="Trivial">Trivial</option>
							</select>
						</div>
						<div class="form-group col-md-12">
							<select id="AssignedTo" class="form-control">
								<?php
								//Gettings all Users
								$users = User::all(array('id', 'first_name', 'last_name'));
								?>
								<option value="00">Assigned To</option>
								@foreach ($users as $user)
								<option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-12">
							<select id="Module" class="form-control">
								<?php
								//Gettings all Users
								$modules = Module::all(['id', 'title']);
								?>
								@foreach ($modules as $m)
								<option value="{{$m->id}}">{{$m->title}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-12">
							ETA:
							<input id="ETA" type="number" name='title' placeholder='ETA' class="form-control"/>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary pull-left">Cancel</button>
						<button type="submit" class="btn btn-success pull-right">Create</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


{{ HTML::script('js/plugins/jquery/jquery-2.1.0.min.js'); }}
{{ HTML::script('js/plugins/jquery-ui/jquery-ui-1.10.4.min.js'); }}
{{ HTML::script('js/plugins/bootstrap/bootstrap.min.js'); }}
{{ HTML::script('js/plugins/jquery-countTo/jquery.countTo.js'); }}
{{ HTML::script('js/plugins/slimScroll/jquery.slimscroll.min.js'); }}
{{ HTML::script('js/plugins/pace/pace.min.js'); }}
{{ HTML::script('js/custom.js'); }}

<script type="text/javascript">
	$(function () {

		//Handling Active Page
		var url = window.location.href;
		var origin = window.location.origin;
		url.replace(origin, '');

		var highlight_item = '';

		if (url.indexOf('show') > -1) {
			highlight_item = 'summary';
		} else if (url.indexOf('tasks') > -1) {
			highlight_item = 'tasks';
		} else if (url.indexOf('milestones') > -1) {
			highlight_item = 'milestones';
		} else if (url.indexOf('reports') > -1) {
			highlight_item = 'reports';
		} else if (url.indexOf('finance') > -1) {
			highlight_item = 'finance';
		} else if (url.indexOf('components') > -1) {
			highlight_item = 'components';
		}

		$('#project-' + highlight_item).addClass('active');	//Check for ids in this template

		console.log(highlight_item);

		$('#create-task-form').submit(function (event) {
			mixpanel.track("Task Creation");

			var task_create_url = "{{URL::to('task/create')}}";
			console.log(task_create_url);

			var project_id = $('#Project').val();
			var title = $('#Title').val();
			var description = $('#Description').val();
			var type = $('#Type').val();
			var priority = $('#Priority').val();
			var assignee_id = $('#AssignedTo').val();
			var eta = $('#ETA').val();
			var module_id = $('#Module').val();

			console.log(title, module_id);

			$.post(task_create_url,
				{
					title: title,
					description: description,
					type: type,
					priority: priority,
					eta: eta,
					assignee_id: assignee_id,
					project_id: project_id,
					module_id: module_id
				},
				function (data, status) {
					console.log(status);
					console.log(data);
					$('#cant-do-all-the-work-for-you').modal('hide');
				}
			)
				.fail(function (data) {
					mixpanel.track("Task Creation Failed");
					console.log("error");
					console.log(data);
					alert(data);
				});
			event.preventDefault();
		});

		$('[data-toggle="tooltip"]').tooltip();

		$('a[href="#cant-do-all-the-work-for-you"]').on('click', function (event) {
			event.preventDefault();
			$('#cant-do-all-the-work-for-you').modal('show');
		});

		$('[data-command="toggle-search"]').on('click', function (event) {
			event.preventDefault();
			$(this).toggleClass('hide-search');

			if ($(this).hasClass('hide-search')) {
				$('.c-search').closest('.row').slideUp(100);
			} else {
				$('.c-search').closest('.row').slideDown(100);
			}
		});

	});
</script>

<script type="text/javascript">
	mixpanel.track_links("a", "click nav link", {
		"referrer": document.referrer
	});
	mixpanel.track_forms("form", "project form");
</script>

@yield('script')
</body>
</html>