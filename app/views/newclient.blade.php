@extends('layouts.blank')

@section('content')


<div class="col-md-12">
<div class="col-md-6 pull-left">
<div class="box">
<div class="box-title">
    <h3>New Client</h3>
</div>
<!-- /.box-title -->
<div class="box-body">
<div class="col-md-12">
<form role="form" id="client-form" method="post">
<div class="form-group col-md-6">
    <input id="FirstName" type="text" name='name0' placeholder='First Name' class="form-control"/>
</div>
<div class="form-group col-md-6">
    <input id="LastName" type="text" name='name0' placeholder='Last Name' class="form-control"/>
</div>
<div class="form-group col-md-12">
    <input id="FullName" type="text" name='name0' placeholder='Full Name' class="form-control"/>
</div>
<div class="form-group col-md-12">
    <input id="Email" type="text" name='email' placeholder='Email' class="form-control"/>
</div>
<div class="form-group col-md-6">
    <input id="Picture" type="file" name='picture' placeholder='Picture' class="form-control"/>
</div>
<div class="form-group col-md-6">
    <input id="Mobile" type="text" name='phone' placeholder='Mobile' class="form-control"/>
</div>

<div class="form-group col-md-4">
    <input id="City" type="text" name='city' placeholder='City' class="form-control"/>
</div>
<div class="form-group col-md-4">
    <input id="State" type="text" name='state' placeholder='State' class="form-control"/>
</div>
<div class="form-group col-md-4">
<select id="Country" class="form-control">
<option value="AFG">Country</option>
<option value="AFG">Afghanistan</option>
<option value="ALA">Åland Islands</option>
<option value="ALB">Albania</option>
<option value="DZA">Algeria</option>
<option value="ASM">American Samoa</option>
<option value="AND">Andorra</option>
<option value="AGO">Angola</option>
<option value="AIA">Anguilla</option>
<option value="ATA">Antarctica</option>
<option value="ATG">Antigua and Barbuda</option>
<option value="ARG">Argentina</option>
<option value="ARM">Armenia</option>
<option value="ABW">Aruba</option>
<option value="AUS">Australia</option>
<option value="AUT">Austria</option>
<option value="AZE">Azerbaijan</option>
<option value="BHS">Bahamas</option>
<option value="BHR">Bahrain</option>
<option value="BGD">Bangladesh</option>
<option value="BRB">Barbados</option>
<option value="BLR">Belarus</option>
<option value="BEL">Belgium</option>
<option value="BLZ">Belize</option>
<option value="BEN">Benin</option>
<option value="BMU">Bermuda</option>
<option value="BTN">Bhutan</option>
<option value="BOL">Bolivia, Plurinational State of</option>
<option value="BES">Bonaire, Sint Eustatius and Saba</option>
<option value="BIH">Bosnia and Herzegovina</option>
<option value="BWA">Botswana</option>
<option value="BVT">Bouvet Island</option>
<option value="BRA">Brazil</option>
<option value="IOT">British Indian Ocean Territory</option>
<option value="BRN">Brunei Darussalam</option>
<option value="BGR">Bulgaria</option>
<option value="BFA">Burkina Faso</option>
<option value="BDI">Burundi</option>
<option value="KHM">Cambodia</option>
<option value="CMR">Cameroon</option>
<option value="CAN">Canada</option>
<option value="CPV">Cape Verde</option>
<option value="CYM">Cayman Islands</option>
<option value="CAF">Central African Republic</option>
<option value="TCD">Chad</option>
<option value="CHL">Chile</option>
<option value="CHN">China</option>
<option value="CXR">Christmas Island</option>
<option value="CCK">Cocos (Keeling) Islands</option>
<option value="COL">Colombia</option>
<option value="COM">Comoros</option>
<option value="COG">Congo</option>
<option value="COD">Congo, the Democratic Republic of the</option>
<option value="COK">Cook Islands</option>
<option value="CRI">Costa Rica</option>
<option value="CIV">Côte d'Ivoire</option>
<option value="HRV">Croatia</option>
<option value="CUB">Cuba</option>
<option value="CUW">Curaçao</option>
<option value="CYP">Cyprus</option>
<option value="CZE">Czech Republic</option>
<option value="DNK">Denmark</option>
<option value="DJI">Djibouti</option>
<option value="DMA">Dominica</option>
<option value="DOM">Dominican Republic</option>
<option value="ECU">Ecuador</option>
<option value="EGY">Egypt</option>
<option value="SLV">El Salvador</option>
<option value="GNQ">Equatorial Guinea</option>
<option value="ERI">Eritrea</option>
<option value="EST">Estonia</option>
<option value="ETH">Ethiopia</option>
<option value="FLK">Falkland Islands (Malvinas)</option>
<option value="FRO">Faroe Islands</option>
<option value="FJI">Fiji</option>
<option value="FIN">Finland</option>
<option value="FRA">France</option>
<option value="GUF">French Guiana</option>
<option value="PYF">French Polynesia</option>
<option value="ATF">French Southern Territories</option>
<option value="GAB">Gabon</option>
<option value="GMB">Gambia</option>
<option value="GEO">Georgia</option>
<option value="DEU">Germany</option>
<option value="GHA">Ghana</option>
<option value="GIB">Gibraltar</option>
<option value="GRC">Greece</option>
<option value="GRL">Greenland</option>
<option value="GRD">Grenada</option>
<option value="GLP">Guadeloupe</option>
<option value="GUM">Guam</option>
<option value="GTM">Guatemala</option>
<option value="GGY">Guernsey</option>
<option value="GIN">Guinea</option>
<option value="GNB">Guinea-Bissau</option>
<option value="GUY">Guyana</option>
<option value="HTI">Haiti</option>
<option value="HMD">Heard Island and McDonald Islands</option>
<option value="VAT">Holy See (Vatican City State)</option>
<option value="HND">Honduras</option>
<option value="HKG">Hong Kong</option>
<option value="HUN">Hungary</option>
<option value="ISL">Iceland</option>
<option value="IND">India</option>
<option value="IDN">Indonesia</option>
<option value="IRN">Iran, Islamic Republic of</option>
<option value="IRQ">Iraq</option>
<option value="IRL">Ireland</option>
<option value="IMN">Isle of Man</option>
<option value="ISR">Israel</option>
<option value="ITA">Italy</option>
<option value="JAM">Jamaica</option>
<option value="JPN">Japan</option>
<option value="JEY">Jersey</option>
<option value="JOR">Jordan</option>
<option value="KAZ">Kazakhstan</option>
<option value="KEN">Kenya</option>
<option value="KIR">Kiribati</option>
<option value="PRK">Korea, Democratic People's Republic of</option>
<option value="KOR">Korea, Republic of</option>
<option value="KWT">Kuwait</option>
<option value="KGZ">Kyrgyzstan</option>
<option value="LAO">Lao People's Democratic Republic</option>
<option value="LVA">Latvia</option>
<option value="LBN">Lebanon</option>
<option value="LSO">Lesotho</option>
<option value="LBR">Liberia</option>
<option value="LBY">Libya</option>
<option value="LIE">Liechtenstein</option>
<option value="LTU">Lithuania</option>
<option value="LUX">Luxembourg</option>
<option value="MAC">Macao</option>
<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
<option value="MDG">Madagascar</option>
<option value="MWI">Malawi</option>
<option value="MYS">Malaysia</option>
<option value="MDV">Maldives</option>
<option value="MLI">Mali</option>
<option value="MLT">Malta</option>
<option value="MHL">Marshall Islands</option>
<option value="MTQ">Martinique</option>
<option value="MRT">Mauritania</option>
<option value="MUS">Mauritius</option>
<option value="MYT">Mayotte</option>
<option value="MEX">Mexico</option>
<option value="FSM">Micronesia, Federated States of</option>
<option value="MDA">Moldova, Republic of</option>
<option value="MCO">Monaco</option>
<option value="MNG">Mongolia</option>
<option value="MNE">Montenegro</option>
<option value="MSR">Montserrat</option>
<option value="MAR">Morocco</option>
<option value="MOZ">Mozambique</option>
<option value="MMR">Myanmar</option>
<option value="NAM">Namibia</option>
<option value="NRU">Nauru</option>
<option value="NPL">Nepal</option>
<option value="NLD">Netherlands</option>
<option value="NCL">New Caledonia</option>
<option value="NZL">New Zealand</option>
<option value="NIC">Nicaragua</option>
<option value="NER">Niger</option>
<option value="NGA">Nigeria</option>
<option value="NIU">Niue</option>
<option value="NFK">Norfolk Island</option>
<option value="MNP">Northern Mariana Islands</option>
<option value="NOR">Norway</option>
<option value="OMN">Oman</option>
<option value="PAK">Pakistan</option>
<option value="PLW">Palau</option>
<option value="PSE">Palestinian Territory, Occupied</option>
<option value="PAN">Panama</option>
<option value="PNG">Papua New Guinea</option>
<option value="PRY">Paraguay</option>
<option value="PER">Peru</option>
<option value="PHL">Philippines</option>
<option value="PCN">Pitcairn</option>
<option value="POL">Poland</option>
<option value="PRT">Portugal</option>
<option value="PRI">Puerto Rico</option>
<option value="QAT">Qatar</option>
<option value="REU">Réunion</option>
<option value="ROU">Romania</option>
<option value="RUS">Russian Federation</option>
<option value="RWA">Rwanda</option>
<option value="BLM">Saint Barthélemy</option>
<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
<option value="KNA">Saint Kitts and Nevis</option>
<option value="LCA">Saint Lucia</option>
<option value="MAF">Saint Martin (French part)</option>
<option value="SPM">Saint Pierre and Miquelon</option>
<option value="VCT">Saint Vincent and the Grenadines</option>
<option value="WSM">Samoa</option>
<option value="SMR">San Marino</option>
<option value="STP">Sao Tome and Principe</option>
<option value="SAU">Saudi Arabia</option>
<option value="SEN">Senegal</option>
<option value="SRB">Serbia</option>
<option value="SYC">Seychelles</option>
<option value="SLE">Sierra Leone</option>
<option value="SGP">Singapore</option>
<option value="SXM">Sint Maarten (Dutch part)</option>
<option value="SVK">Slovakia</option>
<option value="SVN">Slovenia</option>
<option value="SLB">Solomon Islands</option>
<option value="SOM">Somalia</option>
<option value="ZAF">South Africa</option>
<option value="SGS">South Georgia and the South Sandwich Islands</option>
<option value="SSD">South Sudan</option>
<option value="ESP">Spain</option>
<option value="LKA">Sri Lanka</option>
<option value="SDN">Sudan</option>
<option value="SUR">Suriname</option>
<option value="SJM">Svalbard and Jan Mayen</option>
<option value="SWZ">Swaziland</option>
<option value="SWE">Sweden</option>
<option value="CHE">Switzerland</option>
<option value="SYR">Syrian Arab Republic</option>
<option value="TWN">Taiwan, Province of China</option>
<option value="TJK">Tajikistan</option>
<option value="TZA">Tanzania, United Republic of</option>
<option value="THA">Thailand</option>
<option value="TLS">Timor-Leste</option>
<option value="TGO">Togo</option>
<option value="TKL">Tokelau</option>
<option value="TON">Tonga</option>
<option value="TTO">Trinidad and Tobago</option>
<option value="TUN">Tunisia</option>
<option value="TUR">Turkey</option>
<option value="TKM">Turkmenistan</option>
<option value="TCA">Turks and Caicos Islands</option>
<option value="TUV">Tuvalu</option>
<option value="UGA">Uganda</option>
<option value="UKR">Ukraine</option>
<option value="ARE">United Arab Emirates</option>
<option value="GBR">United Kingdom</option>
<option value="USA">United States</option>
<option value="UMI">United States Minor Outlying Islands</option>
<option value="URY">Uruguay</option>
<option value="UZB">Uzbekistan</option>
<option value="VUT">Vanuatu</option>
<option value="VEN">Venezuela, Bolivarian Republic of</option>
<option value="VNM">Viet Nam</option>
<option value="VGB">Virgin Islands, British</option>
<option value="VIR">Virgin Islands, U.S.</option>
<option value="WLF">Wallis and Futuna</option>
<option value="ESH">Western Sahara</option>
<option value="YEM">Yemen</option>
<option value="ZMB">Zambia</option>
<option value="ZWE">Zimbabwe</option>
</select>
</div>
<div class="form-group col-md-12">
    <textarea id="Description" class="form-control" name="description" placeholder='Description' style="height: 100px;"></textarea>
</div>
<div class="col-md-1 col-md-offset-5" style="margin-bottom: 20px;">
    <button type="submit" class="btn btn-sm btn-success">Submit</button>
</div>
</form>
</div>

</div>

<div class="clearfix"></div>
</div>
</div>


<div class="col-md-6 pull-right">
    <div class="box">
        <div class="box-title">
            <h3>Clients</h3>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Project</th>
                    <th>Mobile</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($clients as $client)
                <tr>
                    <td>{{$client->full_name}}</td>
                    <td>{{$client->email}}</td>
                    <td>NA</td>
                    <td>{{$client->phone}}</td>
                </tr>
                @endforeach
                </tbody>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
</div>
<!-- /.wrapper -->
<!-- Dashboard -->
<script type="text/javascript">
    (function ($) {
        "use strict";
        // number count
        $('.timer').countTo();

        //TagsInput
        $("[data-toggle='tags']").tagsinput({
            tagClass: function (item) {
                return 'label label-primary';
            }
        });

        // chat scroll
        $('#chat-box').slimScroll({
            height: '250px'
        });

        //iCheck
        $("input[type='checkbox'], input[type='radio']").iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });

        // ToDo
        $('#checkbox').on('ifChecked', function (event) {
            $('.check').addClass('through')
        });
        $('#checkbox').on('ifUnchecked', function (event) {
            $('.check').removeClass('through')
        });

        // gritter
        setTimeout(function () {
            $.gritter.add({
                title: 'You have one new task for today',
                text: 'Go and check <a href="mailbox.html" class="text-warning">tasks</a> to see what you should do.<br/> Check the date and today\'s tasks.'
            });
        }, 2000);

        // flot
        var v1 = [
            [1, 50],
            [2, 53],
            [3, 40],
            [4, 55],
            [5, 47],
            [6, 39],
            [7, 44],
            [8, 55],
            [9, 43],
            [10, 61],
            [11, 52],
            [12, 57],
            [13, 64],
            [14, 54],
            [15, 49],
            [16, 55],
            [17, 53],
            [18, 57],
            [19, 68],
            [20, 71],
            [21, 84],
            [22, 72],
            [23, 88],
            [24, 74],
            [25, 87],
            [26, 83],
            [27, 76],
            [28, 86],
            [29, 93],
            [30, 95]
        ];
        var v2 = [
            [1, 13],
            [2, 18],
            [3, 14],
            [4, 25],
            [5, 23],
            [6, 17],
            [7, 20],
            [8, 26],
            [9, 24],
            [10, 27],
            [11, 32],
            [12, 37],
            [13, 32],
            [14, 28],
            [15, 25],
            [16, 21],
            [17, 25],
            [18, 33],
            [19, 30],
            [20, 27],
            [21, 35],
            [22, 28],
            [23, 29],
            [24, 28],
            [25, 34],
            [26, 27],
            [27, 40],
            [28, 29],
            [29, 33],
            [30, 45]
        ];
        var C = ["#7fb9d1", "#e65353"];
        var plot = $.plot("#placeholder", [
            { data: v1, label: "Total Visitors", lines: {fillColor: "#f8fcfd"}},
            { data: v2, label: "Unique Visitors", lines: {fillColor: "#fdf8f8"}}
        ], {
            series: {
                lines: {
                    show: true,
                    fill: true
                },
                points: {
                    show: true
                },
                shadowSize: 0
            },
            grid: {
                hoverable: true,
                clickable: true,
                aboveData: true,
                borderWidth: 0
            },
            legend: {
                noColumns: 0,
                margin: [0, -23],
                labelBoxBorderColor: null
            },
            colors: C,
            tooltip: true
        });

        function showTooltip(x, y, contents) {
            $("<div id='flot_tip'>" + contents + "</div>").css({
                top: y - 20,
                left: x + 5
            }).appendTo("body").fadeIn(200);
        }

        var previousPoint = null;
        $("#placeholder").bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;
                    $("#flot_tip").remove();
                    var x = item.datapoint[0].toFixed(0),
                        y = item.datapoint[1].toFixed(0);
                    showTooltip(item.pageX, item.pageY,
                        y + " " + item.series.label + " on the " + x + "th");
                }
            } else {
                $("#flot_tip").remove();
                previousPoint = null;
            }
        });

        // jvectormap
        $('#map').vectorMap({
            map: 'europe_merc_en',
            zoomMin: '3',
            backgroundColor: "#fff",
            focusOn: { x: 0.5, y: 0.7, scale: 3 },
            markers: [
                {latLng: [42.5, 1.51], name: 'Andorra'},
                {latLng: [43.73, 7.41], name: 'Monaco'},
                {latLng: [47.14, 9.52], name: 'Liechtenstein'},
                {latLng: [41.90, 12.45], name: 'Vatican City'},
                {latLng: [43.93, 12.46], name: 'San Marino'},
                {latLng: [35.88, 14.5], name: 'Malta'}
            ],
            markerStyle: {
                initial: {
                    fill: '#fa4547',
                    stroke: '#fa4547',
                    "stroke-width": 6,
                    "stroke-opacity": 0.3
                }
            },
            regionStyle: {
                initial: {
                    fill: '#e4e4e4',
                    "fill-opacity": 1,
                    stroke: 'none',
                    "stroke-width": 0,
                    "stroke-opacity": 1
                }
            }
        });
    })(jQuery);
</script>
@stop
