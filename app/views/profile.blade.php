@extends('layouts.blank')

@section('content')
<div class="col-md-10">
	<div class="box">
		<div class="box-body">
			<ul class="nav nav-tabs nav-justified nav_bg">
				<li class="active"><a href="#about" data-toggle="tab"><i class="fa fa-user"></i> About</a></li>
				<li class=""><a href="#edit-profile" data-toggle="tab"><i class="fa fa-pencil"></i> Edit</a></li>
				<li class=""><a href="#user-activities" data-toggle="tab"><i class="fa fa-laptop"></i> Activities</a>
				</li>
			</ul>
			<div class="tab-content" style=" background-color: #ffffff;">
				<div class="tab-pane animated fadeInRight active" id="about">
					<div class="user-profile-content active">
						<h5><strong>ABOUT</strong> ME</h5>

						<p>{{$user->userdetails->about_me}}</p>
                        <p><img src="{{strlen($user->picture) == 0 ? URL::to('img/avatar.jpg') : $user->picture}}" id="profile_image" height="128px" width="128px"/> </p>
                        <p><input type="file" id="profile"/> </p>

                        <hr>
						<div class="row">
							<div class="col-sm-6">
								<h5><strong>CONTACT ME :</strong></h5>
								<address>
									<strong>Phone</strong><br>
									<abbr title="Phone" id="phone" class="profileedit" contenteditable="true">+91 354 123 4567</abbr>
								</address>
								<address>
									<strong>Email</strong><br>
									<a href="mailto:#" contenteditable="true" id="email" class="profileedit">{{$user->email}}</a>
								</address>
								<address>
									<strong>Website</strong><br>
									<a href="{{$user->userdetails->website}}"contenteditable="true"id="website" class="profileedit">{{$user->userdetails->website}}</a>
								</address>
							</div>
							<div class="col-sm-6" >
								<h5><strong>MY SKILLS :</strong></h5>

								<p contenteditable="true"id="skills1" class="profileedit">UI Design</p>

								<p contenteditable="true"id="skills2" class="profileedit">Clean and Modern Web Design</p>

								<p contenteditable="true"id="skills3" class="profileedit">PHP and MySQL Programming</p>

								<p contenteditable="true"id="skills4" class="profileedit">Vector Design</p>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane animated fadeInRight" id="edit-profile">
					<div class="user-profile-content">
						<form role="form" id="profile-form" method="post">
							<div class="form-group col-md-6">
								<label for="FirstName">First Name</label>
								<input type="text" class="form-control" id="FirstName" value="{{$user->first_name}}">
							</div>
							<div class="form-group col-md-6">
								<label for="LastName">Last Name</label>
								<input type="text" class="form-control" id="LastName" value="{{$user->last_name}}">
							</div>
							<div class="form-group col-md-6">
								<label for="EmpID">Employee ID</label>
								<input type="text" class="form-control" id="EmpID" value="{{$user->employee_id}}">
							</div>
							<div class="form-group col-md-6">
								<label for="Designation">Designation</label>
								<input type="text" class="form-control" id="Designation" value="{{$user->designation}}">
							</div>
							<div class="form-group col-md-12">
								<label for="Email">Email</label>
								<input type="email" class="form-control" id="Email" value="{{$user->email}}">
							</div>
							<div class="form-group col-md-6">
								<label for="Password">Password</label>
								<input type="password" class="form-control" id="Password"
									   placeholder="******">
							</div>
							<div class="form-group col-md-6">
								<label for="RePassword">Confirm Password</label>
								<input type="password" class="form-control" id="RePassword"
									   placeholder="******">
							</div>
							<div class="form-group col-md-6">
								<label for="Phone">Phone</label>
								<input type="text" class="form-control" id="Phone" value="{{$user->userdetails->phone}}"
									   placeholder="Enter Phone">
							</div>
							<div class="form-group col-md-6">
								<label for="Web">Web</label>
								<input type="url" class="form-control" id="Web" value="{{$user->userdetails->website}}"
									   placeholder="Enter Web">
							</div>
							<div class="form-group col-md-12">
								<label for="AboutMe">About Me</label>
								<textarea class="form-control" id="AboutMe" style="height: 125px;">{{$user->userdetails->about_me}}</textarea>
							</div>
							<button type="submit" class="btn btn-success">Save</button>
						</form>
					</div>
				</div>
				<div class="tab-pane" id="user-activities">
					<ul class="media-list">
						<li class="media"><a href="#">
								<p><strong>John Doe</strong> Uploaded a photo <strong>"DSC000254.jpg"</strong> <br>
									<i>2 minutes ago</i></p>
							</a></li>
						<li class="media"><a href="#">
								<p><strong>Imran Tahir</strong> Created an photo album <strong>"Indonesia
																							   Tourism"</strong> <br>
									<i>8 minutes ago</i></p>
							</a></li>
						<li class="media"><a href="#">
								<p><strong>Colin Munro</strong> Posted an article <strong>"London never ending
																						  Asia"</strong> <br>
									<i>an hour ago</i></p>
							</a></li>
						<li class="media"><a href="#">
								<p><strong>Corey Anderson</strong> Added 3 products <br>
									<i>3 hours ago</i></p>
							</a></li>
						<li class="media"><a href="#">
								<p><strong>Morne Morkel</strong> Send you a message <strong>"Lorem ipsum
																							dolor..."</strong> <br>
									<i>12 hours ago</i></p>
							</a></li>
						<li class="media"><a href="#">
								<p><strong>Imran Tahir</strong> Updated his avatar <br>
									<i>Yesterday</i></p>
							</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="box-footer">
		</div>
	</div>
	<div class="block-web full">
		<!--/tab-content-->
	</div>
	<!--/block-web-->
</div>
@section('script')
<script type="text/javascript">
	$("#profile-form").submit(function (event) {

		var first_name = $('#FirstName').val();
		var last_name = $('#LastName').val();
		var about_me = $('#AboutMe').val();
		var phone = $('#Phone').val();
		var web = $('#Web').val();
		var designation = $('#Designation').val();
		var employee_id = $('#EmpID').val();

		console.log(first_name, last_name, about_me);

		var url = "{{URL::to('user/update/')}}";
		url = url+"/{{$user->id}}";

		console.log(url);
		$.post(url,
			{
				first_name: first_name,
				last_name: last_name,
				about_me: about_me,
				phone: phone,
				website: web,
				designation: designation,
				employee_id: employee_id
			},
			function (data, status) {
				console.log(data + " -- " + status);
				console.log("Successfully changed -- ", data);
			}
		)
			.fail(function (data) {
				console.log("error");
				console.log(data);

				if (data.status == 401) {
					console.log("Nope.");
				} else {
					console.log("Something went wrong.");
				}
			})
			.always(function () {
				console.log("finished");
			});

		event.preventDefault();
	});
    $('#profile').change(function(){

        var base_url = window.location.origin;
        var file = $("#profile").get(0).files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;
        var image_url = {};

        console.log(file +" - "+ name +" - "+ size  +" - "+ type);

        var reader = new FileReader();
        var _this = this;
        reader.onload = function(event){
            console.log(event.target.result);
            image_url = {image_base64:event.target.result, image_loaded:true};
            alert("pic uploaded");
            $.ajax({
                type: "POST",
                url: base_url + "/spark/public/user/update"+"/{{$user->id}}",
                data: {
                    picture:image_url['image_base64']
                } , //serialize form
                success: function (msg) {
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }

            });
        };
        reader.readAsDataURL(file);
    });
    $(window).load(function(){

    });
    $(document).ready(function(){
        $("profileedit").keypress(function(){

           if(event.keyCode == 13){

               var myId = $(this).attr('id');
               if(myId == 'email'){
                   key = 'email' ;
               }else if(myId == ''){
                   key = '';
               }
               var myVal = $(this).val();
               $.ajax({
                   type: "POST",
                   url: base_url + "/spark/public/user/update"+"/{{$user->id}}",
                   data: {

                       key:key,
                       value:myVal
                   } , //serialize form
                   success: function (msg) {
                       window.location.reload();
                   },
                   error: function (data) {
                       console.log(data);
                   }

               });


           }
        });
    });
</script>
@stop
@stop