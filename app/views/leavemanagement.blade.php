@extends('layouts.blank')

<link rel="stylesheet" href="css/fullcalendar/fullcalendar.css" />
@section('content')

<!-- Main row -->
	<div class="col-lg-10">
		<div class="box">
			<div class="box-title">
				<i class="fa fa-signal"></i>
				<h3>Leave Mangement</h3>
				<div class="pull-right box-toolbar">
				</div>
			</div>
			<div class="box-body">
                <table class="table">
                    <tbody>
                    <tr>
                    <th>Leave Type</th>
                    <th>From Date</th>
                    <th>End Date</th>
                    <th>Reason</th>
                    <th>Send</th>
                    </tr>
                    <tr>
                    <td> <select class="selectpicker form-control " id="selectpriority">
                            <optgroup label="Priority">
                                <option value="Major">Leave type</option>
                                <option value="Minor">Casual Leave</option>
                                <option value="Critical">Sick Leave</option>
                                <option value="Blocker">Earned Leave</option>
                                <option value="Blocker">Paid Leave</option>
                                <option value="Trivial">Compensatory Off</option>
                                <option value="Trivial"> Maternity Leave</option>
                                <option value="Trivial"> Marriage Leave</option>
                            </optgroup>
                        </select>
                    </td>
                    <td> <input class="form-control" type="date" value="From Date"/> </td>
                    <td> <input class="form-control" type="date" value="End Date"/></td>
                    <td> <textarea class="form-control" > </textarea></td>
                    <td> <input type="button" class="btn btn-primary btn-sm" value="send"/></td>
                    </tr>

                    </tbody>
                </table>
			</div>
		</div>
	</div>

	<div class="col-md-10">
		<div class="box">
			<div class="box-title">
				<i class="fa fa-map-marker"></i>
				<h3>Leave Calender</h3>
				<div class="pull-right box-toolbar">
				</div>
			</div>
			<div class="box">
                <div class="box-body">
                    <div id="calendar"></div>
            </div>
		</div>
	</div>

	<div class="clearfix"></div>
</div>



	</div>
</div>

@stop

@section ('script')


<script src="js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function(){
        //iCheck
        $("input[type='checkbox'], input[type='radio']").iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
            ele.each(function() {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        }
        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear();
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                prev: "<span class='fa fa-caret-left'></span>",
                next: "<span class='fa fa-caret-right'></span>",
                today: 'today',
                month: 'month',
                week: 'week',
                day: 'day'
            },
            //Random events
            events: [
                {
                    title: 'Casual Leave by Raghu',
                    start: new Date(y, m, 2),
                    backgroundColor: "#27ae60"
                },
                {
                    title: 'Sick Leave by Madhav',
                    start: new Date(y, m, d - 2),
                    end: new Date(y, m, d - 2),
                    backgroundColor: "#27ae60"
                },
                {
                    title: 'Compensatory off by Raghu',
                    start: new Date(y, m, 6),
                    backgroundColor: "#27ae60"
                },
                {
                    title: 'Casual Leave by Raghu',
                    start: new Date(y, m, 19),
                    backgroundColor: "#27ae60"
                }
            ],
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function(date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                copiedEventObject.backgroundColor = $(this).css("background-color");
                copiedEventObject.borderColor = $(this).css("border-color");

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            }
        });

        /* ADDING EVENTS */
        var currColor = "#e74c3c"; //default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function(e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("background-color");
            //Add color effect to button
            colorChooser
                .css({"background-color": currColor, "border-color": currColor})
                .html($(this).text()+' <span class="caret"></span>');
        });
        $("#add-new-event").click(function(e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            //Create event
            var event = $("<div />");
            event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
            event.html(val);
            $('#external-events').prepend(event);

            //Add draggable funtionality
            ini_events(event);

            //Remove event from text input
            $("#new-event").val("");
        });
    });
</script>

@stop