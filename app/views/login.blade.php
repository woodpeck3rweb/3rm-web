<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- start Mixpanel -->
	<script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
			for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
		mixpanel.init("{{Config::get("mixpanel.app_id")}}");</script>
	<!-- end Mixpanel -->

	<title>SPARK - Login</title>

	<!-- Maniac stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/animate/animate.min.css"/>
	<link rel="stylesheet" href="css/bootstrapValidator/bootstrapValidator.min.css"/>
	<link rel="stylesheet" href="css/iCheck/all.css"/>
	<link rel="stylesheet" href="css/style.css"/>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="login fixed">
<div class="wrapper animated flipInY">
	<div class="logo"><a href="#"><span>SPARK</span></a></div>
	<div class="box">
		<div class="header clearfix">
			<div class="pull-left"><i class="fa fa-sign-in"></i> Sign In</div>
		</div>
		<form id="loginform" method="post">
			<div class="alert alert-warning no-radius no-margin padding-sm"><i class="fa fa-info-circle"></i> Please
																											  sign in to
																											  SPARK
																											  dashboard
			</div>
			<div class="box-body padding-md">
				<div class="form-group">
					<div class="col-lg-12">
						<div class="input-group" style="margin-bottom: 20px;">
							<input type="text" id="email" name="email" class="form-control" placeholder="Email">
							<span class="input-group-addon">@kronch.co</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-12" style="margin-bottom: 20px;">
						<input type="password" id="password" name="password" class="form-control"
							   placeholder="Password">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-6" style="margin-bottom: 20px;">
						<input type="checkbox"/>
						<small>Remember me</small>
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-success btn-block">Sign in</button>
					<a id="google-signin" class="btn btn-block btn-social btn-google-plus" href="{{URL::to('/glogin?type=google')}}">
						<i class="fa fa-google-plus"></i> Sign in with Woodpeck3r's Google Account
					</a>
				</div>
			</div>
		</form>
	</div>
	<div class="box-extra clearfix">
		<a href="{{ URL::to('forgotpassword') }}" class="pull-left btn btn-xs">Forgotten Password?</a>
		<a href="{{ URL::to('register') }}" class="pull-right btn  btn-xs">Register an account</a>
	</div>

	<footer>
		Copyright &copy; 2014 by Woodpeck3r.

		<ul class="list-unstyled list-inline">
			<li><a href="#">About Us</a></li>
			<li><a href="#">Contact</a></li>
		</ul>
	</footer>
</div>

<!-- Javascript -->
<script src="js/plugins/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/plugins/jquery-ui/jquery-ui-1.10.4.min.js" type="text/javascript"></script>

<!-- Bootstrap -->
<script src="js/plugins/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<!-- Interface -->
<script src="js/plugins/pace/pace.min.js" type="text/javascript"></script>

<!-- Forms -->
<script src="js/plugins/bootstrapValidator/bootstrapValidator.min.js" type="text/javascript"></script>
<script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

<script type="text/javascript">
	mixpanel.track_links("#google-signin", "Google Login");
	//iCheck
	$("input[type='checkbox'], input[type='radio']").iCheck({
		checkboxClass: 'icheckbox_minimal',
		radioClass: 'iradio_minimal'
	});

	$(document).ready(function () {
		$('#loginform').bootstrapValidator({
			message: 'This value is not valid',
			fields: {
				username: {
					message: 'The username is not valid',
					validators: {
						notEmpty: {
							message: 'The username is required and can\'t be empty'
						}
					}
				},
				password: {
					validators: {
						notEmpty: {
							message: 'The password is required and can\'t be empty'
						}
					}
				}
			}
		});

		$('#loginform').submit(function (event) {
			mixpanel.track("Login");

			console.log("Hello");

			var email = $('#email').val() + "@kronch.co";
			var pwd = $('#password').val();

			console.log("Pwd " + pwd + " Email " + email);

			if (pwd == "" && email == "") {
				console.log("Provide Email and Password");
				showError("Provide Email and Password");
			} else {
				$.post("{{URL::to('user/login');}}",
					{
						email: email,
						password: pwd
					},
					function (data, status) {
						console.log(data + " -- " + status);

						var base_url = window.location.origin;
						console.log("Successfully Logged In -- " + base_url);
						window.location.href = base_url + "/spark/public/home";
					}
				)
					.fail(function (data) {
						console.log("error");
						console.log(data);
						if (data.status == 401) {
							console.log("Wrong credentials.");
						} else {
							console.log("Something went wrong.");
						}
					})
					.always(function () {
						console.log("finished");
					});
			}

			event.preventDefault();
		});
	});
</script>
</body>
</html>