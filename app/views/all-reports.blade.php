@extends('layouts.blank')

@section('styles')
{{ HTML::style('css/3rm.css'); }}
@stop

@section('main-content')
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="box">
			<div class="box-title">
				<h3>My Week Stats</h3>
			</div>
			<div class="box-body">
				<div id="container"></div>
			</div>
		</div>
	</div>
</div>

<!-- Dashboard -->
@stop

@section('script')
{{ HTML::script('js/plugins/highcharts/highcharts.js'); }}

<script type="text/javascript">

	$(function () {
		var options = {
			chart: {
				renderTo: 'container',
				type: 'column'
			},
			title: {
				text: ''
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0,
					color: "#0099FF"
				}
			},
			legend: {
				enabled: false
			},
			xAxis: {},
			yAxis: {
				min: 0,
				title: {
					text: 'Hours'
				}
			},
			series: [
				{}
			]
		};

		options.series[0].name = "Hours";
		options.series[0].data = {{ json_encode($graph['values']) }};

	options.xAxis.categories = {{ json_encode($graph['days']) }};

	var chart = new Highcharts.Chart(options);


	});
</script>
@stop