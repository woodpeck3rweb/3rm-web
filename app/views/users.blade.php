@extends('layouts.blank')

@section('styles')
{{HTML::style('css/datatables/dataTables.bootstrap.css')}}
{{HTML::style('css/style.css')}}

@stop

@section('content')
<div class="col-md-12">
	<div class="box">
		<div class="box-title">
			<h3>Users</h3>
		</div>
		<div class="box-body">
			<table id="users" class="table table-bordered table-hover">
				<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Platform</th>
					<th>Joined Date</th>
					<th>Designation</th>
					<th>Mobile No</th>
					<th>PersonalMail Id</th>
					<th>Alternate Mobile No</th>
				</tr>
				</thead>
				<tbody>
				@foreach ($users as $user)
				<option value="{{$user->id}}"></option>
				<tr>
					<td>{{$user->employee_id}}</td>
					<td>{{$user->first_name}} {{$user->last_name}}</td>
					<td>NA</td>
					<td> 20/12/2013</td>
					<td>{{$user->designation}}</td>
					<td>{{$user->mobileno}}</td>
					<td>{{$user->personalmail_id}}</td>
					<td>{{$user->Alternateno}}</td>
				</tr>
				@endforeach
				</tbody>
				<tfoot>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Platform</th>
					<th>Joined Date</th>
					<th>Designation</th>
                    <th>Mobile No</th>
                    <th>PersonalMail Id</th>
                    <th>Alternate Mobile No</th>
				</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Forms -->
{{ HTML::script('js/plugins/datatables/jquery.dataTables.js'); }}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js'); }}
{{ HTML::script('js/custom.js'); }}

<script type="text/javascript">
	$(document).ready(function () {
		$('#users').dataTable({
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false
		});
	});
</script>
@stop