@extends('layouts.blank')

@section('content')
<div class="rightside">
    <div class="container clear_both padding_fix">
        <section class="panel panel-default">
            <div class="panel-heading bg_color">Layout Options</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>Refer below to know all available features.</p>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Options</th>
                                    <th>CSS (Apply class in Body)</th>
                                    <th>JS API</th>
                                    <th>Try</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td width="180">fixed_header</td>
                                    <td><code>fixed_header</code></td>
                                    <td><code>fixed_header(true);</code></td>
                                    <td><button type="button" class="btn btn-primary btn-xs btn-mini">Yes</button></td>
                                </tr>
                                <tr>
                                    <td>left_nav_fixed</td>
                                    <td><code>left_nav_fixed</code></td>
                                    <td><code>left_nav_fixed(true);</code></td>
                                    <td><button type="button" class="btn btn-primary btn-xs btn-mini">Yes</button></td>
                                </tr>
                                <tr>
                                    <td>right_nav</td>
                                    <td><code>right_nav</code></td>
                                    <td><code>right_nav(true);</code></td>
                                    <td><button type="button" class="btn btn-primary btn-xs btn-mini">Yes</button></td>
                                </tr>
                                <tr>
                                    <td>boxed_layout</td>
                                    <td><code>boxed_layout</code></td>
                                    <td><code>boxed_layout(true);</code></td>
                                    <td><button type="button" class="btn btn-primary btn-xs btn-mini">Yes</button></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <p>Contact/message me to report any issues.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="panel panel-default">
            <div class="panel-heading bg_color">theme Options</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>Refer below to know all available features.</p>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Options</th>
                                    <th>CSS (Apply class in Body)</th>
                                    <th>JS API</th>
                                    <th>Try</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td width="180">red_thm</td>
                                    <td><code>red_thm</code></td>                                    <td><code>red_thm(true);</code></td>
                                    <td><button class="btn btn-primary btn-xs btn-mini" type="button">Apply here</button></td>
                                </tr>
                                <tr>
                                    <td>blue_thm</td>
                                    <td><code>blue_thm</code></td>
                                    <td><code>blue_thm(true);</code></td>
                                    <td><button class="btn btn-primary btn-xs btn-mini" type="button">Apply here</button></td>
                                </tr>
                                <tr>
                                    <td>green_thm</td>
                                    <td><code>green_thm</code></td>
                                    <td><code>green_thm(true);</code></td>
                                    <td><button class="btn btn-primary btn-xs btn-mini" type="button">Apply here</button></td>
                                </tr>
                                <tr>
                                    <td>magento_thm</td>
                                    <td><code>magento_thm</code></td>
                                    <td><code>magento_thm(true);</code></td>
                                    <td><button class="btn btn-primary btn-xs btn-mini" type="button">Apply here</button></td>
                                </tr>
                                <tr>
                                    <td>dark_theme</td>
                                    <td><code>dark_theme</code></td>
                                    <td><code>dark_theme(true);</code></td>
                                    <td><button class="btn btn-primary btn-xs btn-mini" type="button">Apply here</button></td>
                                </tr>
                                <tr>
                                    <td>light_theme</td>
                                    <td><code>light_theme</code></td>
                                    <td><code>light_theme(true);</code></td>
                                    <td><button class="btn btn-primary btn-xs btn-mini" type="button">Apply here</button></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <p>Contact/message me to report any issues.</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- /.wrapper -->
@section('script')
<!-- Dashboard -->
<script type="text/javascript">
    (function($) {
        "use strict";
        // number count
        $('.timer').countTo();

        //TagsInput
        $("[data-toggle='tags']").tagsinput({
            tagClass: function(item) {
                return 'label label-primary';
            }
        });

        // chat scroll
        $('#chat-box').slimScroll({
            height: '250px'
        });

        //iCheck
        $("input[type='checkbox'], input[type='radio']").iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });

        // ToDo
        $('#checkbox').on('ifChecked', function(event){
            $('.check').addClass('through')
        });
        $('#checkbox').on('ifUnchecked', function(event){
            $('.check').removeClass('through')
        });

        // gritter
        setTimeout(function() {
            $.gritter.add({
                title: 'You have one new task for today',
                text: 'Go and check <a href="mailbox.html" class="text-warning">tasks</a> to see what you should do.<br/> Check the date and today\'s tasks.'
            });
        }, 2000);

        // flot
        var v1 = [[1,50],[2,53],[3,40],[4,55],[5,47],[6,39],[7,44],[8,55],[9,43],[10,61],[11,52],[12,57],[13,64],[14,54],[15,49],[16,55],[17,53],[18,57],[19,68],[20,71],[21,84],[22,72],[23,88],[24,74],[25,87],[26,83],[27,76],[28,86],[29,93],[30,95]];
        var v2= [[1,13],[2,18],[3,14],[4,25],[5,23],[6,17],[7,20],[8,26],[9,24],[10,27],[11,32],[12,37],[13,32],[14,28],[15,25],[16,21],[17,25],[18,33],[19,30],[20,27],[21,35],[22,28],[23,29],[24,28],[25,34],[26,27],[27,40],[28,29],[29,33],[30,45]];
        var C= ["#7fb9d1","#e65353"];
        var plot = $.plot("#placeholder", [
            { data: v1, label: "Total Visitors",lines:{fillColor: "#f8fcfd"}},
            { data: v2, label: "Unique Visitors",lines:{fillColor: "#fdf8f8"}}
        ], {
            series: {
                lines: {
                    show: true,
                    fill: true
                },
                points: {
                    show: true
                },
                shadowSize: 0
            },
            grid: {
                hoverable: true,
                clickable: true,
                aboveData: true,
                borderWidth: 0
            },
            legend:{
                noColumns: 0,
                margin: [0,-23],
                labelBoxBorderColor: null
            },
            colors: C,
            tooltip: true
        });

        function showTooltip(x, y, contents) {
            $("<div id='flot_tip'>" + contents + "</div>").css({
                top: y - 20,
                left: x + 5,
            }).appendTo("body").fadeIn(200);
        }

        var previousPoint = null;
        $("#placeholder").bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;
                    $("#flot_tip").remove();
                    var x = item.datapoint[0].toFixed(0),
                        y = item.datapoint[1].toFixed(0);
                    showTooltip(item.pageX, item.pageY,
                        y + " " + item.series.label + " on the " + x + "th");
                }
            } else {
                $("#flot_tip").remove();
                previousPoint = null;
            }
        });

        // jvectormap
        $('#map').vectorMap({
            map: 'europe_merc_en',
            zoomMin: '3',
            backgroundColor: "#fff",
            focusOn: { x: 0.5, y: 0.7, scale: 3 },
            markers: [
                {latLng: [42.5, 1.51], name: 'Andorra'},
                {latLng: [43.73, 7.41], name: 'Monaco'},
                {latLng: [47.14, 9.52], name: 'Liechtenstein'},
                {latLng: [41.90, 12.45], name: 'Vatican City'},
                {latLng: [43.93, 12.46], name: 'San Marino'},
                {latLng: [35.88, 14.5], name: 'Malta'}
            ],
            markerStyle: {
                initial: {
                    fill: '#fa4547',
                    stroke: '#fa4547',
                    "stroke-width": 6,
                    "stroke-opacity": 0.3,
                }
            },
            regionStyle: {
                initial: {
                    fill: '#e4e4e4',
                    "fill-opacity": 1,
                    stroke: 'none',
                    "stroke-width": 0,
                    "stroke-opacity": 1
                }
            }
        });
    })(jQuery);
</script>

@stop
