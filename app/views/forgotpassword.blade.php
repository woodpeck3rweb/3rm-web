<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SPARK - ForgotPassword</title>

    <!-- Maniac stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/animate/animate.min.css" />
    <link rel="stylesheet" href="css/iCheck/all.css" />
    <link rel="stylesheet" href="css/style.css" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login fixed">
<div class="wrapper animated flipInY">
    <div class="logo"><a href="#"> <span>SPARK</span></a></div>
    <div class="box">
        <div class="header clearfix">
            <div class="pull-left"><i class="fa fa-plus"></i> Forgot Password</div>
            <div class="pull-right"><a href="#"><i class="fa fa-times"></i></a></div>
        </div>
        <form id="register-form" method="post" role="form">
            <div class="box-body padding-md">
                <div class="form-group" >
                    <div class="input-group" style="margin-bottom: 10px;">
                        <input type="text" id="email" name="email" class="form-control" placeholder="Email" >
                        <span class="input-group-addon">@woodpeck3r.com</span>
                    </div>
                </div>
                <div id="message"></div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-success btn-block" id="submitBtn">Send</button>
                </div>
            </div>
        </form>
    </div>

</div>


<!-- Javascript -->
<script src="js/plugins/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="js/plugins/jquery/jquery.validate.min.js"></script>

<script src="js/plugins/jquery-ui/jquery-ui-1.10.4.min.js" type="text/javascript"></script>

<!-- Bootstrap -->
<script src="js/plugins/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="js/plugins/bootstrapValidator/bootstrapValidator.min.js" type="text/javascript"></script>

<!-- Interface -->
<script src="js/plugins/pace/pace.min.js" type="text/javascript"></script>

<!-- Forms -->
<script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

<script>
    //iCheck
    $("input[type='checkbox'], input[type='radio']").iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal'
    });
    var base_url = window.location.origin;
    $('#register-form').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            email: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    }
                }
            }
        }
    });
    $('#submitBtn').click(function(event) {
        var email = $('#email').val()+"@woodpeck3r.com";
        var register = $('#register-form').valid();// Checking valid form
        alert(register)
        if (register) {
            $.ajax({
                type: "POST",
                url: base_url + "/SPARK/public/sendnewpassword",
                data: {
                    email: email
                } , //serialize form
                success: function (msg) {
                    setTimeout(function(){window.location.href = base_url+"/SPARK/public/login";},5000);
                }
            });
        }
        event.preventDefault();
    });

</script>
</body>
</html>