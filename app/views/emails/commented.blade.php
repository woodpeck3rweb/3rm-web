<?php
$assignee_name = $to_user['first_name']." ".$to_user['last_name'];

$assigned_by = Auth::user()->first_name;
$assigned_by_image = Auth::user()->picture;

$task_title = $task['title'];
$project_name = $task['project']['title'];
$task_url = URL::to("project/tasks/".$task['project']['id']);
$message = $comment['message'];
?>

<div>

	<table cellpadding="0" cellspacing="0" border="0" align="center"
		   style="color:#222;font-family:Arial,Helvetica,sans-serif;border-spacing:0;padding:0;border-collapse:collapse;background-color:#f8f8f8">
		<tbody>
		<tr height="46">
			<td width="30" style="border-collapse:collapse;vertical-align:middle;background:#273849"></td>
			<td width="500" style="border-collapse:collapse;vertical-align:middle;background:#273849">
				<H3 size="3" valign="middle" style="color:white;margin-top: 15px">SPARK</H3>
			</td>
			<td width="30" style="border-collapse:collapse;vertical-align:middle;background:#273849"></td>
		</tr>
		<tr>
			<td height="20" colspan="3" style="border-collapse:collapse;vertical-align:top"></td>
		</tr>
		<tr>
			<td width="30" style="border-collapse:collapse;vertical-align:top"></td>
			<td width="500" valign="top" style="border-collapse:collapse;vertical-align:top">
				<table cellpadding="0" cellspacing="0" border="0" align="left"
					   style="color:#222;font-family:Arial,Helvetica,sans-serif;border-spacing:0;padding:0;border-collapse:collapse">
					<tbody>
					<tr>
						<td valign="top"
							style="border-collapse:collapse;vertical-align:top;font-size:14px;font-weight:bold">
							Hi {{$assignee_name}},
						</td>
					</tr>
					<tr>
						<td height="25" style="border-collapse:collapse;vertical-align:top"></td>
					</tr>
					<tr>
						<td style="border-collapse:collapse;vertical-align:top">
							<table cellpadding="0" cellspacing="0" border="0" width="100%"
								   style="color:#222;font-family:Arial,Helvetica,sans-serif;border-spacing:0;padding:0;border-collapse:collapse">
								<tbody>
								<tr>
									<td style="border-collapse:collapse;vertical-align:top">
										<table cellpadding="0" cellspacing="0" border="0" width="100%"
											   style="color:#222;font-family:Arial,Helvetica,sans-serif;border-spacing:0;padding:0;border-collapse:collapse">
											<tbody>
											<tr>
												<td valign="top" style="border-collapse:collapse;vertical-align:top">
													<a>
														<img alt="{{$assigned_by}}"
															 src="{{$assigned_by_image}}"
															 width="64" height="64" class="CToWUd"></a>
												</td>
												<td width="20" style="border-collapse:collapse;vertical-align:top"></td>
												<td align="top" style="border-collapse:collapse;vertical-align:top">
													<table cellpadding="0" cellspacing="0" border="0" width="100%"
														   style="color:#222;font-family:Arial,Helvetica,sans-serif;border-spacing:0;padding:0;border-collapse:collapse">
														<tbody>
														<tr>
															<td style="border-collapse:collapse;vertical-align:top;color:#454545;font-size:14px">
																{{$assigned_by}} commented on the task:
															</td>
														</tr>
														<tr height="10"></tr>
														<tr>
															<td style="border-collapse:collapse;vertical-align:top;font-weight:bold;color:#33628a;font-size:16px">
																{{$task_title}}
															</td>
														</tr>
														<tr height="40"> <td style="color: #888888;font-size: 14px"><i>"{{$message}}"</i></td></tr>
														<tr height="10"></tr>
														<tr height="1">
															<td width="100%"
																style="border-collapse:collapse;vertical-align:top;background:#ddd;width:100%"></td>
														</tr>
														<tr height="10"></tr>
														<tr>
															<td style="border-collapse:collapse;vertical-align:top">
																<table cellpadding="0" cellspacing="0" border="0"
																	   width="100%"
																	   style="color:#888;font-family:Arial,Helvetica,sans-serif;border-spacing:0;padding:0;border-collapse:collapse;font-size:12px;width:100%">
																	<tbody>
																	<tr height="5"></tr>
																	<tr>
																		<td width="20"
																			style="border-collapse:collapse;vertical-align:top">
																			<img alt="Task:"
																				 src="https://ci3.googleusercontent.com/proxy/UG0fnmTvMCRQ-xu3zTYjGMiOPvgd5PX8wqISKjTlevWE1JntASFeouArPUrJmmhZk7DNwjo9LLjA-B3wPFg-gYS98ApdbW6sb0G127rmeXP4oUZ-NqXhypAbJ0DBsPw=s0-d-e1-ft#http://www.producteev.com/bundles/producteevcore/images/email-project.jpg"
																				 width="14" height="14" class="CToWUd">
																		</td>
																		<td width="10"
																			style="border-collapse:collapse;vertical-align:top"></td>
																		<td style="border-collapse:collapse;vertical-align:top">
																			{{$project_name}}
																		</td>
																	</tr>
																	<tr height="5"></tr>
																	<tr>
																		<td width="20"
																			style="border-collapse:collapse;vertical-align:top">
																			<img alt="Task:"
																				 src="https://ci4.googleusercontent.com/proxy/sZq6pNLxtRhgjEPZVKOCkexL-gBBAFYkzbu0OTIEyb4Wr_qUWlG_PcuaRh38eErvFJwEcANh1wMiV18x-MeVELPFEYEpcLphUaPgo2RPetiF0jt8xB8xDWBXifwH7yw=s0-d-e1-ft#http://www.producteev.com/bundles/producteevcore/images/email-network.jpg"
																				 width="14" height="14" class="CToWUd">
																		</td>
																		<td width="10"
																			style="border-collapse:collapse;vertical-align:top"></td>
																		<td style="border-collapse:collapse;vertical-align:top">
																			Kronch.co
																		</td>
																	</tr>
																	<tr>
																		<td height="40"
																			style="border-collapse:collapse;vertical-align:top"></td>
																	</tr>
																	<tr>
																		<td colspan="3"
																			style="border-collapse:collapse;vertical-align:top">
																			<a href="{{$task_url}}"
																			   style="background:#27ae60;color:white;font-weight:bold;font-size:14px;padding:8px 12px;text-decoration:none;border-radius:3px"
																			   target="_blank">
																				<span>View</span>
																			</a>
																		</td>
																	</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
											</tr>
											</tbody>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td height="25" style="border-collapse:collapse;vertical-align:top"></td>
					</tr>
					</tbody>
				</table>
			</td>
			<td width="30" style="border-collapse:collapse;vertical-align:top"></td>
		</tr>
		<tr>
			<td height="20" colspan="3" style="border-collapse:collapse;vertical-align:top"></td>
		</tr>
		<tr>
			<td width="30"
				style="border-collapse:collapse;vertical-align:top;background-color:#eee;background:#eee"></td>
			<td width="30"
				style="border-collapse:collapse;vertical-align:top;background-color:#eee;background:#eee"></td>
		</tr>
		</tbody>
	</table>

</div>