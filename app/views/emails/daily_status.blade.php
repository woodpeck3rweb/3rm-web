<!DOCTYPE html>
<html lang="en">
<body>
<p>Hello {{ $name }},</p>

<div class="container">
	<div class="col-md-12">
		<div class="well col-md-6 col-md-offset-3 text-center">
			<p>Status :</p>
			<p>{{$status}}</p>

			<div class="pull-left">
				Regards,<br>
				SPARK Team.
			</div>
		</div>
	</div>
</div>

</body>
</html>
