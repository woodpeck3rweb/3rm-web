@extends('layouts.blank')

@section('content')

<div class="col-md-6">
    <div class="box">
        <div class="box-title">
            <h3>Bar Chart</h3>
        </div>
        <!-- /.box-title -->
        <div class="box-body">
            <div class="flot">
                <div class="flot-placeholder" id="flot-bar-chart" style="padding: 0px; position: relative;">
                    <canvas class="flot-base" width="520" height="350" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 520px; height: 350px;"></canvas>
                    <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                        <div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 89px; top: 334px; left: 16px; text-align: center;">1</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 89px; top: 334px; left: 104px; text-align: center;">2</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 89px; top: 334px; left: 192px; text-align: center;">3</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 89px; top: 334px; left: 280px; text-align: center;">4</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 89px; top: 334px; left: 368px; text-align: center;">5</div>
                            <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 89px; top: 334px; left: 456px; text-align: center;">6</div></div>
                          <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                              <div class="flot-tick-label tickLabel" style="position: absolute; top: 321px; left: 7px; text-align: right;">0</div>
                              <div class="flot-tick-label tickLabel" style="position: absolute; top: 285px; left: 7px; text-align: right;">5</div>
                              <div class="flot-tick-label tickLabel" style="position: absolute; top: 250px; left: 0px; text-align: right;">10</div>
                              <div class="flot-tick-label tickLabel" style="position: absolute; top: 214px; left: 0px; text-align: right;">15</div>
                              <div class="flot-tick-label tickLabel" style="position: absolute; top: 178px; left: 0px; text-align: right;">20</div>
                              <div class="flot-tick-label tickLabel" style="position: absolute; top: 143px; left: 0px; text-align: right;">25</div>
                              <div class="flot-tick-label tickLabel" style="position: absolute; top: 107px; left: 0px; text-align: right;">30</div>
                              <div class="flot-tick-label tickLabel" style="position: absolute; top: 71px; left: 0px; text-align: right;">35</div>
                              <div class="flot-tick-label tickLabel" style="position: absolute; top: 36px; left: 0px; text-align: right;">40</div>
                              <div class="flot-tick-label tickLabel" style="position: absolute; top: 0px; left: 0px; text-align: right;">45</div>
                          </div>
                    </div>
                    <canvas class="flot-overlay" width="520" height="350" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 520px; height: 350px; color: #000;"></canvas></div>
            </div>
        </div>
    </div>
	<!-- /.box -->

	<div class="box">
		<div class="box-title">
			<h3>Line Chart </h3>
		</div>
		<!-- /.box-title -->
		<div class="box-body">
			<div class="flot">
				<div class="flot-placeholder" id="flot-line-chart"></div>
			</div>
		</div>
	</div>
	<!-- /.box -->
</div>
<div class="col-md-6">
	<div class="box">
		<div class="box-title">
			<h3>Live Chart</h3>
		</div>
		<!-- /.box-title -->
		<div class="box-body">
			<div class="flot">
				<div class="flot-placeholder" id="flot-live-chart"></div>
			</div>
		</div>
	</div>
	<!-- /.box -->

	<div class="box">
		<div class="box-title">
			<h3>Pie Chart </h3>
		</div>
		<!-- /.box-title -->
		<div class="box-body">
			<div class="flot">
				<div class="flot-placeholder" id="flot-pie-chart"></div>
			</div>
		</div>
	</div>
	<!-- /.box -->
</div><!-- /.col -->
@stop

@section('script')

{{ HTML::script('js/plugins/flot/jquery.flot.min.js'); }}
{{ HTML::script('js/plugins/flot/jquery.flot.resize.min.js'); }}
{{ HTML::script('js/plugins/flot/jquery.flot.pie.min.js'); }}
{{ HTML::script('s/plugins/flot/jquery.flot.stack.min.js'); }}
{{ HTML::script('js/plugins/flot/jquery.flot.crosshair.min.js'); }}



<script type="text/javascript">
var data = [], totalPoints = 100;
function getRandomData() {

	if (data.length > 0)
		data = data.slice(1);

	// Do a random walk
	while (data.length < totalPoints) {

		var prev = data.length > 0 ? data[data.length - 1] : 50,
			y = prev + Math.random() * 10 - 5;

		if (y < 0) {
			y = 0;
		} else if (y > 100) {
			y = 100;
		}

		data.push(y);
	}

	var res = [];
	for (var i = 0; i < data.length; ++i) {
		res.push([i, data[i]]);
	}

	return res;
}

var flot_live_chart_plot = $.plot("#flot-live-chart", [getRandomData()], {
	grid: {
		borderColor: "#f3f3f3",
		borderWidth: 1,
		tickColor: "#f3f3f3"
	},
	series: {
		shadowSize: 0, // Drawing is faster without shadows
		color: "#4a6b8b"
	},
	lines: {
		fill: true, //Converts the line chart to area chart
		color: "#4a6b8b"
	},
	yaxis: {
		min: 0,
		max: 100,
		show: true
	},
	xaxis: {
		show: true
	}
});

var updateInterval = 500; //Fetch data ever x milliseconds
var realtime = "on"; //If == to on then fetch data every x seconds. else stop fetching
function update() {

	flot_live_chart_plot.setData([getRandomData()]);

	// Since the axes don't change, we don't need to call plot.setupGrid()
	flot_live_chart_plot.draw();
	if (realtime === "on")
		setTimeout(update, updateInterval);
}

if (realtime === "on") {
	update();
}


$(function () {
	var barOptions = {
		series: {
			bars: {
				show: true,
				barWidth: 0.6,
				fill: true,
				fillColor: {
					colors: [
						{
							opacity: 0.8
						},
						{
							opacity: 0.8
						}
					]
				}
			}
		},
		xaxis: {
			tickDecimals: 0
		},
		colors: ["#4a6b8b"],
		grid: {
			color: "#999999",
			hoverable: true,
			clickable: true,
			tickColor: "#D4D4D4",
			borderWidth: 0
		},
		legend: {
			show: false
		},
		tooltip: true,
		tooltipOpts: {
			content: "x: %x, y: %y"
		}
	};
	var barData = {
		label: "bar",
		data: [
			[1, 30],
			[2, 15],
			[3, 39],
			[4, 22],
			[5, 18],
			[6, 41]
		]
	};
	$.plot($("#flot-bar-chart"), [barData], barOptions);
});

$(function () {
	var barOptions = {
		series: {
			lines: {
				show: true,
				lineWidth: 2,
				fill: true,
				fillColor: {
					colors: [
						{
							opacity: 0.0
						},
						{
							opacity: 0.0
						}
					]
				}
			}
		},
		xaxis: {
			tickDecimals: 0
		},
		colors: ["#4a6b8b"],
		grid: {
			color: "#999999",
			hoverable: true,
			clickable: true,
			tickColor: "#D4D4D4",
			borderWidth: 0
		},
		legend: {
			show: false
		},
		tooltip: true,
		tooltipOpts: {
			content: "x: %x, y: %y"
		}
	};
	var barData = {
		label: "bar",
		data: [
			[1, 34],
			[2, 25],
			[3, 19],
			[4, 34],
			[5, 32],
			[6, 44]
		]
	};
	$.plot($("#flot-line-chart"), [barData], barOptions);

});
//Flot Pie Chart
$(function () {

	var data = [
		{
			label: "Sales 1",
			data: 17,
			color: "#34495e"
		},
		{
			label: "Sales 2",
			data: 3,
			color: "#d35400"
		},
		{
			label: "Sales 3",
			data: 25,
			color: "#e74c3c"
		},
		{
			label: "Sales 4",
			data: 50,
			color: "#27ae60"
		}
	];

	var plotObj = $.plot($("#flot-pie-chart"), data, {
		series: {
			pie: {
				show: true
			}
		},
		grid: {
			hoverable: true
		},
		tooltip: true,
		tooltipOpts: {
			content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
			shifts: {
				x: 20,
				y: 0
			},
			defaultTheme: false
		}
	});

});
</script>
@stop