@extends('layouts.blank')

@section('styles')
{{ HTML::style('css/3rm.css'); }}
{{ HTML::style('css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); }}
@stop

@section('main-content')
<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="box" style="height: 360px">
			<div class="box-title">
				<i class="fa fa-check-square-o"></i>
				<h3>ToDo List</h3>
				<div class="pull-right box-toolbar">
				</div>
			</div>
			<div class="box-body" style="height:320px;overflow-x: hidden">
				<ul style="padding-left: 0px;">
					@forelse ($tasks as $task)
					<li class="task-normal">
						<span class="text">{{$task->title }}</span>
						<span class="label label-{{strtolower($task->status)}} pull-right">{{$task->status}}</span>
						<span class="text-info pull-right" style="margin-right: 5px">{{$task->type}}</span>
						<a href="{{ URL::to('project/tasks/') }}/{{$task->project->id}}"><span
								class="center-block text-success">{{$task->project->title}}</span></a>
					</li>
					@empty
					<div>
						<h2>No Tasks today !!, You must be kidding me.</h2>
					</div>
					@endforelse
				</ul>
			</div>

		</div>
	</div>
	<div class="col-lg-6 col-md-6">
		<div class="box" style="height: 360px">
			<div class="box-title">
				<i class="fa fa-check-square-o"></i>
				<h3>On Going Tasks</h3>
				<div class="pull-right box-toolbar">
				</div>
			</div>
			<div class="box-body" style="height:320px;overflow-x: hidden">
				<ul style="padding-left: 0px;">
					@forelse ($current as $t)
					<li class="task-normal">
						<span class="center-block text-success">{{ isset($t->assignee) ? $t->assignee->full_name : "" }}</span>
						<a href="{{ URL::to('project/tasks/') }}/{{$t->project_id}}">
							<span class="text">{{ $t->title }}</span>
						</a>
						<span class="label label-{{strtolower($t->status)}} pull-right">{{$t->status}}</span>
						<span class="label label-{{strtolower($t->priority)}} pull-right">{{$t->priority}}</span>
						<span class="text-info pull-right" style="margin-right: 5px">{{$t->type}}</span>
					</li>
					@empty
					<div>
						<h2>No ones working now !!</h2>
					</div>
					@endforelse
				</ul>
			</div>
		</div>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="box">
			<div class="box-title">
				<h3>This Week Stats</h3>
			</div>
			<div class="box-body">
				<div id="container"></div>
			</div>
		</div>
	</div>
</div>

<!-- Dashboard -->
@stop

@section('script')
{{ HTML::script('js/plugins/highcharts/highcharts.js'); }}

<script type="text/javascript">
	$(function () {
		$('#container').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: 'Daily Stats of the Team'
			},
			subtitle: {
				text: 'Source: SPARK'
			},
			xAxis: {
				categories: [
					'Mon',
					'Tue',
					'Wed',
					'Thu',
					'Fri',
					'Sat',
					'Sun'
				]
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Hours'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f} Hrs</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: {{ json_encode($graph['values']) }}
		});
	});

</script>
@stop