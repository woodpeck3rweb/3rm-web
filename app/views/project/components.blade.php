@extends('layouts.project')

@section('styles')
{{ HTML::style('css/bootstrap.css'); }}
{{ HTML::style('css/editable/bootstrap-editable.css'); }}
{{ HTML::style('css/3rm.css'); }}
<link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css;"/>

@stop

@section('content')
<div class="col-md-6">
    <div class="box">
        <div class="box-title">
            <h3>Components Table</h3>
            <a data-placement="top" style="float:right;" data-toggle="tooltip" href="#addcomponents" data-original-title="" title="">
                <button class="btn btn-grey btn-sm" type="button" style="background-color: #616161;color: #fff;">+</button>
            </a>
        </div>
        <!-- /.box-title -->
        <div class="box-body">
            @foreach ($components as $component)
            <table class="table">
                <tbody>
                <tr>
                    <td id="popover" data-trigger="hover">{{$component->title }}</td>
                    <td><button class="btn btn-danger btn-xs pull-right" onclick="deleteComponent({{$component->id}})"><i class="fa fa-trash-o"></i> </button></td>
                </tr>
                </tbody>
            </table>
            @endforeach
        </div>
    </div>
</div>

<div id="addcomponents" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-center" id="mySmallModalLabel">Add Components</h4>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                        <div class="form-group col-md-12">
                            <input id="componentTitle"  type="text" name='title' placeholder='Title' class="form-control"/>
                        </div>
                        <div class="col-md-1 col-md-offset-5" style="margin-bottom: 20px;">
                            <button type="submit" class="btn btn-sm btn-success" id="buttonaddcomponent">Submit</button>
                        </div>

            </div>
                <div class="modal-footer" style="border-top: 0px solid #e5e5e5;">

                </div>
    </div>
</div>
</div>




@stop

@section('script')

<script >
    var base_url = window.location.origin;
    var moduleIds = new Array();
    function deleteComponent(id)
    {
        var component_id = id;
        var url = base_url + "/spark/public/project/component/"+component_id;
    $.ajax({
        type: "POST",
        url: url,
        data: {
        } , //serialize form
        success: function (msg) {
            console.log("Successfully removed -- "+ base_url);
            window.location.reload();
        }
    });
    }
    $(document).ready(function (){
            $('a[href="#addcomponents"]').on('click', function (event) {
                event.preventDefault();
                $('#addcomponents').modal('show');
            });

        $('#buttonaddcomponent').on('click',function (){
            //var userIds = new Array();
            var url = base_url + "/spark/public/project/addcomponent/{{$project->id}}";
            //
            // alert($('#componentTitle').val());
            $.post( url,
                {
                    title: $('#componentTitle').val()
                },
                function (data) {
                    $('#addcomponents').modal('hide');
                    alert(data.message);
                }
            )
        });

    });

</script>

<!--{{ HTML::script('js/jquery.min.js'); }}-->
<!--{{ HTML::script('js/plugins/bootstrap/bootstrap.js'); }}-->
{{ HTML::script('js/plugins/editable/bootstrap-editable.js'); }}
{{ HTML::script('js/plugins/react/react.js'); }}
{{ HTML::script('js/plugins/react/JSXTransformer.js'); }}

@stop
