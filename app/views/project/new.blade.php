@extends('layouts.blank')

@section('content')

<div class="col-md-6">
	<div class="box">
		<div class="box-title">
			<h3>New Project</h3>
		</div>
		<!-- /.box-title -->
		<div class="box-body">
			<div class="col-md-12">
				<form id="new-project-form" method="post">
					<div class="form-group col-md-12">
						<input id="Name" type="text" name='name0' placeholder='Name' class="form-control"/>
					</div>
					<div class="form-group col-md-12">
						<textarea class="form-control" id="Description" style="height: 125px;"></textarea>
					</div>

					<div class="form-group col-md-6">
						<input id="ETA" type="text" name='mobile0' placeholder='Estimate for Project'
							   class="form-control"/>
					</div>
					<div class="form-group col-md-6">
						<input id="Cost" type="text" name='mobile0' placeholder='Cost of the Project'
							   class="form-control"/>
					</div>
					<div class="form-group col-md-12">
						<select id="Client" class="form-control">
							<option value="000">--select client--</option>
							@foreach ($clients as $client)
								<option value="{{$client->id}}">{{$client->full_name}}</option>
							@endforeach
							<option value="-1">Create</option>
						</select>
					</div>
					<div class="form-group col-md-12">
						<select id="Lead" class="form-control">
							@foreach ($users as $user)
							<option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-md-6">
						Start Date:
						<input id="StartDate" type="date" name='mobile0' placeholder='Start Date'
							   class="form-control"/>
					</div>
					<div class="form-group col-md-6">
						End Date:
						<input id="EndDate" type="date" name='mobile0' placeholder='End Date'
							   class="form-control"/>
					</div>
					<div class="col-md-1 col-md-offset-5" style="margin-bottom: 20px;">
						<button type="submit" class="btn btn-sm btn-success">Submit</button>
					</div>
				</form>
			</div>
		</div>

		<div class="clearfix"></div>
	</div>
</div>

@stop

<!-- Dashboard -->
@section('script')
<script type="text/javascript">
	$(document).ready(function () {
		$('#Client').change(function() {
			if (this.value == -1) {
				window.location = "{{ URL::to('allclients') }}";
			}
		});

		$("#new-project-form").submit(function (event) {
			event.preventDefault();

			var name = $('#Name').val();
			var description = $('#Description').val();
			var eta = $('#ETA').val();
			var client = $('#Client').val();
			var managed_id = $('#Lead').val();
			var start_date = $('#StartDate').val();
			var end_date = $('#EndDate').val();

			console.log(name);

			var url = "{{URL::to('project/create')}}";

			alert(url);

			$.post(url,
				{
					title: name,
					description: description,
					eta: eta,
					start_date: start_date,
					end_date: end_date,
					manager_id: managed_id,
					client_id: client
				},
				function (data, status) {
					console.log(status);
					console.log(data);
				}
			)
			.fail(function (data) {
				console.log("error");
				console.log(data);
			});
		});
	});
</script>
@stop