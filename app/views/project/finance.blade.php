@extends('layouts.project')

@section('content')
<div class="col-md-12">
	<div class="box">
		<div class="box-body padding-md">
			<div class="invoice-title">
				<h4 class="pull-right">Order # 12345</h4>
				<h3><i class="fa fa-bolt"></i> Maniac, Inc.</h3>
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-6">
					<address>
						<strong>Billed To:</strong><br>
						John Smith<br>
						1234 Main<br>
						Apt. 4B<br>
						Springfield, ST 54321
					</address>
				</div>
				<div class="col-xs-6 text-right">
					<address>
						<strong>Shipped To:</strong><br>
						Jane Smith<br>
						1234 Main<br>
						Apt. 4B<br>
						Springfield, ST 54321
					</address>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<address>
						<strong>Payment Method:</strong><br>
						Visa ending **** 4242<br>
						jsmith@email.com
					</address>
				</div>
				<div class="col-xs-6 text-right">
					<address>
						<strong>Order Date:</strong><br>
						March 7, 2014<br><br>
					</address>
				</div>
			</div>
			<div class="well no-radius no-border">
				<h4 style="margin-bottom: 30px">Order summary</h4>

				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
						<tr>
							<td><strong>Item</strong></td>
							<td class="text-center"><strong>Price</strong></td>
							<td class="text-center"><strong>Quantity</strong></td>
							<td class="text-right"><strong>Totals</strong></td>
						</tr>
						</thead>
						<tbody>
						<!-- foreach ($order->lineItems as $line) or some such thing here -->
						<tr>
							<td>BS-200</td>
							<td class="text-center">$10.99</td>
							<td class="text-center">1</td>
							<td class="text-right">$10.99</td>
						</tr>
						<tr>
							<td>BS-400</td>
							<td class="text-center">$20.00</td>
							<td class="text-center">3</td>
							<td class="text-right">$60.00</td>
						</tr>
						<tr>
							<td>BS-1000</td>
							<td class="text-center">$600.00</td>
							<td class="text-center">1</td>
							<td class="text-right">$600.00</td>
						</tr>
						<tr>
							<td class="thick-line"></td>
							<td class="thick-line"></td>
							<td class="thick-line text-center"><strong>Subtotal</strong></td>
							<td class="thick-line text-right">$670.99</td>
						</tr>
						<tr>
							<td class="no-line"></td>
							<td class="no-line"></td>
							<td class="no-line text-center"><strong>Shipping</strong></td>
							<td class="no-line text-right">$15</td>
						</tr>
						<tr>
							<td class="no-line"></td>
							<td class="no-line"></td>
							<td class="no-line text-center"><strong>Total</strong></td>
							<td class="no-line text-right">$685.99</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="box-footer padding-md">
			<button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
			<button class="btn btn-success pull-right" style="margin-left: 5px;"><i class="fa fa-credit-card"></i>
				Submit Payment
			</button>
			<button class="btn btn-primary pull-right"><i class="fa fa-download"></i> Generate PDF</button>
		</div>
	</div>
</div>
@stop