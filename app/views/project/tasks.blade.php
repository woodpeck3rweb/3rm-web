@extends('layouts.project')

@section('styles')
{{ HTML::style('css/editable/bootstrap-editable.css'); }}
{{ HTML::style('css/3rm.css'); }}
{{ HTML::style('js/plugins/bootstrap-datepicker/css/datepicker.css'); }}

<style type="text/css">
	div > li.active {
		background-color: aliceblue;
		margin: 0px -10px 0px -10px;
		padding-left: 10px;
		padding-right: 10px;
	}
	li {
		outline: none;
	}
</style>
@stop

@section('content')

<div class="col-md-7">
	<div class="box">
		<div class="box-title">
			<i class="fa fa-check-square-o"></i>

			<h3>Tasks</h3>

			<div class="form-group">
				<div class="col-sm-3"><i class="glyphicon glyphicon-star-empty"></i>
					<select class="selectpicker" id="selectpriority">
						<optgroup label="Priority">
							<option value="All"> All</option>
							<option value="task-major">Major</option>
							<option value="task-minor">Minor</option>
							<option value="task-critical">Critical</option>
							<option value="task-blocker">Blocker</option>
							<option value="task-trivial">Trivial</option>
						</optgroup>
					</select>
				</div>
				<div class="col-sm-3" style="margin-left: -40px; "><i class="glyphicon glyphicon-align-center"></i>
					<select class="selectpicker" id="select">
						<optgroup label="Progress">
							<option value="All"> All</option>
							<option value="task-progress">Progress</option>
							<option value="task-open">Open</option>
							<option value="task-resolved">Resolved</option>
							<option value="task-reopened">Reopened</option>
							<option value="task-closed">Closed</option>
						</optgroup>
					</select>
				</div>

				<div class="col-sm-3" style="margin-left: -30px; "><i class="fa fa-child"></i>
					<select class="selectpicker" id="selectuser">
						<optgroup label="Users">
							<option value="All"> All</option>
							@foreach ($users as $user)
							<option value="{{$user->first_name}}">{{$user->first_name}}</option>
							@endforeach
						</optgroup>
					</select>
				</div>
			</div>
			<div class="pull-right box-toolbar" style="cursor: pointer;">
				<div id="hidecompletedtasks" style="display: block;">
					<i class="fa fa-minus-square"></i> <span>hide completed</span>
				</div>
				<div id="showcompletedtasks" style="display: none;">
					<i class="fa fa-plus-square"></i> <span>show completed</span>
				</div>
			</div>
		</div>

		<!--  Tasks will be updated from React JS -->
		<div class="box-body" id="tasks">
			<!--			-->
		</div>
	</div>
</div>

<div class="col-md-5" style="margin-left:-44px; margin-top: -8px; ">
	<!--	Task Details from React JS	-->
	<div class="col-md-12 col-sm-12" id="task-details">
		<div class="box" style="margin-top: 8px;">
			<div class="box-title" style="height: 50px;  ">
				<div class="form-group">
					<H4>Task details </H4>
				</div>
			</div>
			<div class="box-body" style="margin: 20px; height: 200px;  ">
				<div class="form-group">
					<H3> Select a Task to view details </H3>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="addtomilestone" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
	 aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md ">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title text-center" id="mySmallModalLabel">Add to Milestone</h4>
			</div>
			<div class="modal-body">
				<form id="task-form" method="post">
					<div class=" col-sm-12">
						@foreach ($milestones as $milestone)
						<div class="form-group col-md-12 addtomilestone">
							<div id="milestone{{$milestone->id}}" class="form-control"> {{$milestone->title}}</div>
						</div>
						@endforeach
					</div>
				</form>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
@stop

@section('script')

<!--{{ HTML::script('js/jquery.min.js'); }}-->
{{ HTML::script('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); }}
{{ HTML::script('js/plugins/editable/bootstrap-editable.js'); }}
{{ HTML::script('js/plugins/moment/moment.js'); }}
{{ HTML::script('js/plugins/react/react.js'); }}
{{ HTML::script('js/plugins/react/JSXTransformer.js'); }}

<script type="text/jsx" src="{{URL::asset('js/app/tasks.js')}}"></script>

@stop