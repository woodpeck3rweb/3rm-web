@extends('layouts.project')

@section('styles')
{{ HTML::style('css/bootstrap.css'); }}
{{ HTML::style('css/editable/bootstrap-editable.css'); }}
{{ HTML::style('css/3rm.css'); }}

@stop

@section('content')


<div class="col-md-4">
    <div class="box">
        <div class="box-title">
            <h3>Project Name</h3>
        </div>
        <form data-reactid=".0.1.0">
            <input class="form-control" type="text" id="editprojectname" data-reactid=".0.1.0.0" placeholder="Enter task" value="{{$project->title}}">
        </form>
    </div>
</div>


<div class="col-md-4">
    <div class="box">
        <div class="box-title">
            <div class="invoice-title">
            <h3>Modules</h3>
            <a data-placement="top" style="float:right;" data-toggle="tooltip" href="#addmodules" data-original-title="" title="">
                <button class="btn btn-grey btn-sm" type="button" style="background-color: #616161;color: #fff;">+</button>
            </a>
        </div>
       </div>
        <!-- /.box-title -->
        <div class="box-body padding-md">
            <div class=" form-group" >
                @foreach ($modules as $module)
                <div class=" addtomilestone">
                <table class="table">
                    <tbody>
                    <tr>
                        <td class="col-md-12"> {{$module->title}} </td>
                        <td><button class="btn btn-danger btn-xs" date-id="{{$module->id}}" onclick="deleteModule({{$module->id}})"><i class="fa fa-trash-o"></i> </button></td>
                    </tr>
                    </tbody>
                </table>
                 </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="box">
        <div class="box-title">
            <h3>Team</h3>
            <a data-placement="top" style="float:right;" data-toggle="tooltip" href="#addusers" data-original-title="" title="">
                <button class="btn btn-grey btn-sm" type="button" style="background-color: #616161;color: #fff;">+</button>
            </a>
        </div>
        <!-- /.box-title -->
        <div class="box-body padding-md">
            <div class=" form-group" >
                @foreach ($users as $user)
                <div class="addtomilestone">
                <table class="table">
                    <tbody>
                    <tr>
                        <td class="col-md-12">  {{$user->first_name}} </td>
                        <td><button class="btn btn-danger btn-xs"  onclick="deleteTeam({{$user->id}})"><i class="fa fa-trash-o"></i> </button></td>
                    </tr>
                    </tbody>
                </table>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>



<script >
    var base_url = window.location.origin;
    var moduleIds = new Array();
    function deleteModule(id)
    {
        var module_id = id;
        var url = base_url + "/spark/public/project/module/"+module_id;

        $.ajax({
            type: "POST",
            url: url,
            data: {
            } , //serialize form
            success: function (msg) {
                console.log("Successfully removed -- "+ base_url);
                window.location.reload();
            }
        });
    }
    function deleteTeam(id)
    {
        var module_id = id;
        var url = base_url + "/spark/public/project/team/"+team_id;
    $.ajax({
        type: "POST",
        url: url,
        data: {
        } , //serialize form
        success: function (msg) {
            console.log("Successfully removed -- "+ base_url);
            window.location.reload();
        }
    });
    }
    $(document).ready(function (){

        $('#addmodules').hide();
        $('a[href="#addmodules"]').on('click', function (event) {
            event.preventDefault();
            $('#addmodules').modal('show');
        });
        $('a[href="#addusers"]').on('click', function (event) {
            event.preventDefault();
            $('#addusers').modal('show');
        });

        $('#buttonaddmodule').on('click',function (){
				mixpanel.track("Project : Add Module");
            var moduleIds = new Array();
            $("input[name='modules']:checked").each(function (){
                moduleIds.push($(this).val())

            });
            var url = base_url + "/spark/public/project/assignmodules";
            $.post( url,
                {
                    projectId: {{$project->id}},
                    modules: moduleIds
                },
                function (data) {
                    console.log(data.status);
                    /*if(data.message=='Modules Assigned Successfully!'){
                        $('#addmodules').modal('hide');
                    }else{
                        $('#addmodules').modal('hide');

                    }*/
                    $('#addmodules').modal('hide');
                    alert(data.message);
                }
            )
        });
    $('#buttonadduser').on('click',function (){
			mixpanel.track("Project : Add User");
            var userIds = new Array();
            $("input[name='users']:checked").each(function (){
                userIds.push($(this).val())
            });
            var url = base_url + "/spark/public/project/addusers";
            $.post( url,
                {
                    projectId: {{$project->id}},
                    users: userIds
                },
                function (data) {
                    console.log(data.status);
                   /* if(data.message=='Users Assigned Successfully!'){
                        $('#addusers').modal('hide');
                    }else{
                        $('#addusers').modal('hide');
                    }*/
                    $('#addusers').modal('hide');
                    alert(data.message);
                }
             )
          });

    $('#editprojectname').on('keypress',function (event){
			mixpanel.track("Edit Project Name");
           var newName = $(this).val();
           if (event.which == 13) {
                    event.preventDefault();
                    var url = base_url + "/spark/public/project/projectname";
                    $.post( url,
                    {
                         projectId: {{$project->id}},
                         newName: newName
                    },
                    function (data) {
                        console.log(data.status);
                        alert(data.message);
                    }
                )
           }
    });
 });

</script>
<div id="addmodules" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-center" id="mySmallModalLabel">Add Module</h4>
            </div>
            <div class="modal-body">
                <form id="user-form" method="post">
                    @if ( $countRestModules > 0 )

                    <div class=" col-sm-12">
                        @foreach ($restModules as $module)
                        <div class="form-group col-md-12 ">
                            <input type="checkbox" class="pull-left" style="margin-bottom: -30px; margin-left: -20px;" name="modules" value="{{$module->id}}" />
                            <div id="module{{$module->id}}" class="form-control col-md-8" style="margin-left: 10px;"> {{$module->title}}</div>
                        </div>
                        @endforeach
                    </div>
                    <div style="margin-left: 10px;"><input type="button" class="btn btn-success btn-sm" id="buttonaddmodule" name="addmodule" value="Add module"> </div>
                    @else
                    There are no more modules to assign to this project!
                    @endif

                </form>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<div id="addusers" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-center" id="mySmallModalLabel">Add Users</h4>
            </div>
            <div class="modal-body">
                <form id="user-form" method="post">
                    @if ( $countRestUsers > 0 )
                    <div class=" col-sm-12">
                        @foreach ($restUsers as $user)
                        <div class="form-group ">
                            <div class="col-md-12" style="margin-bottom: 20px;">
                            <input type="checkbox" class="pull-left" style="margin-bottom: -30px; margin-left: -20px;" name="users" value="{{$user->id}}" />
                            <div id="user{{$user->id}}" class="form-control col-md-8" style="margin-left: 10px;"> {{$user->first_name}}</div></div>
                            </div>
                        @endforeach
                    </div>
                    <div style="margin-left: 10px;"><input type="button" class="btn btn-success btn-sm" id="buttonadduser" name="adduser" value="Adduser"> </div>
                    @else
                    There are no more users to add to this project!
                    @endif
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>


@stop

@section('script')

<!--{{ HTML::script('js/jquery.min.js'); }}-->
<!--{{ HTML::script('js/plugins/bootstrap/bootstrap.js'); }}-->
{{ HTML::script('js/plugins/editable/bootstrap-editable.js'); }}
{{ HTML::script('js/plugins/react/react.js'); }}
{{ HTML::script('js/plugins/react/JSXTransformer.js'); }}


@stop
