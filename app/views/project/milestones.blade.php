@extends('layouts.project')

@section('content')

<div class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-7">
            <div class="box">
                <div class="box-title">
                    <div class="invoice-title">
                        <h3><i class="fa fa-bolt "></i> Milestones</h3>
                        <a  href="#"  data-toggle="modal" data-target="#myModal">
                          <button type="button" class="btn btn-success btn-sm pull-right" style="margin-right: 15px;" >+</button> </a>
                    </div>
                    </div>
                    <div class="box-body padding-md" id="milestones">
                    <!--@foreach ($milestones as $milestone)
                        <table class="table">
                            <tbody>
                            <tr>
                                <td><div class="col-md-3"> {{$milestone->title }}</div><div class="col-md-6"> {{$milestone->description}}</div> <div class="col-md-3"> {{$milestone->completion_date}}</div></td>
                                <td><button class="btn btn-danger btn-xs pull-right" date-id="{{$milestone->id}}" onclick="deleteMilestone({{$milestone->id}})"><i class="fa fa-trash-o"></i> </button></td>
                            </tr>
                            </tbody>
                        </table>
                        @endforeach
-->
                   </div>
                </div>
            </div>
    <div class="col-md-5 pull-right">
        <div class="col-md-12 col-sm-12">
        <div class="box">
                <div class="box-title" style="height: 50px;">
                    <div class="form-group row text-center">
                        <H4>Milestone details </H4>
                    </div>
                </div>
                <div class="box-body" style="margin: 20px; height: 200px;  ">
                    <div class="form-group" >
                     <div  id="milestone-details"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Create Milestone</h4>
            </div>
            <div class="modal-body">
                <form id="new-milestone-form" method="post">
                    <h3 class="text-center"> Milestone Creation</h3>
                    <div class="form-group col-md-12">
                        <input id="title" type="text" name='title' placeholder='Title' class="form-control"/>
                    </div>
                    <div class="form-group col-md-12">
                        <textarea class="form-control" id="Description" placeholder="Description" style="height: 100px;" ></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        End Date:
                        <input id="EndDate" type="date" name='mobile0' placeholder='End Date'
                               class="form-control"/>
                    </div>
                    <div class="col-md-1 col-md-offset-5" style="margin-bottom: 20px;">
                        <button type="submit" class="btn btn-sm btn-success">Submit</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="border-top: 0px solid #e5e5e5;">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#Client').change(function() {
            if (this.value == -1) {
                window.location = "{{ URL::to('newclient') }}";
            }
        });

        $("#new-milestone-form").submit(function (event) {
            event.preventDefault();
			mixpanel.track("New Milestone Submit");

            var title = $('#title').val();
            var description = $('#Description').val();
            var end_date = $('#EndDate').val();
            var project_id = "{{$project->id}}";
            console.log(name);

            var url = "{{URL::to('project/newmilestone')}}";

            alert(url);

            $.post(url,
                {
                    title: title,
                    description: description,
                    completion_date: end_date,
                    project_id: project_id
                },
                function (data, status) {
                    console.log(status);
                    console.log(data);
                }
            )
                .fail(function (data) {
                    console.log("error");
                    console.log(data);
                });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        //iCheck
        $("input[type='checkbox'], input[type='radio']").iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
    });
</script>
{{ HTML::script('js/plugins/react/react.js'); }}
{{ HTML::script('js/plugins/react/JSXTransformer.js'); }}
<script type="text/jsx" src="{{URL::asset('js/app/milestones.js')}}"></script>
@stop