@extends('layouts.project')

@section('styles')

@stop

@section('content')

<div class="container-fluid">
	<!--/row-->
	<div class="row">
		<div class="col-md-7">
			<!--			<h3>{{$project->title}} Reports</h3>-->

			<div class="box">
				<div class="box-title">
					<h3>Employee Hours</h3>
				</div>
				<div class="box-body">
					<div id="container2"></div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div id="bitbucket"></div>
		</div>
	</div>
</div>
@stop

@section('script')
{{ HTML::script('js/plugins/moment/moment.js'); }}
{{ HTML::script('js/plugins/react/react.js'); }}
{{ HTML::script('js/plugins/react/JSXTransformer.js'); }}
{{ HTML::script('js/plugins/highcharts/highcharts.js'); }}

<script type="text/jsx" src="{{URL::asset('js/app/bitbucket.js')}}"></script>

<script type="text/javascript">
	$(function () {

		var options = {
			chart: {
				renderTo: 'container2',
				type: 'column'
			},
			title: {
				text: ''
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0,
					color: "#5CB85C"
				}
			},
			legend: {
				enabled: false
			},
			xAxis: {},
			yAxis: {
				min: 0,
				title: {
					text: 'Hours'
				}
			},
			series: [
				{}
			]
		};

		options.series[0].name = "Hours";
		options.series[0].data = {{ $values['values'] }};

	options.xAxis.categories = {{ $values['names'] }};
	var chart = new Highcharts.Chart(options);
	})
</script>
@stop