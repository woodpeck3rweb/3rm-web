@extends('layouts.blank')

@section('content')
{{ HTML::script('js/plugins/jquery/jquery-1.10.2.min.js'); }}

<div class="col-md-6">
	<div class="box">
		<div class="box-title">
			<h3>Projects</h3>
            <a data-placement="top" data-toggle="tooltip" href="#newproject" data-original-title="" title="">
                <button class="btn btn-success btn-sm pull-right" type="button">+</button>
            </a>
		</div>
		<div class="box-body">
			<table class="table table-hover">
				<thead>
				<tr>
					<th>Name</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Lead</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
				</thead>
				<tbody>

				@forelse ($projects as $proj)
				<tr>
					<td><a href="{{ URL::to('project/tasks/') }}/{{$proj->pid}}">{{$proj->title}}</a></td>
					<td>{{$proj->start_date}}</td>
					<td>{{$proj->end_date}}</td>
                    <td>{{$proj->first_name}}</td>
                    <td>
						<span class="label label-success">{{$proj->status}}</span>
					</td>
			<!--		<td>
						<a class="btn btn-success btn-sm" href="#">
							<i class="fa fa-search-plus "></i>
						</a>
					</td>-->
                    <td>
						<button class="btn btn-danger btn-sm" id="fav_{{$proj->pid}}" onclick="addToFavorites({{$proj->pid}})">
							<i class="glyphicon glyphicon-star-empty "></i>
						</button>
					</td>
				</tr>
				@empty
					<div>
						<h2>No Projects !! Start creating projects</h2>
					</div>
				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="box">
		<div class="box-title">
			<h3>Favourites</h3>
		</div>
		<!-- /.box-title -->
		<div class="box-body">
			<div class="form-group">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                       <!-- <th>Actions</th>-->
                    </tr>
                    </thead>
                    <tbody>
                @forelse ($favorites as $fav)
                <tr>
                    <td><a href="{{ URL::to('project/tasks/') }}/{{$fav->id}}">{{$fav->title}}</a></td>
                    <td>{{$fav->start_date}}</td>
                    <td>{{$fav->end_date}}</td>
                    <td>
                        <span class="label label-success">{{$fav->status}}</span>
                    </td>
                 <!--   <td>
                        <a class="btn btn-success btn-sm" href="#">
                            <i class="fa fa-search-plus "></i>
                        </a>
                    </td>-->
                </tr>
                @empty
                <div>
                    <h2>No favourite projects.</h2>
                </div>
                @endforelse
                    </tbody>
                </table>
			</div>
		</div>
		<div class="box-footer">
			<p>Tap on the fav button on the projects table.</p>
        </div>
    </div>
	<!-- /.box -->
</div>

<div id="newproject" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-center" id="mySmallModalLabel">New Project</h4>
            </div>
            <div class="modal-body">
                <form id="new-project-form" method="post">
                    <div class="form-group col-md-12">
                        <input id="Name" type="text" name='name0' placeholder='Name' class="form-control"/>
                    </div>
                    <div class="form-group col-md-12">
                        <textarea class="form-control" id="Description" style="height: 125px;"></textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <input id="ETA" type="text" name='mobile0' placeholder='Estimate for Project'
                               class="form-control"/>
                    </div>
                    <div class="form-group col-md-6">
                        <input id="Cost" type="text" name='mobile0' placeholder='Cost of the Project'
                               class="form-control"/>
                    </div>
                    <div class="form-group col-md-12">
                        <select id="Client" class="form-control">
                            <option value="000">--select client--</option>
                            @foreach ($clients as $client)
                            <option value="{{$client->id}}">{{$client->full_name}}</option>
                            @endforeach
                            <option value="-1">Create</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <select id="Lead" class="form-control">
                            <option value="000">--Team Lead--</option>
                            @foreach ($users as $user)
                            <option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <select id="Status" class="form-control">
                            <option value="Status">Status </option>
                            <option value="New"> New </option>
                            <option value="Progress"> Progress </option>
                            <option value="Open"> Open </option>
                            <option value="Reopened"> Reopened </option>
                            <option value="Resolved"> Resolved </option>
                            <option value="Closed"> Closed </option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        Start Date:
                        <input id="StartDate" type="date" name='mobile0' placeholder='Start Date'
                               class="form-control"/>
                    </div>
                    <div class="form-group col-md-6">
                        End Date:
                        <input id="EndDate" type="date" name='mobile0' placeholder='End Date'
                               class="form-control"/>
                    </div>
                    <div class="col-md-1 col-md-offset-5" style="margin-bottom: 20px;">
                        <button type="submit" class="btn btn-sm btn-success">Submit</button>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#Client').change(function() {
            if (this.value == -1) {
                window.location = "{{ URL::to('allclients') }}";
            }
        });

        $("#new-project-form").submit(function (event) {
			mixpanel.track("New Project Submit");
            event.preventDefault();

            var name = $('#Name').val();
            var description = $('#Description').val();
            var eta = $('#ETA').val();
            var client = $('#Client').val();
            var managed_id = $('#Lead').val();
            var start_date = $('#StartDate').val();
            var end_date = $('#EndDate').val();
            var status = $('#Status').val();

            console.log(name);

            var url = "{{URL::to('project/create')}}";

            alert(url);

            $.post(url,
                {
                    title: name,
                    description: description,
                    eta: eta,
                    start_date: start_date,
                    end_date: end_date,
                    manager_id: managed_id,
                    client_id: client,
                    status: status
                },
                function (data, status) {
                    console.log(status);
                    console.log(data);
                    $('#newproject').modal('hide');
                }
            )
                .fail(function (data) {
                    console.log("error");
                    console.log(data);
                });
        });
    });
    var base_url = window.location.origin;
    var moduleIds = new Array();
    $(document).ready(function (){
        $('a[href="#newproject"]').on('click', function (event) {
            event.preventDefault();
			mixpanel.track("New Project Click");
            $('#newproject').modal('show');
        });

        $('#buttonnewproject').on('click',function (){
            //var userIds = new Array();
//            var url = base_url + "/SPARK/public/project/allnewproject/";
			var url = "{{URL::to('project/allnewproject')}}";
            //
            // alert($('#componentTitle').val());
            $.post( url,
                {
                    title: $('#NewProject').val()
                },
                function (data) {
                    $('#newproject').modal('hide');
                    alert(data.message);
                }
            )
        });

    });

    function addToFavorites(id)
    {
		mixpanel.track("Project Fav");
        var project_id = id;
        var user_id = {{Auth::user()->id}};
        var b_url = "{{URL::to('user/favorite')}}";
        var url = b_url+"/"+user_id;
        $.ajax({
        type: "post",
        url: url,
        data: {
            project_id: project_id,
            user_id: user_id
        } ,
        success: function (msg) {
            console.log("Successfully removed -- "+ base_url);
            window.location.reload();
        }
    });
    }
</script>
@stop
@stop