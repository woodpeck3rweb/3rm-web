<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- start Mixpanel -->
	<script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
			for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
		mixpanel.init("{{Config::get("mixpanel.app_id")}}");</script>
	<!-- end Mixpanel -->

    <title>SPARK - Register</title>

    <!-- Maniac stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/animate/animate.min.css"/>
    <link rel="stylesheet" href="css/iCheck/all.css"/>
    <link rel="stylesheet" href="css/style.css"/>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login fixed">
<div class="wrapper animated flipInY">
    <div class="logo"><a href="#"> <span>SPARK</span></a></div>
    <div class="box">
        <div class="header clearfix">
            <div class="pull-left"><a href="{{ URL::to('login') }}"><i class="fa fa-long-arrow-left"></i></a> Register an account</div>
        </div>
        <form id="register-form" method="post" role="form">
            <div class="box-body padding-md">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" id="email" name="email" class="form-control" placeholder="Email">
                        <span class="input-group-addon">@kronch.co</span>
                    </div>
                </div>
                <div class="form-group">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password"/>
                </div>
                <div class="form-group">
                    <input type="password" id="re-password" name="cnfrmpassword" class="form-control"
                           placeholder="Repeat Password"/>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-success btn-block" id="submitBtn">Register</button>
                </div>
            </div>

        </form>

    </div>


    <!-- Javascript -->
    <script src="js/plugins/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript" src="js/plugins/jquery/jquery.validate.min.js"></script>
    <script src="js/plugins/jquery-ui/jquery-ui-1.10.4.min.js" type="text/javascript"></script>

    <!-- Bootstrap -->
    <script src="js/plugins/bootstrap/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/plugins/bootstrapValidator/bootstrapValidator.min.js" type="text/javascript"></script>

    <!-- Interface -->
    <script src="js/plugins/pace/pace.min.js" type="text/javascript"></script>

    <!-- Forms -->
    <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

    <script>
        //iCheck
        $("input[type='checkbox'], input[type='radio']").iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
        var base_url = window.location.origin;
        $('#register-form').bootstrapValidator({
            message: 'This value is not valid',
            fields: {
                email: {
                    message: 'The username is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The username is required and can\'t be empty'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required and can\'t be empty'
                        },
                        stringLength: {
                            min: 6,
                            message: 'The password must be more than 6  characters long'
                        }
                    }
                },
                cnfrmpassword: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required and can\'t be empty'
                        },
                        stringLength: {
                            min: 6,
                            message: 'The password must be more than 6  characters long'
                        },
                        identical: {
                            field: 'password',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                }
            }
        });
        $('#submitBtn').click(function (event) {
			mixpanel.track("Register");
            var email = $('#email').val() + "@kronch.co";
            var pwd = $('#password').val();
            console.log("Pwd " + pwd + " Email " + email);
            var register = $('#register-form').valid();// Checking valid form
            if (register) {
                $.ajax({
                    type: "POST",
                    url: base_url + "/spark/public/user/create",
                    data: {
                        email: email,
                        password: pwd
                    }, //serialize form
                    success: function (msg) {
                        console.log("Successfully Logged In -- " + base_url);
                        window.location.href = base_url + "/spark/public/login";
                    }
                });
            }
            event.preventDefault();
        });

    </script>
</body>
</html>