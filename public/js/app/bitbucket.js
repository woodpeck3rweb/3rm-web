/** @jsx React.DOM */
/**
 * Created by pradyumnad on 05/12/14.
 */

var CommitItem = React.createClass({
    render: function () {
        var commit = this.props.commit;
        return (
            <div className="item">
                <img src={commit.author.user.links.avatar.href} className="img-thumbnail"/>

                <p className="message">
                    <a href="#" className="name">
                        <small className="text-muted pull-right">
                            <i className="fa fa-clock-o"></i> {commit.date}</small>
                        {commit.author.user.display_name}
                    </a>
                    {commit.message}
                </p>
            </div>
            );
    }
});

var CommitList = React.createClass({
    fetchCommits: function () {

        var componentThis = this;
        var jqxhr = $.get("http://localhost:8888/spark/public/commits.json",
            function (data) {
                console.log(data);
                componentThis.setState({commits: data.values});
            })
            .fail(function (data) {
                alert("error");
                console.log(data);
            });
    },
    componentDidMount: function () {
        this.fetchCommits();
    },

    getInitialState: function () {
        return {commits: []};
    },
    render: function () {
        var commitNodes = this.state.commits.map(function (commit) {
            return (
                <CommitItem commit={commit}>
                    {commit.text}
                </CommitItem>
                );
        });
        return (
            <div className="">
            {commitNodes}
            </div>
            );
    }
});

var BitbucketApp = React.createClass({
    render: function () {
        var divStyle = {
            position: "relative",
            overflow: "hidden",
            width: "auto"
        };

        return(
            <div className="box">
                <div className="box-title">
                    <i className="fa fa-comments-o"></i>
                    <h3>Recent Bitbucket activity</h3>
                </div>
                <div className="slimScrollDiv" style={divStyle}>
                    <div className="box-body chat" id="chat-box">
                        <CommitList />
                        <div className="box-footer">
                        </div>
                    </div>
                </div>
            </div>
            );
    }
});
//React.renderComponent(<BitbucketApp />, document.getElementById("bitbucket"));