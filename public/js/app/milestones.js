/** @jsx React.DOM */
var base_url = window.location.origin;

var ListItem = React.createClass({
    getInitialState: function () {
        var item1 = this.props.data;
        return {milestoneId: item1.id,
            item: item1};
    },
    deleteMilestone: function () {
        var milestone_id = this.state.milestoneId;
        $.ajax({
            type: "POST",
            url: base_url + "/spark/public/milestone/delete/"+milestone_id,
            error: function (data) {
                console.log(data);
            },
            success: function (data) {
                console.log("deleted");
            }.bind(this)
        });
    },
    showTaskList: function () {
        React.renderComponent(
            <TaskList tasks={this.state.tasks}/>,
            document.getElementById('milestone-details')
        );
    },
    handleOnMilestoneClicked: function () {
        var milestone_id = this.state.milestoneId;
        $.ajax({
            type: "GET",
            url: base_url + "/spark/public/milestone/tasks/"+milestone_id,
            dataType: 'json',
            error: function (data) {
                console.log(data);
            },
            success: function (data) {
                console.log(data['tasks']);
                var nextItems = data['tasks'];
                this.setState({tasks: nextItems});
                this.showTaskList();
            }.bind(this)
        });
    },
    render: function () {
        var item = this.props.data;

        return (
            <table className="table" >
                <tbody>
                    <tr>
                        <td onClick={this.handleOnMilestoneClicked}>
                            <div className="col-md-3"> {item.title }</div>
                            <div className="col-md-6"> {item.description}</div>
                            <div className="col-md-3"> {item.completion_date}</div>
                        </td>
                        <td>
                            <button className="btn btn-danger btn-xs pull-right" date-id={item.id} onClick={this.deleteMilestone}>
                                <i className="fa fa-trash-o"></i>
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
            );
    }

});
var MilestonesList = React.createClass({
    getInitialState: function () {
        return {items: []};
    },
    loadMilestonesFromServer: function () {
        $.ajax({
            url: base_url + "/spark/public/milestone/index",
            dataType: 'json',
            type: 'GET',
            error: function (data) {
                console.log("Error");
            },
            success: function (data) {
                console.log(data);
                var nextItems = data;
                this.setState({items: nextItems});
            }.bind(this)
        });
    },
    componentWillMount: function () {
        this.loadMilestonesFromServer();
        setInterval(this.loadMilestonesFromServer, this.props.pollInterval);
    },
    render: function () {
        var items = this.state.items.map(function (i) {
            return <ListItem data= {i}/>
        });
        return (
            <div>
                {items}
            </div>
            );
    }
});
React.renderComponent(<MilestonesList pollInterval={50000} />, document.getElementById('milestones'));

var Task = React.createClass({
    getInitialState: function () {
        var item1 = this.props.data;
        return {taskId: item1.id,
            item: item1};
    },

    render: function () {
        var task = this.props.data;
        return (
            <table className="table" >
                <tbody>
                    <tr>
                        <td>
                            <div className="col-md-3"> {task.title}</div>
                            <div className="col-md-6"> {task.description}</div>
                            <div className="col-md-3"> {task.eta}</div>
                        </td>
                    </tr>
                </tbody>
            </table>
            );
    }

});

var TaskList = React.createClass({
    render: function () {
        var tasks = this.props.tasks;
        var items = tasks.map(function (i) {
            console.log(i);
            return <Task data= {i}/>
        });
        return (
            <div>

                {items}
            </div>
            );
    }
});