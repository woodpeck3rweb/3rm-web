/** @jsx React.DOM */
/**
 * Created by pradyumnad on 22/08/14.
 */

var base_url = window.location.origin;
var href = window.location.href;
var currentTaskId;
//Project Id from URL (may have unwanted ?#)
var project_id = href.substr(href.lastIndexOf('/') + 1);
var numb = project_id.match(/\d/g); //Finding numbers
numb = numb.join("");
project_id = numb;

var characters = {'A': 'A', 'B': 'B', 'C': 'C', 'D': 'D', 'E': 'E', 'F': 'F', 'G': '1', 'H': '2', 'I': '3', 'J': '4', 'K': '5', 'L': '6', 'M': '7', 'N': '8', 'O': '9', 'P': 'A', 'Q': 'B', 'R': 'C', 'S': 'D', 'T': 'E', 'U': 'F', 'V': '1', 'W': '2', 'X': '3', 'Y': '4', 'Z': '5'};
var readableDays = {"-1": "Yesterday", "0": "Today", "1": "Tomorrow"};

Date.createFromMysql = function (mysql_string) {
    if (typeof mysql_string === 'string') {
        var t = mysql_string.split(/[- :]/);

        //when t[3], t[4] and t[5] are missing they defaults to zero
        return new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);
    }

    return null;
};

timeFromSeconds = function (seconds) {
    var sec_num = parseInt(seconds, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    var time = hours + ':' + minutes + ':' + seconds;
    return time;
};

secondsFromDates = function (date1, date2) {
    var d1 = Date.createFromMysql(date1);
    var d2 = Date.createFromMysql(date2);

    var seconds = d2.getTime() - d1.getTime();
    seconds = seconds / 1000;

    return Math.abs(seconds);
};

var Comment = React.createClass({
    render: function () {
        var comment = this.props.comment;

        var commentDiv = {
            fontSize: "12px",
            marginLeft: "-30px"
        };

        var bgColor = uniqueUserColor(comment.user.first_name, comment.user.last_name);
        var bgStyle = {
            backgroundColor: bgColor,
            color: "#fff",
            padding: "4px"
        };

        var dateStyle = {
            color: "#999"
        };
        var nameBadge = userNameBadge(comment.user.first_name, comment.user.last_name);

        var createdDate = readableDate(comment.created_at);
        return (
            <div>
                <div className="form-group" style={commentDiv}>
                    <div className="col-md-12">
                        <div className="col-md-1">
                            <abbr title={nameBadge} style={bgStyle}>{nameBadge}</abbr>
                        </div>
                        <div className="col-md-10" >{comment.message}</div>
                        <span className="pull-right" style={dateStyle}> {createdDate}</span>
                    </div>
                </div>
                <div className="clearfix">
                    <hr/>
                </div>
            </div>
            );
    }
});

var CommentsList = React.createClass({
    postComment: function () {
        var task_id = this.props.taskId;
        var componentThis = this;
        var comments = this.state.comments;

        $.post(base_url + "/spark/public/comment/create", {message: this.state.message, task_id: task_id})
            .success(function (data) {
                console.log(data);
                comments.splice(0, 0, data.comment);
                componentThis.setState({comments: comments, message: ""});
            })
            .fail(function (data) {
                console.log(data);
            });
    },
    fetchComments: function () {
        var task_id = this.props.taskId;
        var componentThis = this;
        $.get(base_url + "/spark/public/task/comments/" + task_id,
            function (data) {
                componentThis.setState({comments: data.results});
            })
            .fail(function (data) {
                console.log(data);
            });
    },
    componentWillMount: function () {
        this.fetchComments();
        setInterval(this.fetchComments, this.props.interval);
    },
    componentWillUnmount: function () {
        clearInterval(this.props.interval);
    },
    getInitialState: function () {
        return{comments: [], message : ""};
    },
    onChange: function(e) {
        this.setState({message: e.target.value});
    },
    render: function () {
        var commentNodes = this.state.comments.map(function (c) {
            return <Comment comment={c}/>
        });

        if (this.state.comments.length == 0) {
            commentNodes = <div>
                <h4>No Comments</h4>
                <br/>
            </div>;
        }

        return(
            <div>
                {commentNodes}
                <div className="form-group">
                    <textarea className="form-control" placeholder="Comment" onChange={this.onChange} value={this.state.message}></textarea>
                </div>
                <div clasName="form-group">
                    <button type="button" className="btn btn-info" onClick={this.postComment}>Comment
                    </button>
                </div>
            </div>
            );
    }
});

function readableDate(due_date) {
    var is_past = false;
    var temp_date = due_date;
    if (due_date.length > 0 && due_date != "0000-00-00") {
        var days_diff = moment(due_date).diff(moment().format("YYYY-MM-DD"), 'days');

        due_date = moment(due_date).fromNow(true);
        if (days_diff == 0) {
            due_date = moment(temp_date).calendar();
        }
        else if (days_diff > -2 && days_diff < 2) {
            due_date = readableDays[days_diff];
        } else if (days_diff > -31 && days_diff < 31) {
            due_date = moment(temp_date).format("MMM D");
        } else {
            due_date = moment(temp_date).format("MMM D, YY");
        }
    } else {
        due_date = "Due Date";
    }
    return due_date;
}

var Timer = React.createClass({
    getDefaultProps: function () {
        return {
            seconds_elapsed: 0
        };
    },
    getInitialState: function () {
        return {
            start_time: '', wt_id: '',
            secondsElapsed: this.props.seconds_elapsed,
            timeElapsed: '00:00:00',
            interval: 0
        };
    },
    tick: function () {
        var time = timeFromSeconds(this.state.secondsElapsed);
        this.state.secondsElapsed += 1;
        this.setState({timeElapsed: time});
    },
    stopTick: function () {
        this.hideTimer(true);
        this.state.secondsElapsed = 0;
        clearInterval(this.state.interval);
    },
    componentWillMount: function () {
        this.loadRecentTask();
    },
    componentDidMount: function () {
//        this.state.interval = setInterval(this.tick, 1000);
    },
    componentWillUnmount: function () {
        this.hideTimer(true);
        clearInterval(this.state.interval);
    },
    render: function () {
        var button = React.DOM.button;
        var hiddenDiv = {
            display: "none"
        };

        return (
            <div>
                <div ref="startTimer">
                    <button type="button" className="btn btn-success btn-sm" onClick={this.onclick}>Punch In</button>
                </div>
                <div ref="endTimer" style={hiddenDiv}>
                    <p className="lead">{this.state.timeElapsed}</p>
                    <button type="button" className="btn btn-primary btn-sm" onClick={this.handlePunchOut}>Punch Out</button>
                </div>
            </div>
            );
    },
    onclick: function () {
        this.hideTimer(false);
        this.startTimer();
    },
    hideTimer: function (visibility) {
        if (!visibility) {
            this.refs.endTimer.getDOMNode().style.display = "block";
            this.refs.startTimer.getDOMNode().style.display = "none";
        } else {
            this.refs.endTimer.getDOMNode().style.display = "none";
            this.refs.startTimer.getDOMNode().style.display = "block";
        }
    },
    handlePunchOut: function () {
        console.log("Punched Out " + this.state.timeElapsed);
        this.stopTaskTimeInServer();
    },
    startTimer: function () {
        mixpanel.track("Punch In");
        $.ajax({
            url: base_url + "/spark/public/timelog/create/" + this.props.task.id,
            type: "POST",
            dataType: 'json',
            success: function (data) {
                this.setState({wt_id: data.created_id});

                this.setState({secondsElapsed: 0});
                this.state.interval = setInterval(this.tick, 1000);
                this.tick();
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(status, err.toString());
            }.bind(this)
        });
    },
    loadRecentTask: function () {
        $.ajax({
            url: base_url + "/spark/public/timelog/recent/" + this.props.task.id,
            type: "GET",
            dataType: 'json',
            success: function (data) {
                this.setState({start_time: data.start_time, wt_id: data.id});

                var seconds = secondsFromDates(this.state.start_time, data.current_time);

                this.hideTimer(false);
                this.setState({secondsElapsed: seconds});
                this.state.interval = setInterval(this.tick, 1000);
                this.tick();
            }.bind(this),
            error: function (xhr, status, err) {
                this.hideTimer(true);
                console.log(status, err.toString());
            }.bind(this)
        });
    },
    stopTaskTimeInServer: function () {
        mixpanel.track("Punch Out");
//        alert("Stopped");
        $.ajax({
            url: base_url + "/spark/public/timelog/update/" + this.state.wt_id,
            type: "POST",
            dataType: 'json',
            success: function (data) {
                console.log(data);

                this.stopTick();
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(status, err.toString());
            }.bind(this)
        });
    }
});

var TaskDetails = React.createClass({
    initXEditable: function () {
        var t_id = this.props.task.id;
        var task_update_url = base_url + "/spark/public/task/update/" + t_id;

        var aThis = this;

        $("#assignee_id").editable({
                placement: "bottom",
                empty: "Assigned To",
                source: base_url + "/spark/public/user/index?api_key=123",
                url: task_update_url,
                success: function(response, newValue) {
                    if(!response) {
                        return "Unknown error!";
                    }

                    if(response.success === false) {
                        return response.msg;
                    }

                    var task = aThis.props.task;
                    task.assignee_id = newValue;
                    aThis.props.onTaskUpdate(task);
                }
            }
        );
        $("#priority").editable({
                placement: "bottom",
                empty: "Priority",
                source: ["Blocker", "Critical", "Major", "Minor", "Trivial"],
                url: task_update_url,
                success: function(response, newValue) {
                    if(!response) {
                        return "Unknown error!";
                    }

                    if(response.success === false) {
                        return response.msg;
                    }

                    var task = aThis.props.task;
                    task.priority = newValue;
                    aThis.props.onTaskUpdate(task);
                }
            }
        );
        $("#status").editable({
                placement: "bottom",
                empty: "Progress",
                source: ["Open", "Progress", "Resolved", "Reopened", "Closed"],
                url: task_update_url,
                success: function(response, newValue) {
                    if(!response) {
                        return "Unknown error!";
                    }

                    if(response.success === false) {
                        return response.msg;
                    }

                    var task = aThis.props.task;
                    task.status = newValue;
                    aThis.props.onTaskUpdate(task);
                }
            }
        );
        $("#milestone").editable({
                placement: "bottom",
                empty: "milestone",
                source: ["Delete Task", " Add to Milestone"],
                url: task_update_url
            }
        );
        $("#description").editable({
                mode: "inline",
                url: task_update_url
            }
        );

        //Date picker
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        //http://www.eyecon.ro/bootstrap-datepicker/
        $('#due_date').datepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? '' : '';
            }
        })
            .on('changeDate',
            function (ev) {
                startDate = new Date(ev.date);
                $('#due_date').text(readableDate($('#due_date').data('date')));
                var due_date = $('#due_date').data('date');

                var task = aThis.props.task;
                task.due_date = due_date;
                aThis.props.onTaskUpdate(task);

                $.ajax({
                    url: task_update_url,
                    method: "post",
                    data: {
                        due_date: due_date
                    },
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        console.log(data);
                    }
                });
            });

        //Dynamic Editing
        $("#title").keydown(function (event) {
            mixpanel.track("Task Title Edit");
            var esc = event.which == 27,
                nl = event.which == 13,
                el = event.target,
                input = el.nodeName != 'INPUT' && el.nodeName != 'TEXTAREA',
                data = {};
            console.log(el.nodeName + " -- " + el.innerHTML);

            if (input) {
                if (esc) {
                    // restore state
                    document.execCommand('undo', true, null);
                    el.blur();
                } else if (nl) {
                    // save
                    el.innerHTML = el.innerHTML.replace("&nbsp;", " ");
                    el.innerHTML = el.innerText
                    data[el.getAttribute('data-name')] = el.innerHTML;
                    var url = el.getAttribute('data-url');
                    console.log(url);

                    var task = aThis.props.task;
                    task.title = el.innerHTML;
                    aThis.props.onTaskUpdate(task);

                    $.ajax({
                        url: url,
                        data: data,
                        type: 'post'
                    }).success(function (data) {
                        console.log("SUCCESS");
                        console.log(data);
                    });
//                    console.log(JSON.stringify(data));

                    el.blur();
                    event.preventDefault();
                }
            }
        });
    },
    logTimeSpentToServer: function () {
        event.preventDefault();

        var time_spent = $('#Time').val();

        var task_update_url = base_url + "/spark/public/task/time/" + this.props.task.id;

        $.ajax({
            url: task_update_url,
            method: "post",
            data: {
                time_spent: time_spent
            },
            error: function (data) {
                console.log(data);
                alert(data);
            },
            success: function (data) {
                console.log(data);
                $('#LogTimeModalForm').modal('hide');
            }
        });
    },
    componentDidUpdate: function () {
        this.initXEditable();
    },
    componentDidMount: function () {
        this.initXEditable();
    },
    render: function () {
        var boxBodyStyle = {
            marginLeft: "20px",
            marginRight: "15px"
        };
        var boxTitleStyle = {
            height: "50px"
        };
        var boxStyle = {
            width: "460px",
            boxShadow: "0 0 10px 1px #AAA"
        };
        var pidStyle = {
            marginLeft: "30px",
            cursor: "pointer"
        };
        var divStyle = {
            paddingLeft: "0px",
            paddingRight: "0px"
        };

        var activityPStyle = {
            color: "gray"
        };

        var task = this.props.task;
        var assignee_name = "Assign To";
        if (task.assignee != null) {
//            assignee_name = task.assignee.first_name + " " + task.assignee.last_name;
            assignee_name = task.assignee.first_name;
            if (assignee_name.length > 8) {
                assignee_name = assignee_name.substring(0, 7) + "..";
            }
        }

        var status = "";
        if (task.status.length == 0) {
            status = "Status";
        }

        //Getting Readable Date
        var due_date = "Due Date";
        due_date = readableDate(task.due_date);

        var comments = <CommentsList taskId ={task.id} interval={20000}/>;
        var timer = <Timer key={this.props.task.id} task={this.props.task}/>;

        //Update URL
        var t_id = this.props.task.id;
        var task_update_url = base_url + "/spark/public/task/update/" + t_id;

        var time_spent = Math.round(task.time_spent * 100) / 100;

        var time_spent_html = <h4 className="pull-right">
            <mark>{time_spent}</mark>
            <span>/{task.eta} Hrs</span>
        </h4>;
        if (task.eta == null || task.eta <= 0.0) {
            if (task.time_spent == 0) {
                time_spent_html = <h4></h4>
            } else {
                time_spent_html = <h4 className="pull-right">
                    <mark>{time_spent} Hrs</mark>
                </h4>;
            }
        }

        return (
            <div className="box" key={task.id} style={boxStyle}>
                <div className="box-title"  style={boxTitleStyle} >
                    <div className="col-md-2 col-sm-2" style={divStyle}>
                        <div className="form-group">
                            <div className="col-sm-1">
                                <i className="fa fa-child"></i>
                            </div>
                            <p id="assignee_id" data-type="select" data-pk={task.id} data-title="Assigned To"
                            className="editable" style={pidStyle}>{assignee_name}</p>
                        </div>
                    </div>
                    <div className="col-md-2 col-sm-2" style={divStyle}>
                        <div className="form-group">
                            <div className="col-sm-1">
                                <i className="fa fa-calendar"></i>
                            </div>
                            <a href="#" id="due_date" data-date-format="yyyy-mm-dd" data-date="">{due_date}</a>
                        </div>
                    </div>
                    <div className="col-md-2 col-sm-2" style={divStyle}>
                        <div className="form-group">
                            <div className="col-sm-1">
                                <i className="glyphicon glyphicon-star-empty"></i>
                            </div>
                            <p id="priority" data-type="select" data-pk={task.id} data-title="Priority"
                            className="editable" style={pidStyle}>{task.priority}</p>
                        </div>
                    </div>
                    <div className="col-md-2 col-sm-2" style={divStyle}>
                        <div className="form-group">
                            <div className="col-sm-1">
                                <i className=" glyphicon glyphicon-align-center"></i>
                            </div>
                            <p id="status" data-type="select" data-pk={task.id} data-title="Progress"
                            className="editable" style={pidStyle}>{task.status}</p>
                        </div>
                    </div>
                    <div className="col-md-2 col-sm-2" style={divStyle}>
                        <div className="form-group">
                            <div className="col-sm-1">
                                <a href="#" id="time_spent" data-toggle="modal" data-target="#LogTimeModalForm">
                                    <i className="glyphicon glyphicon-time"></i>
                                </a>
                            </div>
                            <a href="#" id="time_spent" data-toggle="modal" data-target="#LogTimeModalForm">
                                {task.time_spent == 0 ? "Log" : (Math.round(task.time_spent * 10) / 10) + " Hrs"}
                            </a>
                        </div>
                    </div>
                    <div className="col-md-2 col-sm-2 pull-right" style={divStyle}>
                        <div className="form-group">
                            <div className="col-sm-1">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                                    <span>
                                        <i className="fa fa-caret-down"></i>
                                    </span>
                                </a>
                                <ul className="dropdown-menu">
                                    <li>
                                        <a id="deletetask" href="#">
                                            <i className="fa fa-trash-o"></i>
                                        Delete Task</a>
                                    </li>
                                    <li>
                                        <a  href="#add-to-milestone"data-toggle="tooltip"
                                        data-placement="top">
                                            <i className="fa fa-user"></i>
                                        Add to Milestone</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="box-body" style={boxBodyStyle}>
                    <form role="form">
                        <div className="form-group">
                            <h2 id="title" contentEditable={true} data-url={task_update_url} data-name="title" data-pk={task.id} >{task.title}</h2>
                        </div>
                        <div className="form-group">
                            <p id="description" className="editable" id="description" data-type="textarea" data-pk={task.id}
                            data-title="Enter Description">{task.description}</p>
                        </div>
                        <div className="clearfix">
                            <hr/>
                        </div>
                        <div className="col-md-12">
                            <div className="col-md-6">
                                <div id="timer">
                                    {timer}
                                </div>
                            </div>
                            <div className="col-md-6">
                                {time_spent_html}
                            </div>
                        </div>
                        <div className="clearfix">
                            <hr/>
                        </div>
                        <p style={activityPStyle}>Created by {task.user.first_name} {task.user.last_name}</p>
                        <div className="clearfix">
                            <hr/>
                        </div>
                        {comments}
                    </form>
                </div>

                <div className="modal fade" id="LogTimeModalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content col-md-6 col-md-offset-2">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">
                                    <span
                                    aria-hidden="true">×</span>
                                    <span className="sr-only">Close</span>
                                </button>
                                <h4 className="modal-title" id="myModalLabel">
                                    <i className="fa fa-edit"></i>
                                Modal Form</h4>
                            </div>
                            <div className="modal-body">
                                <form id="log-time-form" className="form-vertical" role="form" onSubmit={this.logTimeSpentToServer}>
                                    <div className="form-group">
                                        <div className="col-sm-7">
                                            <input type="text" className="form-control" id="Time" placeholder="Time Worked." />
                                        </div>
                                    </div>
                                    <button type="submit" id="log-time-form-button" className="btn btn-primary">Log</button>
                                </form>
                            </div>
                            <div className="modal-footer">
                            M/m to represent Minutes. Default: Hours
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            );
    }
});

//Helper functions for User theme
function uniqueUserColor(first_name, last_name) {
    var name = first_name + last_name;
    name = name.toUpperCase().substr(0, 6);

    for (var i = 0; i < 6; i++) {
        var c = name.charAt(i);
        name = name.replace(c, characters[c]);
    }
    var bg_color = "#" + name;
    return bg_color;
}

function userNameBadge(first_name, last_name) {
    var assignee_name = "";
    if (last_name.length > 0) {
        assignee_name = first_name.substr(0, 1) + last_name.substr(0, 1);
    } else {
        assignee_name = first_name.substr(0, 2);
    }
    assignee_name = assignee_name.toUpperCase();
    return assignee_name;
}

var Task = React.createClass({
    getInitialState: function () {
        return {task: {}};
    },
    handleTaskUpdate: function(task) {
        this.setState({task: task});
    },
    showTaskDetails: function () {
        React.renderComponent(
            <TaskDetails task={this.props.task} onTaskUpdate={this.handleTaskUpdate}/>,
            document.getElementById('task-details')
        );
    },
    handleOnTaskClicked: function () {
        var task = this.props.task;
        this.setState({task: task});
        mixpanel.track("Show Task", task);
        console.log(this.props.task);
        currentTaskId = task.id;
        this.showTaskDetails();
    },
    onFocus: function () {
        console.log("Focussed");
        this.showTaskDetails();
    },
    onFocusOut: function () {

    },
    render: function () {
        var task = jQuery.isEmptyObject(this.state.task) ? this.props.task : this.state.task;

        var classString = 'label';
        classString += " label-" + task.status.toLowerCase();
        classString += " pull-right";

        var priorityClassString = 'label';
        priorityClassString += " label-" + task.priority.toLowerCase();
        priorityClassString += " pull-right";


        var bg_color = "transparent";

        //Generating Name Badge
        var assignee_name = "";
        if (task.assignee != null) {
            assignee_name = userNameBadge(task.assignee.first_name, task.assignee.last_name);
            bg_color = uniqueUserColor(task.assignee.first_name, task.assignee.last_name);
        }

        //Getting Readable Date
        var due_date = "";
        var is_past = false;
        var dateColor = {
            color: "#000"
        };

        if (task.due_date != null && task.due_date.length > 0 && task.due_date != "0000-00-00") {
            due_date = task.due_date;
            var days_diff = moment(due_date).diff(moment().format("YYYY-MM-DD"), 'days');

            due_date = moment(due_date).fromNow(true);

            if (days_diff > -2 && days_diff < 2) {
                due_date = readableDays[days_diff];
            }

            if (days_diff < 0) {
                is_past = true;
                dateColor = {
                    color: "#F00"
                };
            }
        }

        var divStyle = {
            backgroundColor: bg_color,
            color: "#FFF",
            padding: "4px",
            marginRight: "10px"
        };

        var myline = {};
        var li_class = "task-normal";
        if (task.status == "Closed") {
            li_class = "task-closed";
            myline = {
                textDecoration: "line-through"
            };
            dateColor = {
                display: "none"
            };
        }
        li_class = "task-" + task.status.toLowerCase();


        var li_priority_class = "task-" + task.priority.toLowerCase();

        if (task.assignee != null)
            var li_user_class = task.assignee.first_name;
        else
            var li_user_class = "";
        li_class += " " + li_priority_class + " " + li_user_class;
        return (
            <li tabIndex={task.id} ref="task" className={li_class} onClick={this.handleOnTaskClicked} onFocus={this.onFocus}>
                <abbr style={divStyle}>{assignee_name}</abbr>
                <span style={myline}>{task.title}</span>
                <span className="pull-right" style={dateColor}> {due_date}</span>
                <span className={classString}>{task.status}</span>
                <span className={priorityClassString}>{task.priority}</span>
            </li>
            );
    }
});

var TaskList = React.createClass({
    loadScript: function () {
        $('div').on('focus', 'li',function () {
            $this = $(this);
            $this.addClass('active').siblings().removeClass('active');
        }).on('keydown', 'li',function (e) {
            $this = $(this);
            if (e.keyCode == 40) {
                $this.next().focus();
                return false;
            } else if (e.keyCode == 38) {
                $this.prev().focus();
                return false;
            }
        }).find('li').first().focus();

        //Hiding and Showing Completed tasks
        $('#hidecompletedtasks').on('click', function () {
            var x = $('li').filter(".task-closed").hide();

            $(this).hide();
            $('#showcompletedtasks').show();
        });

        $('#showcompletedtasks').on('click', function () {
            $('#task-li li').show();

            $('#selectpriority').trigger('change');
            $(this).hide();
            $('#hidecompletedtasks').show();

        });
    },
    componentDidMount: function () {
        this.loadScript();
    },
    componentDidUpdate: function () {
//        this.loadScript();
        console.log("Hey Task list updated");
    },
    render: function () {
        var createItem = function (item) {
            return <Task task={item}/>
        };

        var divStyle = {
            overflow: "scroll",
            height: "400px",
            overflowX: "hidden"
        };

        return <div id="task-li" style={divStyle}>{this.props.items.map(createItem)}</div>;
    }
});

var TaskApp = React.createClass({
    loadTasksFromServer: function () {
        $.ajax({
            url: base_url + "/spark/public/project/tasks/" + project_id + "?api_key=123",
            dataType: 'json',
            error: function (data) {
                console.log(data);
            },
            success: function (data) {
                console.log("== Tasks ==");
                var nextItems = data;
                this.setState({items: nextItems});
            }.bind(this)
        });
    },
    postTaskToServer: function () {
        mixpanel.track("Quick Task Creation");

        var title = this.state.text;
        console.log(title);

        var url = base_url + "/spark/public/task/create";

        var reactThis = this;
        $.post(url,
            {
                title: title,
//                description: description,
//                type: type,
//                priority: priority,
//                eta: eta,
//                user_id: user_id,
//                module_id: module_id,
                project_id: project_id
            },
            function (data, status) {
                console.log(status);
                console.log(data);

                if (status) {
                    var nextItems = reactThis.state.items.concat(data['task']);
                    var nextText = '';
                    reactThis.setState({items: nextItems, text: nextText});
                }
            }
        )
            .fail(function (data) {
                console.error(data);
            });
    },
    componentWillMount: function () {
        this.loadTasksFromServer();
        setInterval(this.loadTasksFromServer, this.props.pollInterval);
    },
    getInitialState: function () {
        return {items: [], text: ''};
    },
    onChange: function (e) {
        this.setState({text: e.target.value});
    },
    handleSubmit: function (e) {
        e.preventDefault();
        this.postTaskToServer();
    },
    render: function () {
        var liStyle = {
            position: "relative",
            display: "block",
            paddingTop: "10px",
            borderTop: "2px solid rgb(36, 166, 239)"
        };

        var ulStyle = {
            paddingLeft: "0px"
        };

        return (
            <ul style={ulStyle}>
                <TaskList items={this.state.items} />
                <li style={liStyle}>
                    <form onSubmit={this.handleSubmit}>
                        <input ref="taskInput" type="text" onChange={this.onChange} value={this.state.text} className="form-control task-input" placeholder="Enter task" />
                    </form>
                </li>
            </ul>
            );
    }
});

$(document).ready(function () {

    React.renderComponent(<TaskApp pollInterval={20000} />, document.getElementById('tasks'));

    $('body').on('click', "a[href='#add-to-milestone']", function (event) {
        //alert('hi2');
        event.preventDefault();
        $('#addtomilestone').modal('show');
    });

    $('body').on('click', ".addtomilestone", function (event) {
        var myId = event.target.id.replace('milestone', '');
        var url = base_url + "/spark/public/task/assignmilestone";
        $.post(url,
            {
                taskId: currentTaskId,
                milestoneId: myId
            },
            function (data) {
                console.log(data.status);
                if (data.status == 'milestone attached successfully.') {
                    $('#addtomilestone').modal('hide');
                }
            }
        )
    });

    $('body').on('click', "#deletetask", function (event) {
        mixpanel.track("Task Delete");
        //alert('hi2');
        event.preventDefault();
        var url = base_url + "/spark/public/task/deletetask";
        $.post(url,
            {
                taskId: currentTaskId
            },
            function (data) {
                if (data.status == 'Task deleted successfully.') {
                    $('#task-details').html('');
                }
            }
        )

    });

    $('#selectpriority,#select,#selectuser').on('change', function () {

        var selectpriorityclass = $('#selectpriority').val();
        var selectclass = $('#select').val();
        var selectuserclass = $('#selectuser').val();

        $('#task-li li').hide();

        if (selectpriorityclass == 'All' || selectclass == 'All' || selectuserclass == 'All') {
            if (selectpriorityclass == 'All' && selectclass == 'All' && selectuserclass == 'All') {
                $('#task-li li').show();
            }
            else if (selectpriorityclass == 'All' && selectuserclass == 'All' && selectclass != 'All') {
                $('.' + selectclass).show();
            }
            else if (selectpriorityclass == 'All' && selectuserclass != 'All' && selectclass == 'All') {

                $('.' + selectuserclass).show();
            }
            else if (selectpriorityclass != 'All' && selectuserclass == 'All' && selectclass == 'All') {

                $('.' + selectpriorityclass).show();
            }
            else if (selectpriorityclass == 'All' && selectuserclass != 'All' && selectclass != 'All') {
                $('.' + selectuserclass + '.' + selectclass).show();
            }
            else if (selectpriorityclass != 'All' && selectuserclass != 'All' && selectclass == 'All') {
                $('.' + selectpriorityclass + '.' + selectuserclass).show();
            }
            else if (selectpriorityclass != 'All' && selectuserclass == 'All' && selectclass != 'All') {
                $('.' + selectpriorityclass + '.' + selectclass).show();
            }
        }
        else {
            $('.' + selectpriorityclass + '.' + selectuserclass + '.' + selectclass).show();
        }
    });
});